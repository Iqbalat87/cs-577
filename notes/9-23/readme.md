---
title: 'Lecture Notes - Sept 23'
---

Today's lecture will be a bit more about runtime. 

To support today's lecture, Prof. JX built a binary called
[`crackme`](https://drive.google.com/file/d/1P9vEPeT1NeNv0rrYShLdT4iIoZ397QiV/view?usp=sharing)
from stripped C Code. You can download this binary more easily by doing:

```shell
$ wget 'https://drive.google.com/u/1/uc?id=1P9vEPeT1NeNv0rrYShLdT4iIoZ397QiV&export=download' -O 'crackme'
$ chmod +x crackme
```

I'll include the file [here](crackme), in the repository.

![](https://i.imgur.com/DUoqBOQ.png)

So, how does the loading process work?? It turns out, the first thing that 
happens in your program is this library, `ld-linux-x86-64.so`, is loaded. This
library is the entrypoint, and its job is to 

Now, let's continue running the binary until we hit the entry point of our 
`crackme` binary:

```shell
gdb: info files
gdb: break *0xXXXXXXX (the program entry)
gdb: continue
```

The `break` command here will actually set a gdb breakpoint at the instruction
address `*0xXXXXXXX`. We want to execute until we get to the program's 
breakpoint. However, we need to know the specific location to set at the 
breakpoint. 

We can find the program's entry point via gdb's `info files` command:

<details>
<summary><code>gdb</code>'s <code>info files</code> command</summary>

```
Symbols from "/home/remnux/crackme".
Native process:
	Using the running image of child process 3450.
	While running this, GDB does not access memory from...
Local exec file:
	`/home/remnux/crackme', file type elf64-x86-64.
	Entry point: 0x4004e0
	0x0000000000400238 - 0x0000000000400254 is .interp
	0x0000000000400254 - 0x0000000000400274 is .note.ABI-tag
	0x0000000000400274 - 0x0000000000400298 is .note.gnu.build-id
	0x0000000000400298 - 0x00000000004002b4 is .gnu.hash
	0x00000000004002b8 - 0x0000000000400348 is .dynsym
	0x0000000000400348 - 0x00000000004003bb is .dynstr
	0x00000000004003bc - 0x00000000004003c8 is .gnu.version
	0x00000000004003c8 - 0x0000000000400408 is .gnu.version_r
	0x0000000000400408 - 0x0000000000400438 is .rela.dyn
	0x0000000000400438 - 0x0000000000400480 is .rela.plt
	0x0000000000400480 - 0x0000000000400497 is .init
	0x00000000004004a0 - 0x00000000004004e0 is .plt
	0x00000000004004e0 - 0x0000000000400702 is .text
	0x0000000000400704 - 0x000000000040070d is .fini
	0x0000000000400710 - 0x0000000000400744 is .rodata
	0x0000000000400744 - 0x0000000000400788 is .eh_frame_hdr
	0x0000000000400788 - 0x00000000004008a8 is .eh_frame
	0x0000000000600e10 - 0x0000000000600e18 is .init_array
	0x0000000000600e18 - 0x0000000000600e20 is .fini_array
	0x0000000000600e20 - 0x0000000000600ff0 is .dynamic
	0x0000000000600ff0 - 0x0000000000601000 is .got
	0x0000000000601000 - 0x0000000000601030 is .got.plt
	0x0000000000601030 - 0x0000000000601040 is .data
	0x0000000000601040 - 0x0000000000601048 is .bss
	0x00007ffff7dd51c8 - 0x00007ffff7dd51ec is .note.gnu.build-id in /lib64/ld-linux-x86-64.so.2
	0x00007ffff7dd51f0 - 0x00007ffff7dd52c4 is .hash in /lib64/ld-linux-x86-64.so.2
	0x00007ffff7dd52c8 - 0x00007ffff7dd53c0 is .gnu.hash in /lib64/ld-linux-x86-64.so.2
	0x00007ffff7dd53c0 - 0x00007ffff7dd56f0 is .dynsym in /lib64/ld-linux-x86-64.so.2
	0x00007ffff7dd56f0 - 0x00007ffff7dd5914 is .dynstr in /lib64/ld-linux-x86-64.so.2
	0x00007ffff7dd5914 - 0x00007ffff7dd5958 is .gnu.version in /lib64/ld-linux-x86-64.so.2
	0x00007ffff7dd5958 - 0x00007ffff7dd59fc is .gnu.version_d in /lib64/ld-linux-x86-64.so.2

```
</details>

So, as we can see, the entry point is `0x4004e0`, we'll use this as the
breakpoint and have the program continue until we get here. Now, we can just
have the program continue running until the entrypoint with `continue`.

![](https://i.imgur.com/6ejJX8b.png)

So, what is the runtime doing here? Well, we need to load the shared libraries
that are being used, and **then**, we need to switch the execution to the
program entrypoint. Since this code wasn't compiled statically with the program,
it needs to be loaded when needed.

If you want to see what dynamic libraries are required by your program, you can
use the `ldd` program:

```shell
$ ldd crackme
	linux-vdso.so.1 (0x00007fff6bb98000)
	libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f7adc928000)
	/lib64/ld-linux-x86-64.so.2 (0x00007f7adcd19000)
```

But where do these libraries go? Well, on the stack! The stack has a section
where libraries tend to end up. Initially, these will just be links (like symlinks
for low level memory). It'd be really expensive to load the whole library into
memory, so things are only loaded by need, but they're linked at runtime. 

### Step 4: Continue running the binary until the main function

Let's run the binary and break at the main function! But, we still need to find
the main function. Remember, the main function **won't** be the entrypoint, since
there will be some dynamically loaded code that is executed. We can't search
for `main`, since the symbols have been removed. If we had symbols, we could
use gdb's `disassemble` command. 

Instead, we'll have to use `x/`, which can print things in a special format.
What do we print? Well, we want to print the value of the program counter. The
register `rip` will contain that, so we'll just use that. 

At this point, we're in the entrypoint function, so maybe we want to figure
out how long this function is. Maybe we can use `x` to print a certain number
of instructions in assembly format?

Well, `x/si` is the command to print in assembly format. Let's say we want to
print 20 instructions - in this case, we'll use `x/20si $rip`, which says to 
print next 20 instructions after rip:

<details>
<summary><code>x/20si $rip</code></summary>

```
(gdb) x/20si $rip
=> 0x4004e0:	xor    %ebp,%ebp
   0x4004e2:	mov    %rdx,%r9
   0x4004e5:	pop    %rsi
   0x4004e6:	mov    %rsp,%rdx
   0x4004e9:	and    $0xfffffffffffffff0,%rsp
   0x4004ed:	push   %rax
   0x4004ee:	push   %rsp
   0x4004ef:	mov    $0x400700,%r8
   0x4004f6:	mov    $0x400690,%rcx
   0x4004fd:	mov    $0x4005e1,%rdi
   0x400504:	callq  *0x200ae6(%rip)        # 0x600ff0
   0x40050a:	hlt    
   0x40050b:	nopl   0x0(%rax,%rax,1)
   0x400510:	repz retq 
   0x400512:	nopw   %cs:0x0(%rax,%rax,1)
   0x40051c:	nopl   0x0(%rax)
   0x400520:	push   %rbp
   0x400521:	mov    $0x601040,%eax
   0x400526:	cmp    $0x601040,%rax
   0x40052c:	mov    %rsp,%rbp

```
</details>

Unfortunately, this is in att syntax (`$literal` and `%reg` are att), so we
should switch the syntax to intel, since that's what we're learning in.
Thankfully, we can do this with `set disassembly-flavor intel`:

<details>
<summary><code>x/20si $rip</code> in intel syntax</summary>

```
=> 0x4004e0:	xor    ebp,ebp
   0x4004e2:	mov    r9,rdx
   0x4004e5:	pop    rsi
   0x4004e6:	mov    rdx,rsp
   0x4004e9:	and    rsp,0xfffffffffffffff0
   0x4004ed:	push   rax
   0x4004ee:	push   rsp
   0x4004ef:	mov    r8,0x400700
   0x4004f6:	mov    rcx,0x400690
   0x4004fd:	mov    rdi,0x4005e1
   0x400504:	call   QWORD PTR [rip+0x200ae6]        # 0x600ff0
   0x40050a:	hlt    
   0x40050b:	nop    DWORD PTR [rax+rax*1+0x0]
   0x400510:	repz ret 
   0x400512:	nop    WORD PTR cs:[rax+rax*1+0x0]
   0x40051c:	nop    DWORD PTR [rax+0x0]
   0x400520:	push   rbp
   0x400521:	mov    eax,0x601040
   0x400526:	cmp    rax,0x601040
   0x40052c:	mov    rbp,rsp
```
</details>

So anyways, this isn't necessarily the main function - we know that. There's a
trick to finding it though! When looking for the main function, look for an
instruction that moves an address to `rdi` before calling. 

With the System-V ABI, the first argument or every function is placed in `rdi`. 
In this case, it's likely that our code is being loaded by the loading library.
This is to stay compliant with the operating system so the logic can actually
be run, but in general, this can be depended on. So, we know that our main
function is in `0x4005e1`.


### Step 5: Read Throuch Code

In reality, you won't have to read through each instruction like we're going to
do here, but this is just for demonstration.

So, we'll set our breakpoint and continue until the main function:

```
(gdb) break *0x4005e1
Breakpoint 2 at 0x4005e1
(gdb) continue
Continuing.
```

Now that we're here, let's see the next 20 instructions:

<details>
<summary>next 20 instructions</summary>

```
(gdb) x/20si $pc
=> 0x4005e1:	push   rbp
   0x4005e2:	mov    rbp,rsp
   0x4005e5:	sub    rsp,0x20
   0x4005e9:	mov    rax,QWORD PTR fs:0x28
   0x4005f2:	mov    QWORD PTR [rbp-0x8],rax
   0x4005f6:	xor    eax,eax
   0x4005f8:	lea    rdi,[rip+0x115]        # 0x400714
   0x4005ff:	mov    eax,0x0
   0x400604:	call   0x4004c0 <printf@plt>
   0x400609:	lea    rax,[rbp-0x14]
   0x40060d:	mov    rsi,rax
   0x400610:	lea    rdi,[rip+0x10e]        # 0x400725
   0x400617:	mov    eax,0x0
   0x40061c:	call   0x4004d0 <__isoc99_scanf@plt>
   0x400621:	lea    rdi,[rip+0x100]        # 0x400728
   0x400628:	mov    eax,0x0
   0x40062d:	call   0x4004c0 <printf@plt>
   0x400632:	lea    rax,[rbp-0x10]
   0x400636:	mov    rsi,rax
   0x400639:	lea    rdi,[rip+0xe5]        # 0x400725
```
</details>

If we want to step forward by one instructions, we can do so with `si`:

<details>
<summary>step forward</summary>

```
(gdb) si
0x00000000004005e2 in ?? ()
(gdb) x/20si $pc
=> 0x4005e2:	mov    rbp,rsp
   0x4005e5:	sub    rsp,0x20
   0x4005e9:	mov    rax,QWORD PTR fs:0x28
   0x4005f2:	mov    QWORD PTR [rbp-0x8],rax
   0x4005f6:	xor    eax,eax
   0x4005f8:	lea    rdi,[rip+0x115]        # 0x400714
   0x4005ff:	mov    eax,0x0
   0x400604:	call   0x4004c0 <printf@plt>
   0x400609:	lea    rax,[rbp-0x14]
   0x40060d:	mov    rsi,rax
   0x400610:	lea    rdi,[rip+0x10e]        # 0x400725
   0x400617:	mov    eax,0x0
   0x40061c:	call   0x4004d0 <__isoc99_scanf@plt>
   0x400621:	lea    rdi,[rip+0x100]        # 0x400728
   0x400628:	mov    eax,0x0
   0x40062d:	call   0x4004c0 <printf@plt>
   0x400632:	lea    rax,[rbp-0x10]
   0x400636:	mov    rsi,rax
   0x400639:	lea    rdi,[rip+0xe5]        # 0x400725
   0x400640:	mov    eax,0x0
```
</details>

As we can see, the program moved forward! We've moved to the `mov` instruction.

#### Interlude - The Stack

Let's zoom into these first 2 instructions here, since they're kinda important:

```
=> 0x4005e1:	push   rbp
   0x4005e2:	mov    rbp,rsp
```

These two instructions appear in the start of most functions. They're used to
maintain the stack - the first is to save and update the frame pointer (save
the existing value of `rbp`, then update it with the current stack pointer
`rsp`). 

Remember, the stack "grows downwards", that is, lower positions of memory are
occupied with new stack frames. So, the "top" of the stack moves lower and
lower as new stack frames are added. This makes less sense numerically, and
more sense physically.

Remember, memory is **physical**. In x86, due to some historical hardware
limitation, it was decided that the stack should be placed at the 'end of
memory', and should grow upside down, so it can always (for the most part) be
allowed to grow unimpeded. So, in a way, the stack is like a **physical
thing**, that moves physically across memory.  It may be easier to visualize
the stack flipped upside-down physically, and then to assign numbers increasing
from top-to-bottom.

Notice that when you add to the stack, since its upside down, the new element
is added onto the lower portion. Also notice, that since the new element is
added onto the stack, it occupies a **numerically lower** position in memory
than the previous stack frame. This is kind of like in physics, when you could
model movement with the negative axis or positive axis, as long as you stay
consistent.

Now, let's visualize an individual stack frame:

![](https://i.imgur.com/7942Wry.png)

Let's say we have a local variable `a` on the stack here, and a local variable
`b`. In general, `rsp` will be numerically at the bottom of the frame. So,
if you want to get at `a`, you can try to use the offset to `rsp`. This is
important because the stack is often used to save local variables, so they can
be yanked into and out of registers when need be. 

So, long story short, when loading memory with `rsp` offsets, this could be
when code is accessing local variables.

##### Why save `rbp`

![](https://i.imgur.com/CA5ZPVj.png)

#### Step 5: continued

![](https://i.imgur.com/CBGv04C.png)

### Step 6: Read thru next few instructions

Here are the instructions after the stack stuff:

```
=> 0x4005e5:	sub    rsp,0x20
   0x4005e9:	mov    rax,QWORD PTR fs:0x28
   0x4005f2:	mov    QWORD PTR [rbp-0x8],rax
   0x4005f6:	xor    eax,eax
   0x4005f8:	lea    rdi,[rip+0x115]        # 0x400714
   0x4005ff:	mov    eax,0x0
   0x400604:	call   0x4004c0 <printf@plt>
```

So, the first 3 instructions here **just deal with stack space**. `sub rsp,
0x20` just allocates some space on the stack (move the pointer down grows the
stack frame), and `mov QWORD PTR [rbp-0x8], rax` just copies the value we computed
an instruction ago in `rax` into the space we made.

But, what's with the calculation, this `mov rax, QWORD PTR fs:0x28`? Well, this
instruction specifically moves the the value at offset `0x28` from segment
`fs` to `rax`. The 4th instruction listed here clears the bottom values in 
`rax` (since `xor eax, eax` will make `eax` equal to 0, and `eax` is the lower
4 bytes in `rax`).

![](https://i.imgur.com/FfyzFMI.png)

**Note:** `fs` in this case stands for "file segment". We should just treat it
like a register I guess, since it looks like that. for the purposes here, it's
just creating a random value for us, which we'll store.

This feels **really weird**, but it's a actually something called a __**stack
canary**__. A stack canary is a piece of code used to do stack-overflow
protection by hashing and adding a random value to the begining of the stack
frame. 
