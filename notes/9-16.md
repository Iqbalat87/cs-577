---
title: 'Lecture Notes - Sept 16'
---

# Assembly Code (System-V x64)

Last time we talked about the ELF file and the binary format. We talked about 
the different segments (data and code), and how to find them. We also talked 
about the linking, and how linking works. We'll cover dynamic linking when we
talk more about runtime. Today, we'll talk a bit more about static things. 

![](https://i.imgur.com/NASS229.png)

The first step of reverse engineering is to reverse the binary back into its 
assembly form. Once we have the assembly, the logic starts being 
semi-human-readable. Usually you'd still want to go one step further, and 
reverse back to C source code, but assembly is the first step. 


## Example

![](https://i.imgur.com/26AzVAI.png)

```c
int main() {
  int a; int b;
  a = 1;
  b = 2;
  return a+b;
}
```

Let's say that this source code is compiled with the following command:

```shell
$ gcc -g -O0 -o hello
```

We have a few questions:

1. Why `-O0`?
  * **Answer:** `-O0` disables all of gcc's optimizations.
2. The binary we have here is usually much simpler than what we have in 
   practice. Why?
  * **Answer:** In practice, usually the binaries we use are compiled with 
    optimizations to make them a bit faster. The same source code might produce
    a more complicated looking binary. 
3. How do we get the assembly code from this ELF?
  * **Answer:** Use tools!

## Getting Assembly from ELF

![](https://i.imgur.com/pLD15XY.png)

We have 2 options:

1. Use `objdump`
  * Run `objdump -M att -d hello` in the command line
2. Use ghidra
  * Launch Ghidra
  * Open a new project
  * Import file (the binary you want, in our case `hello`
  * Run default tool


## How do these tools work internally?

![](https://i.imgur.com/ca5lqvk.png)

In the code segment, we have **machine code** or machine instructions. It's
literally 1's and 0's. The assembly code is a more human-readable version of
the machine code. Each instruction is translated back from 1's and 0's to the
instruction names they encode. 

The tools will look in the ELF file at the **Link View**. What we've been 
covering has is the **Load View** (the code / data stuff). This doesn't mean 
we're looking at different ELF files, and it *also* doesn't mean that we're at
a different stage of reverse engineering. The Link view is just like taking a 
peek at what's *in* the code and what's *in* the data, rather than just looking
at them as nameless faceless chunks.

![](https://i.imgur.com/fmZLrzs.jpg)

These tools will look in the `.text` section, which is where the code is held. 

## Reverse Engineering our `hello.c`

![](https://i.imgur.com/XjmB8FQ.png)

It's important to keep in mind the goal of our reverse engineering. 
- Are we trying to understand certain behaviors?
- Are we trying to change or modify certain behaviors? 

Now that we have a goal, we can start trying to achieve it:

1. Find the entry point (or the `main` function) from the outputs of `objdump`
   or Ghidra
2. Read the assembly code of the `main` function and follow child functions if
   there are any
3. Understand the meaning of the assembly code

### Step 1: How to find `main` in assembly

![](https://i.imgur.com/BKPrtuu.png)

1. Open Ghidra
2. Find function in assembly, click function
3. Type G, a window will pop up
   
   ![](https://i.imgur.com/BKPrtuu.png)

### Aside: How to obfuscate binary files

When compiling with gcc, you might compile with this command:

```
$ gcc -O0 -g hello.c
```

The `-g` flag adds in debugging symbols. When working with this, it makes things
easy to debug, but also makes things easy to reverse engineer. In the real
world, these debugging symbols will be stripped before the binaries are 
released. 

When these binaries are stripped, certain entries from the ELF's symbol table
are removed:

![](https://i.imgur.com/eQKgzx2.png)

### How to find `main` in assembly - cont.

With ghidra, we type `G` to search, and we type `main`. We can't find it, 
because the binary was obfuscated! So, we'll have to find some other way that
the entry point. 

However, we always know we can find the main function, because it's somewhere
in the code. If it wasn't, then this program wouldn't be able to execute its
main function. 

You can usually get the true entrypoint from the ELF header, and then you can
start tracing. For example, in our case, the entrypoint does some weird setup,
then calls `libc_start_main`:

![](https://i.imgur.com/0rCjFZQ.png)

Before calling it, it loads an address into the `RDI` register with `LEA`. This
is the address, `0x001005fa`, is actually the address of the main function!

### Step 2: Read assembly code and follow any child functions

![](https://i.imgur.com/RXzB95b.png)

### Step 3: Unserstand the meaning of the assembly code

![](https://i.imgur.com/mrdMlTM.png)

## Assembly code

#### First, some notes

- In this lecture and course, we'll focus on x86/x64. 
- We'll stick with System-V ABI, which is used frequently on linux.
- We'll follow the intel syntax (since this is the default syntax of objdump
  and Ghidra). The most common syntaxes are [intel and
  att](https://en.wikipedia.org/wiki/X86_assembly_language#Syntax).
  [Here](https://web.archive.org/web/20131003180256/http://www.ibm.com/developerworks/linux/library/l-gas-nasm/index.html),
  there is a more comprehensive comparison between intel and att syntax. 

### Structure of an instruction

![](https://i.imgur.com/le3zz8F.png)

**IMPORTANT:** In the intel syntax, the destination operand comes **before**
the source operand ~~since intel is fucking backwards~~. So, `mov rbp rsp` is
`rsp` &rarr; `rbp`. So, whenever you see a `mov`, think right-to-left, rather
than left-to-right (read in manga-mode, basically).

#### Opcodes

An opcode are like the 'functions' of assembly code. Each operand does a 
different thing. You can find a list of operands [here](http://ref.x86asm.net/).

![](https://i.imgur.com/B8OHk17.png)

#### Operands

If opcodes are the functions of assembly code, operands are the function 
*arguments*. There are quite a few different kinds of operads, including
immediates (which are like literals), registers, memory, etc.

##### Registers

Registers are fast storage units in CPUs. They all have unique names, and there
are only a handful of them, which means registers are typically used sparingly
in a program. Register use depends on the *calling convention*, which in our
course is System-V.

![](https://i.imgur.com/FsLbyPB.png)

When registers start with an r (e.g. `rax`), it means they're 64 bit registers. 
When they start with an e (e.g. `eax`), it means they're 32 bit registers. 

64-bit registers are 8 bytes. These registers typically support 32-bit variants
to be backwards compatible with 32 bit code:

![](https://i.imgur.com/YFyRZBn.png)

As you can see, the lower 4 bytes are used for `eax`. This is how backwards 
compatibility is achieved with `x64` and `x86`.

You might think the registers stop here, but 32 bit registers can be broken 
down into 16 bit register (e.g. `eax` &rarr; `ax` and `ah`):

![](https://i.imgur.com/yI9bgw0.png)

##### Memory

There are 3 types of memory-based operands:

- Simple mode: `[REG]`
  * This type accesses the memory at the address stored in `REG`.
  * e.g.: `mov [rax] 0x12` moves `0x12` into the memory location stored in `rax`.
- Simple mode w/ displacement: `[REG + Offset]`
  * This type accesses the memory at the address stored `Offset` away from `REG`.
  * e.g.: `[rax + 0x0]`
- Complete mode: `[REG1 + SIZE*REG2 + OFFSET]`
  * Accesses memory at address `REG1 + SIZE * REG2 + Offset`.
  * e.g. `[rax + rax*1 + 0x0]`
  * `REG1` is called the "base address", and `REG2` is called the "displacement".

##### Operand Sizes

Operands are sized! The size is determined by the instruction. With intel syntax,
the size of operands are determined in 2 ways:

1. If both operands are registers, we can determine the size of the operands. 
   `r`-style registers are 8 bytes, `e`-style are 4, etc.
2. If there is a memory-based operand, for instance, 
   `mov dword ptr [rbp-0x8], 0x1`, we deide with the word `dword` here, which 
   tells us that the memory address is a 'double word'. \
   **Note:** att syntax instead chooses to make the instruction itself carry
   the type. For example, `movd` can only work on double words. 

##### Implicit instructions

An instruction can have implicit operands

![](https://i.imgur.com/RpQa3YD.png)

## Can we reverse our "hellp" example now?

![](https://i.imgur.com/2XDmgw8.png)

Let's try to go through instruction-by-instruction and analyze the assembly 
code here.

Instruction 1: `push rbp`

- Semantic: `sub rsp, 0x8; mov [rsp], rbp;` => decrease `rsp` by `8` and then 
  move `rbp` to the memory location at `rsp`
- Why does the program do this?
- **Answer:** Well, we can see that this `push` is a stack operation! So, 
  `push rbp` is some kind of stack operation - maybe `rbp` has some important
  information, that the program needs to be doing what the source reads. However,
  usually these operations are inserted by the compiler to maintain the stack. 
  Values placed there can be tangentially related, or even not really related
  to the logic of the source code at all. So, for the purposes of reverse
  engineering, we can typically skip these instructions.

Instruction 2: `mov rbp, rsp`

- Semantic: Move `rsp` to `rbp`
- Why does the program do this?
- **Answer:** Well, this has to do with `rbp`, which we know was some stack
  thing. We can probably skip this instruction too, since it's probably not
  important to the core logic of the application.

Instruction 3: `mov DWORD PTR [rbp-0x8], 0x1`

- Semantic: Move the value `0x1` to the memory location from the address
  `rbp-0x8` (the size of the move is 4 bytes)
- Check the source code to see if you know why the assembly does this
- **Answer:** Well, there's a 1 in the source code (the `a = 1;` line), so maybe
  this is assigning a to 1 (which means `[rbp-0x8]` is where `a` is right now).

Instruction 4: `mov DWORD PTR [rbp-0x4], 0x2`

- Semantic: Move the value `0x2` to the memory location beginning from the 
  address `rbp-0x4` (the size of the move is 4 bytes)
- Check the source code to see if you know why the assembly does this
- **Answer:** This is very similar to instruction 3. This is probably the 
  `b = 2` line, which means `[rbp-0x4]` is where we're keeping `b`. 

Instruction 5: `mov edx, DWORD PTR [rbp-0x8]`

- Semantic: move the value at the memory location `[rbp-0x8]` to `edx`
- What is the value in `[rbp-0x8]`?
- **Answer:** It's `0x1`, since we moved it here at instruction 3, and no other
  instructions since then have changed it. 

Instruction 6: `mov edx, DWORD PTR [rbp-0x4]`

- Semantic: move the value at the memory location `[rbp-0x4]` to `edx`
- What is the value in `[rbp-0x4]`?
- **Answer:** It's `0x2`, since we moved it to this location at instruction 4.

Instruction 7: `add eax, edx`

- Semantic: add the values in `edx` and `eax`, and save the result in `eax` 
  (since that's the destination register in intel syntax).
- This instruction is pretty clear. It's the add operation just before the
  `return` in the source code.

Instruction 8: `pop rbp`

- Semantic: `mov rbp, [rsp]; add rsp, 0x8;` Move the value at the memory 
  location `[rsp]` to `rbp`, then increase `rsp` by `0x8`.

Instruction 9: `ret`

- Semantic: return from this function to the calling function. 

There's a difference between return in this assembly code and return in high
level source code. Here, it doesn't appear to return anything! Well, it does,
but the return instruction isn't storing the return value; it's only returning.

In this case, the return value is inside `rax`. The value in `rax` is actually
the result of the addition, since we didn't touch the first 4 bytes of `rax`,
they should still be zeroed out, and we *did* touch the last 4 bytes of `rax`
(since we stored the addition into `eax`). As a result, we effectively return
`rax` untouched.

`rax` is the return register in System-V. The register that's used to return
values depends on the calling convention / ABI, so this might differ on windows
or mac. 

 
