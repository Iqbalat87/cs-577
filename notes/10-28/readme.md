---
title: Lecture Notes - Oct 28
---

Today we'll be talking about the different memory exploits and what they look 
like at the binary level. 

## What memory vulnerabilities *look like*

### Stack Buffer Overflow

Source Code:

```c
int main(int argc, char Viargv)
{
  char small_buffer[16];
  read(0, small_buffer, 128);
  printf("%s\n'', small_buffer);
}

int win(void){
     printf("You win!\n");
     exit(0);
}
```

Assembly Code:

```asm
push   rbp
mov    rbp,rsp
sub    rsp,0x20
mov    DWORD PTR [rbp-0x14], edi
mov    QWORD PTR [rbp-0x20],rsi
lea    rax,[rbp-0x10]
mov    edx,0x80
mov    rsi,rax
mov    edi,0x0
mov    eax,0x0
call   400470 <read@plt>
lea    rax,[rbp-0x10]
mov    rdi,rax
call   400460 <puts@plt>
mov    eax,0x0
leave
ret
```

If you were just looking at the binary level, here's how you understand what's
going on. We're seeing `read` be called, so we should be interested in where
the data is being saved to. 

Here are the arguments being given to `read`:

<table>
<td>

```asm
mov    edx,0x80
mov    rsi,rax
mov    edi,0x0
; [...]
call   400470 <read@plt>
```
</td>
<td>

Arguments to `read`:

1. EDI = 0
2. RSI = RBP - 0x10
3. EDX = 0x80
</td>
</table>

So, we interpret this as saying we "read at most `0x80` bytes from file 
descriptor 0 (which is standard input), and then we save the data to the memory
location at `RBP - 0x10`. Let's do a bit of quick math using just the code to
understand how large the buffer at `rbp-0x10` is.

Because of the `sub rsp, 0x20`, we know there's `0x20` bytes allocated for this
stack frame. Our data supposedly starts at `rbp-0x10` (since `lea` performs a
memory address calculation, but doesn't actually access memory), and as always,
`rbp` is the pointer to the start of the stack frame. So, that means we must be
writing to a stack location that's `0x10` bytes long:

```math
\begin{aligned}
  \text{buffer size} &= \text{frame size} - \text{frame offset} \\
                     &= \texttt{0x20} - \texttt{0x10} \\
                     &= \texttt{0x10}
\end{aligned}
```

Here's a nicer diagram that shows the big picture of what we just calculated:

![](https://i.imgur.com/YcbDB1a.png)

Alright, so our buffer is `0x10` bytes long. However, we read *at most `0x80`*
bytes when we called `read`, remember? The data we write to the buffer could 
potentially be larger than the data itself - a classic buffer overflow. 

#### Tips to clue yourself on

- When you see a call to `read` with `edi = 0`, that means it's reading from
  standard input.
- Whenever the `edx` is larger than the buffer size, you have a stack overflow!
  * In this case, our `edx` was larger than every other literal in the program, 
    which was a bit of a giveaway. 
- Find the buffer size by using the size of the stack frame and the offset of
  the buffer from the frame.

### Heap Buffer Overflow

Here's an example of a heap buffer overflow:

```c
int main(int argc, char **argv)
{
   char *small_buffer;
   small_buffer = (char *)malloc(16);
   read(0, small_buffer, 128);
}
```

Here's the assembly for this BTW:

```asm
push   rbp
mov    rbp,rsp
sub    rsp,0x20
mov    DWOR0 PTR [rbp-0x14],edi
mov    QWOR0 PTR [rbp-0x20],rsi
mov    edi,0x10
call   400480 <malloc@plt>
mov    QWORD PTR [rbp-0x8],rax
mov    rax,QWORD PTR [rbp-0x8]
mov    edx,0x80
mov    rsi,rax
mov    edi,0x0
mov    eax,0x0
call   400470 <read@plt>
mov    rax,QWORD PTR [rbp-0x8]
mov    rdi,rax
call   400460 <puts@plt>
mov    eax,0x0
leave
ret
```

Heap buffer overflows are a bit easier to understand at the binary level to 
understand than stack overflows, since the grammars of the source and asm are
similar. You don't really see arrays with heap stuff, you just see pointers, 
which is what arrays boil down to in assembly anyways. 

When looking at these, we're interested in two things:

- the arguments that were given to `malloc`
- the arguments that were given to read

<table>
<td>

```asm
mov    edi,0x10
call   400480 <malloc@plt>
```
</td>
<td>

Arguments to `malloc`:

1. `edi`: `0x10`
</td>
</table>

<table>
<td>

```asm
mov    QWORD PTR [rbp-0x8],rax
mov    rax,QWORD PTR [rbp-0x8]
mov    edx,0x80
mov    rsi,rax
mov    edi,0x0
; [...]
call   400470 <read@plt>
```
</td>
<td>

Arguments to `read`:

1. `edi`: `0`
2. `rsi`: location returned by `malloc`
3. `edx`: `0x80`
</td>
</table>

So, it's pretty obvious that the location given by `malloc` is our buffer. We 
even get the buffer size served to us on a silver platter - `0x10`, the single
argument that was given to `malloc`.

Now, looking at `read`, we can once again see that we're reading from standard
input (`edi` is 0). We also see that we're reading at most `0x80` bytes, which
is again the largest literal in the source here. We know our buffer is `0x10` 
bytes, which means we have a buffer overflow.

### Integer Overflows

Integer overflows are kinda weird, but they usually involve functions that we
*know* have the possibility of overflowing. 

Here's the source for the example:

```c
int main() {
  unsigned int size;
  scanf("%i", &size);
  char *buf = alloca(size+1);
  int n = read(0, buf, size);
  buf[n] = '\0';
}
```

As some background, the function `scanf` takes 2 arguments: A format string, and
a bunch of memory locations to write things to. It reads from *standard input*,
and then writes what the user types to the buffers given to it. 

Here's the ASM;

```asm
push   rbp
mov    rbp,rsp
sub    rsp,0x10
lea    rax,[rbp-0x10]
mov    rsi,rax
lea    rdi,[rip+0xd7]        # 400694 <_IO_stdi
mov    eax,0x0
call   4004b0 <__isoc99_scanf@plt>
mov    eax,DWORD PTR [rbp-0x10]
add    eax,0x1
mov    eax,eax
mov    rdi,rax
call   4004a0 <malloc@plt>
```

Note that the ASM is only a stub - the `read` and other things after our `alloca`
have been ommitted, since the overflow already happens and we don't need to see
the rest. 

Let's take a look at what `scanf` is doing:

<table>
<td>

```asm
lea    rax,[rbp-0x10]
mov    rsi,rax
lea    rdi,[rip+0xd7]        # 400694 <_IO_stdi
mov    eax,0x0
call   4004b0 <__isoc99_scanf@plt>
```
</td>
<td>

Arguments to `scanf`:

1. `edi`: `rip+0xd7`, the location of the format string
2. `rsi`: `rax = rbp-0x10`, location to save input to
3. `eax`: 0, no vector registers as arguments
</td>
</table>

So this gives us some information of the location of the format string, the
location to save input to. If we had GDB running, we could check that the value
at `rip+0xd7` really was our format string, and that it *was* reading in an
integer. 

As a note, you really *should* be interested in what the format string is, since
that will determine the semantics. However, in this case we know it's reading an
integer in. 

So, the big important thing we learned is that `rbp-0x10` is the memory location
where `scanf` will write the integer to. 

Let's move on to the next chunk of code:

<table>
<td>

```asm
mov    eax,DWORD PTR [rbp-0x10]
add    eax,0x1
mov    eax,eax
mov    rdi,rax
call   4004a0 <malloc@plt>
```
</td>
<td>

The received input (now read in as an integer) gets incremented by 1, then is
used as the size argument to `malloc`. Registers are generally considered
unsigned.
</td>
</table>

So, here is the setup for the `malloc`. What's actually happening overall, then?
Well, since `scanf` doesn't take a size argument, we could write in whatever 
value we want - potentially a really big number. And we *know* this memory 
location is being treated like a number, because it's added to, and then it's 
given to `malloc` as a size argument.

So, if we write a value that exceeds 1 byte in size, `scanf` will just write 
the full value out, no matter how much space is needed. 

#### Tips to clue yourself on

- Look for `scanf`, or other "int reading" functions that don't have a read size
  argument. These functions are usually vulnerable. 
- Ensure that the memory after your integer location isn't cleared out or
  over-written before it's used. Otherwise, our overflow might actually be
  over-written by benign or developer-intended data.

### Signedness Mixups

```c
int main() {
          int size;
          char buf[16];
          scanf("%i", &size);
          if (size > 16) exit(1);
          read(0, buf, size);
}
```

As we can see by the source code, the problem is that we're using a signed 
int `size`, which we're reading in, but then we're using it as an *unsigned*
int (since the argument to `read` is a `sizet`, which is an unsigned int).

If we wanted, we could declare the integer itself to be negative. It won't trip
the `exit`, and it will be passed to `read`. Since signed numbers usually have
a 1 as their highest bit when they're negative, this value will be read as a
very high positive integer when interpreted as an unsigned integer by the code
in `read`. 

Here's the assembly:

```asm
push   rbp
mov    rbp,rsp
sub    rsp,0x20
lea    rax,[rbp-0x4]
mov    rsi,rax
lea    rdi,[rip+0xc7]        # 4006
mov    eax,0x0
call   400490 <__isoc99_scanf@plt>
mov    eax.DWORD PTR [rbp-0x4]
cmp    eax,0x10
jle    4005c9 <main+0x32>
mov    edi,0x1
call   4004a0 <exit@plt>
mov    edx,DWORD PTR [rbp-0x4] ; <main+0x32>
lea    rax,[rbp-0x20]
mav    rsi,rax
mav    edi,0x0
mov    eax,0x0
call   400480 <read@plt>
mov    eax,0x0
leave
ret
```

With this example, the signedness mixup can lead to buffer overflow; this case
is a bit special because the error can be easier identified w/ binary code.

I'm not going to go over how we work with this, because it's basically the same
analysis as integer overflows. However, the difference is here we're focusing on
the use of negative values to mess with things, rather than *just* controlling
the integer to overflow the buffer.

![](https://i.imgur.com/DJi7XVE.png)

### Uninitialized Memory

```c
int main() {
    int size;
    char * buffer = malloc(size);
    read(0, buffer, 128);
    printf("%s\n", buffer);
}
```

```asm
push   rbp
mov    rbp,rsp
sub    rsp,0x10
mov    eax,DWORD PTR [rbp-0x4]
cdqe
mov    rdi,rax
call   400480 <malloc@plt>
mov    QWORD PTR [rbp-0x10],rax
mov    rax,QWORD PTR [rbp-0x10]
mav    edx,0x80
mov    rsi,rax
mov    edi,0x0
mov    eax,0x0
call   400470 <read@plt>
mov    rax,QWORD PTR [rbp-0x10]
mov    rdi,rax
call   400460 <puts@plt>
mov    eax,0x0
leave
ret
```

<table>
<td>

```asm
mov    eax,DWORD PTR [rbp-0x4]
cdqe
mov    rdi,rax
call   400480 <malloc@plt>
```
</td>
<td>

The value inside `[rbp-0x4]` is never initialized. Then the value is passed as
the first argument to `malloc`.
</td>
</table>

### Use After Free

```c
int main() {
    int* size;
    size = malloc(sizeof(int));
    *size = 128;
    free(size);
    char * buffer = malloc(*size);
    read(0, buffer, 128);
}
```

```asm
push   rbp
mov    rbp,rsp
sub    rsp,0x10
mov    edi,0x4
call   400480 <malloc@plt>
mov    QWORD PTR [rbp-0x8],rax
mov    rax,QWORD PTR [rbp-0x8]
mov    DWORD PTR [rax],0x80
mov    rax,QWORD PTR [rbp-0x8]
mov    rdi,rax
call   400460 <free@plt>
mov    rax,QWORD PTR [rbp-0x8]
mov    eax,DWORD PTR [rax]
cdqe
mov    rdi,rax
call   400480 <malloc@plt>
mov    QWORD PTR [rbp-0x10],rax
mov    rax,QWORD PTR [rbp-0x10]
mov    edx,0x80
mov    rsi,rax
mov    edi,0x0
mov    eax,0x0
call   400470 <read_plt>
mov    eax,0x0
leave
```

![](https://i.imgur.com/VkLgNh6.png)

## How to find memory errors

Generally, there are 3 techniques:

- Manual analysis: looking at the code line-by-line to find patterns of errors. 
  This is basically what we were doing with the above examples.
- Fuzzing: Generating a ton of inputs to run on the binary to see if anything
  bad happens.
- Symbolic execution: enumerating every possible execution path to detect
  errors.

### Fuzzing

This approach involves generating a large amount of random unexpected inputs to
test the software for memory errors.

There are generally 3 types of fuzzing:

- black box: generated inputs are totally random, and not based on the target
  program at all
- grey box: generated inputs are somewhat based on the target program
- white box: generated inputs are completely derived from the target program

We'll focus on grey-box fuzzing, since it's easier to setup. 

**The tool we'll be using is America Fuzzy Lop
([afl](https://lcamtuf.coredump.cx/afl)).**

<!-- I ran the following to trim the lecture video to the demo:
ffmpeg -i memory-errors-2.mp4 -ss 1:06:40 -to 1:30:06 -c copy demo.mp4 
-->

![Demo](fuzzing-demo.mp4)

### Symbolic Execution

So symbolic execution usually uses some logic tool like a theorem prover to 
enumerate every possible execution path and search for errors. This involves
following the execution path until a conditional statement, then generate two
inputs to respectively satisfy the true branch *and* the false branch. 

```c
void test_me(int x, int y) (
  if (2*y == x) {
     if (x <= y+10) print("OK");
     else {
        print("something bad");
        ERROR;
     }
  } else print("OK");
}
```

![](https://i.imgur.com/bHL4OJd.png)

**The tool we'll use here is called [angr](https://github.com/angr/angr).**

![](https://i.imgur.com/dpOXuCR.png)

![Demo](angr-demo.mp4)

## Defenses against Memory errors.

There are tons of different defense solutions, but the two we'll talk about are
stack canaries and address layout randomization. 

### Stack Canaries

To fight buffer overflows into the return address, researchers introduced *stack
canaries*.

1. In function prologue, write a random value at the end of the stack frame.
2. In function epilogue, make sure this value is still intact.

They are very effective in general, but there *are* some situational bypass 
methods. 

1. Leak the canary (using some other vulnerability)
2. Brute force the canary (for forking processes)
   
   ```c
   int main() {
     char buf[16];
     while (1) {
       if (fork()) wait(0);
       else { read(0, buf, 128); return; }
     }
   }
   ```
3. Jumping the canary (if the situation allows)
   
   ```c
   int main() {
     char buf[16];
     int i;
     for (i = 0; i < 128; i++)
       read(0, buf + i, 1);
   }
   ```
   Depending on the stack layout, you can overwrite `i` and redirect the read to
   point to *after* the canary. 
   
### Address Space Layout Randomization

Memory corruption often focuses on corrupting *pointers* to point somewhere
else.

What if we randomize the location of code and data in memory? Maybe this'll make
it harder for the attacker to find something to point to.

When compiling, this is enabled by default, actually. If you don't want to have
it for whatever reason, you have to compile with `-no-pie` - PIE stands for
Position Independent Executable.
