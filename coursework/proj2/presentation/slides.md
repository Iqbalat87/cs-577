---
title: Course Project Presentation
author: Marcus Simpkins
date: 12/09/2020
---

<!-- Compiled with:
pandoc -t beamer -s slides.md -o slides.pdf
-->

# Binaries I created

### Category I

- `5hykxB_1`
- `5hykxB_2`
- `5hykxB_3`

### Category II

- `5hykxB_4`
- `5hykxB_5`

# How my binaries work

Compiled with `gcc -s` and didn't run anything else on binaries. I thought `gcc
-s` stripped, but apparently not. Lesson learned!

# How my binaries work
## Category I: `5hykxB_1`

The algorithm behind this first crackme is a loop that adds the high byte of
each character value in `lorem` to itself, effectively multiplying the ascii
value by 2:

```c
for (int i = 0; i < 10; i++) {
  char low = lorem[i] & 0xf;
  lorem[i] = low + lorem[i];
}

if (strcmp(lorem, argv[1]) == 0) goto success;
else goto fail;
```

**This results in a password of `XˆTJZRPVZZ`.**

# How my binaries work
## Category I: `5hykxB_2`

With this binary I wanted to force static analysis by using `ptrace` to prevent
gdb from attaching to my binary. 

With that in mind, I'm safe to construct my password! I start with the following
variables:

```c
// value will be over-written c;
char password[9] = "pAsSwOrD";
password[8] = '\0';
// 0x74 when i is 1, and 0x30 otherwise.
int base;
```

I initialized `password` to a human-readable value as a nice little red herring.
**I wanted my password to be `0x424242`**, so I perform a bit of number magic to
get each character in `pAsSwOrD` to map onto its corresponding character in
`0x424242`:

# How my binaries work
## Category I: `5hykxB_2`

```c
/* "pAsSwOrD" -> "0x424242":
 * [0] 'p'    -> 0x30 '0'
 * [1] 'A'    -> 0x78 'x'
 * [2] 's'    -> 0x34 '4'
 * [3] 'S'    -> 0x32 '2'
 * [4] 'w'    -> 0x34 '4'
 * [5] 'o'    -> 0x32 '2'
 * [6] 'r'    -> 0x34 '4'
 * [7] 'd'    -> 0x32 '2'
 * [8] '\0'   -> 0x00 '\0'
 **/
```

Since the ASCII values are so chaotic in my target password, I set `base` to a
different number so it's closer to its destination character. I also mess with
an `offset`, to actually *reach* the value of the destination character I 
want with `base + offset`. I then loop through `password` character-by-character
applying this algorithm and over-writing the old value of `pAsSwOrD`.


# How my binaries work
## Category II: `5hykxB_3`

With this binary, I attempted to use `dlsym` and `dlopen` to obfuscate calls to
`strcmp`: 

```c
void *handle = dlopen("/lib/...", RTLD_LAZY);
char strcmp_sym[7]; /* strcmp */
char strcmp_obf[] = { 0xf3, 0xf4 , /*... */};
memcpy(strcmp_sym, &strcmp_obf, 7 * sizeof(char));
// prevention of strings detection
for (int i = 0; i < 7; i++)
strcmp_sym[i] -= 0x80;

// create functions
int (*strcmp_)(const char *, const char *) = dlsym(handle, strcmp_sym);
```

This is a simple algorithm to reverse, all the characters of the function are
just ascii-shifted down by `0x80`. The idea is just that it'll slow people down.

# How my binaries work
## Category II: `5hykxB_3`

To this end, we also re-use the `ptrace` code from [`5hykxB_2`](#5hykxB-2) to
force static analysis, so they cant just jump in and snatch the value in GDB.

Now we can construct the password. I wanted to make the password be
`deadbeef`:

```c
/*    base: 60
 *   chars:  d   e   a   d   b   e   e   f
 *   ascii: 64  65  61  64  62  65  65  66
 * offsets:  4   5   1   4   2   5   5   6
 **/
```

As you can see, the ASCII values are in the `0x60`s range, so I can just add
different `offsets` from `0x60` based on how far along in the string I am.

# How my binaries work
## Category II: `5hykxB_4`

This crackme was really fun to write. It will search for a file that's larger
than itself, then it'll copy itself over top of the file it finds. This is meant
to mimic malware hiding & infecting behaviors.

One nice touch is that I made sure, when copying into the larger file, I copy to
the *front* of the file. That means that if you `chmod +x` the file that my
crackme chose to copy itself to, you could run it, and it'd try to find an even
*larger* file!

# How my binaries work
## Category II: `5hykxB_5`

The idea behind this program is that it will "encrypt" a file in the same
directory as it and then demand a ransom for access to the file. In reality,
this isn't a *real* encryption scheme - I actually just used a simple run-length
encoding scheme.

```c
int runlengthencoder(char *content, int size, char *out) {
  char *c = content; int i; int s;
  for (i = 0; i < size; s+=2) {
    int o;
    for (o = 1; *(c+o+i) == *(c+i) && i < size; o++);
    out[s] = o; out[s+1] = *(c+i);
    i += o;
  }
  return s;
}
```

# Binaries I cracked

I had to go for the "easier" ones, since I started kinda late. However, I did
have a really fun time cracking the binaries I cracked, so thanks to the
authors!

### Category I

- `0DKFyl_1`
- `0DKFyl_2`
- `0DKFyl_3`
- `ggpnd9_1`
- `ggpnd9_2`
- `ggpnd9_3`
- `hy58ZR_1`
- `hy58ZR_2`
- `hy58ZR_3`
- `iC2P8s_1`
- `iC2P8s_2`
- `iC2P8s_3`

# Binaries I cracked: Category II

- `0DKFyl_4`
- `ggpnd9_4`
- `ggpnd9_5`
- `hy58ZR_4`
- `hy58ZR_5`
- `iC2P8s_4`

# Cracking Byproducts: `ghidraCFG.py`

Contributed to
[`ghidraCFG.py`](https://gist.github.com/bin2415/15028e78d5cf0c708fe1ab82fc252799).

You can now generate really nice looking control-flow diagrams like this one:
    
![CFG Graph Example](../write-ups/as1wRi-1/cfg/main.svg)

# Cracking Byproducts: `angr-brute.py`

Created a bruteforcing script with angr called [`angr-brute.py`](https://gist.github.com/MyriaCore/3ccd713f1b48ec2c8db487504af1895b). 

This one was super fun to work on, and I got the sense that `angr` was really
powerful.  I couldn't get it to work, because I'm not really familiar enough
with the python library. 
