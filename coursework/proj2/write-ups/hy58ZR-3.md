---
title: hy58ZR-3
author: Marcus Simpkins
---

Here's ghidra's recompiled source code for this binary:

```c
int main(int argc,char **argv)
{
  int i;
  int sum;
  
  i = 0;
  sum = 0;
  do {
    sum = sum + argv[1][i];
    i = i + 1;
  } while (argv[1][i] != '\0');
  if (sum == 0xf1) {
    printf("Correct");
  }
  else {
    printf("Wrong");
  }
  return 0;
}
```

This binary is very small and very simple. The password is any string whose 
characters sum to `0xf1`, or decimal 241. Let's try the string `AAA.`:

```shell
$ ./hy58ZR_3 AAA.
Correct
```

Indeed, `AAA.` was a valid password for hy58ZR_3!
