---
title: iC2P8s-3
author: Marcus Simpkins
---

The main's pretty tiny:

```c
ulong main(undefined8 param_1,undefined8 param_2,undefined8 param_3,undefined8 param_4,
          undefined8 param_5,undefined8 param_6)
{
  long lVar1;
  
  lVar1 = ptrace(PTRACE_TRACEME,0,1,0,param_5,param_6,param_2);
  if (-1 < lVar1) {
    DAT_00302060 = FUN_00100908;
    DAT_00302068 = obfuscated_strcmp;
    mainloop();
  }
  else {
    printf("\nExiting debugging");
  }
  return (ulong)(-1 >= lVar1);
}
```

Not sure why the main has a billion inputs, but the interesting elements here
are the contents of the `if` statement. The outside is clearly an attempt to
thwart GDB users. Good thing I'm using Ghidra!

```c
void mainloop(void)
{
  __pid_t _Var1;
  long lVar2;
  long in_FS_OFFSET;
  undefined local_78 [104];
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  printf("\nEnter input : ");
  __isoc99_scanf(&DAT_00100ba6,local_78);
  lVar2 = (*(code *)PTR_strlen_00302018)(local_78);
  if (lVar2 == 0xf) {
    _Var1 = fork();
    if (_Var1 == 0) {
      (*DAT_00302060)(local_78);
    }
    else {
      (*DAT_00302068)(local_78);
    }
  }
  else {
    success_or_fail(2);
  }
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return;
}
```

That `success_or_fail` function is the *same* one we saw in `iC2P8s-2`. The
`fork` is also interesting, but I can't really think of what it'd be doing,
meaningfully. 

The real interesting bit is that When I tried to trace what the calls to
`DAT_00302060` and `DAT_00302068` were, my ghidra couldn't find them, since the
values weren't globally stored, apparently.

These happen to be the same data values that we set just before running
`mainloop`:

```c
DAT_00302060 = FUN_00100908;
DAT_00302068 = obfuscated_strcmp;
mainloop();
```

So let's check these functions out! `FUN_00100908` is a red herring, and doesn't
actually do anything other than loop a bit and update indecies. There isn't any
side-effecting going on that could affect execution outside of this function,
either, so it's just a dummy. 

`obfuscated_strcmp` is the real deal, though:

```c
void obfuscated_strcmp(undefined8 param_1)
{
  uint uVar1;
  
  uVar1 = (*(code *)PTR_strcmp_00302010)
                    (param_1,
                     PTR_s_WER8djTV8IahQPi_00302038,
                     param_1,
                     PTR_s_WER8djTV8IahQPi_00302038);
  success_or_fail((ulong)uVar1);
  return;
}
```

Once again, I'm gonna assume this `(*(code *)PTR_strcmp_00302010)` business is
some strange obfuscation or decompilation error, and we're just calling `strcmp`
here. This calls `success_or_fail` directly on the result too. Very promising.

So, what's this `PTR_s_WER8djTV8IahQPi_00302038` variable then? Well, it's a
global pointer to a string `"WER8djTV8IahQPi"`. So, I tried running on that:

```shell
$ ./iC2P8s_3

Enter input : WER8djTV8IahQPi

Bingo!
```

`WER8djTV8IahQPi` was indeed the password!
