---
title: hy58ZR-2
author: Marcus Simpkins
---

Here's the decompiled C source code for `hy58ZR-2`:

```c
/* WARNING: Could not reconcile some variable overlaps */

int main(int argc,char **argv)
{
  long lVar1;
  int retval;
  long in_FS_OFFSET;
  char masked;
  int i;
  char password [6];
  char mask [6];
  
  lVar1 = *(long *)(in_FS_OFFSET + 0x28);
  password._0_4_ = 0x426d416c;
  password._4_2_ = 0x4164;
  mask[0] = '\x02';
  mask[1] = '\x03';
  mask[2] = '\x02';
  mask[3] = '\x03';
  mask[4] = '\x05';
  mask[5] = '\x03';
  i = 0;
  do {
    if ((char)(mask[i] + password[i]) != argv[1][i]) {
      printf("Wrong");
      retval = 0;
      goto LAB_EXIT;
    }
    i = i + 1;
  } while ((password[i] != '\0') && (mask[i] != '\0'));
  printf("Correct");
  retval = 1;
LAB_EXIT:
  if (lVar1 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return retval;
}
```

So there's some complicated logic going on, but the important thing that's
happening here is that we're constructing a password and a mask, neither of
which are related to the user input. The do-while loop just checks character
by character - they might has well added them together and done a strcmp.

I kinda don't wanna have to reverse this algorithm by hand, so I'll just go in
GDB and take a peek around to see if I can construct the real password myself.

Ghidra says that the `i = 0` before the loop is at `0x5555555546ed`, so I'm just
gonna pop in there, set a breakpoint, and have it print `mask` and `password`.

After cross-referencing GDB output and Ghidra, I've determined that `password`
is located at `[rbp-0x14]`, and `mask` is located at `[rbp-0xe]`.

```gdb
(gdb) x/s ((char *)$rbp-0xe)
0x7fffffffe172:	"\002\003\002\003\005\003"
(gdb) x/s ((char *)$rbp-0x14)
0x7fffffffe16c:	"lAmBdA\002\003\002\003\005\003"
(gdb) 
(gdb) # Printing the value of the string w/ each character's ascii value added
(gdb) # together: 
(gdb) set $i = 0
(gdb) while (*((char *)$rbp-0xe+$i) != '\0' && *((char*)$rbp-0x14+$i) != '\0')
 >printf "\\x%02x", *((char*)$rbp-0xe+$i) + *((char*)$rbp-0x14+$i)
 >if (*((char *)$rbp-0xe+$i+1) == '\0' || *((char*)$rbp-0x14+$i+1) == '\0')
  >printf "\n"
  >end
 >set $i = $i + 1
 >end
\x6e\x44\x6f\x45\x69\x44
```

And, `\x6e\x44\x6f\x45\x69\x44` is indeed the password:

```shell
$ ./hy58ZR_2 $(echo -ne '\x6e\x44\x6f\x45\x69\x44')
Correct
```
