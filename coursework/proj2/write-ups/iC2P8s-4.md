---
title: iC2P8s-4
author: Marcus Simpkins
---

This is a really long source file but I you really just have to slog through it
cause not much of it is boilerplate. 

Let's take this main function in chunks. 



```c
int in_fd;
int out_fd;
int fxstat_result;
long idx;
undefined8 retval;
ssize_t send_result;
undefined8 *buf_it;
long in_FS_OFFSET;
byte zero;
off_t send_offset;
undefined8 buf [6];
size_t num_bytes;
char input [104];
long local_10;
                                                           
zero = 0;
local_10 = *(long *)(in_FS_OFFSET + 0x28);
idx = ptrace(PTRACE_TRACEME,0,1,0,param_5,param_6,param_2);
if (idx < 0) {
  printf("\nExiting debugging");
  retval = 1;
} else {
  // ... logic continues
}
```

So this just sets up a `PTRACE` so we can't use GDB. No biggie. Let's look at
that else statement:

```c
printf("\nEnter input : ");
__isoc99_scanf(&DAT__s,input);
in_fd = open(input,0);
if (in_fd == -1) {
  puts("\nPlease run me again!");
  retval = 0;
} else {
  // ... logic continues
}
```

So this takes in the user input into the char array `input`. We then call
[`open`](https://linux.die.net/man/2/open) to open a file descriptor with
`input` being the filename for the file we open. 

`open`'s second argument is an access mode dictating if we're gonna read, write,
or do both. I figured the value for 0 was read, but I wanted to be sure, so I
checked the source where this comes from,
[`fcntl.h`](https://github.com/torvalds/linux/blob/master/include/uapi/asm-generic/fcntl.h),
and sure enough, [I was
right](https://github.com/torvalds/linux/blob/cd796ed3345030aa1bb332fe5c793b3dddaf56e7/include/uapi/asm-generic/fcntl.h#L20).

```c
out_fd = creat("temp",0x1b0);
if (out_fd == -1) {
  puts("\nPlease run me again!");
  retval = 0;
} else {
  // ... logic continues
}
```

If opening our user-specified input file didn't fail, we then call `creat`,
which, [according to its man page](https://linux.die.net/man/2/creat), opens and
"possibly creates" a file or device. So, we're probably creating a file called
`temp`, here.

The second argument to `creat` is a mode, specifying if we want to create the
file if it doesn't exist, or if we want to specify directories, etc. I'm not
really sure what our value of `0x1b0` means, but since it's `creat` I'm hoping
it's just gonna be analogous to `O_CREAT`.

```c
send_offset = 0;
idx = 0x12;
buf_it = buf;
while (idx != 0
            /* WARNING: this loop does nothing */) {
  idx = idx + -1;
  *buf_it = 0;
  buf_it = buf_it + (ulong)zero * 0x1ffffffffffffffe + 1;
}
fxstat_result = obfuscated_fxstat();
if (fxstat_result == -1) {
  puts("\nPlease run me again!");
  retval = 0;
} else {
  // ... logic continues    
}
```

If *that* didn't fail, then we enter this loop that doesn't do anything, and
then we call this function, `obfuscated_fxstat`. Here's the function definition:

```c
void obfuscated_fxstat(int param_1,stat *param_2)
{
  __fxstat(1,param_1,param_2);
  return;
}
```

Now I had never heard of `fxstat` before, and it's not on any man pages, but when
I looked it up found some [source 
code](https://code.woboq.org/userspace/glibc/sysdeps/unix/sysv/linux/wordsize-64/fxstat.c.html)
which appears to just be calling `fstat` on the passed arguments. Weird.

Weirder still is the fact that we call the outer function, `obfuscated_fxstat`,
without any arguments. I'm sure this is some kinda error, but let's just say
this checks the file for errors and leave it at that.

```c
send_result = sendfile(out_fd,in_fd,&send_offset,num_bytes);
if ((int)send_result == -1) {
  puts("\nPlease run me again!");
  retval = 0;
} else {
  // ... logic continues
}
```

So if `obfuscated_fxstat` likes what it sees, we get to call `sendfile`, a
function that, according to [its man
page](https://linux.die.net/man/2/sendfile), copies data from one file
descriptor to another. We're giving git a *lot* of uninitialized variables tbh,
which is weird, especially w/ `num_bytes`, but if all goes well this should
*just* be copying data from our input file to the temp file we generated just a
bit ago.

```c
in_fd = close(in_fd);
if (in_fd == -1) {
  puts("\nPlease run me again!");
  retval = 0;
}
else {
  in_fd = close(out_fd);
  if (in_fd == -1) {
    puts("\nPlease run me again!");
    retval = 0;
  }
  else {
    puts("\nGuess what I did :)");
    retval = 0;
  }
}
```

Finally, if all goes well, we close the file descriptors and print a cheeky
little taunt.

---

So we *did* kinda reveal the logic, but we really went in depth, so at a high
level, what's this binary's secret behavior? Well, this binary allows the user
to give it a file, then it copies the data from that file to a file called
`temp`, that it potentially creates if it doesn't exist.

Let's test this out ourselves in the shell!

```shell
$ cat << EOF > test
> this is a
> test file
> EOF
$ ./iC2P8s_4 

Enter input : test

Guess what I did :)
$ cat temp
this is a
test file
```

Yep, does *exactly* as we expect!
