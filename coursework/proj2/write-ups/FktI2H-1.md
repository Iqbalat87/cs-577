---
title: FktI2H-1
author: Marcus Simpkins
---

Thankfully, this binary looks much easier to reverse than the other binaries. At
first I thought it was a type I binary cause there are a lot of prints, but now
I think it's a type II:

```shell
$ ./FktI2H_1 
what is my secret?
x
Challenge me if you can!
$ ./FktI2H_1 
what is my secret?
Bingf
Challenge me if you can!
```

Here's the `strings` output:

```shell
$ strings FktI2H_1 
[...]
u+UH
what is H
my secreH
Bingf
[]A\A]A^A_
Challenge me if you can!
[...]
```
</details>


Unfortunately for me, this is a bit more difficult, because it turns out this
program has the exact same super weird AF structure as [`c4y8c7-1`](c4y8c7-1).
I'm starting to think that this is just a type of binary that Ghidra fails to
analyze. 

I guess since I'm gunning it alone, the least I can do is search for those 
strings. When I searched for strings I get this:

![](https://i.imgur.com/yWwI0nf.png)

This bit of the assembly apparently makes up the `what is my secret` printout:

```asm
MOV RAX, 0x2073692074616877
MOV RDX, 0x657263657320796d
```

### Function Creation

Eventually I discovered I could right click w/ ghidra to correct the bounds of
the function when ghidra guesses them wrong. Fun. Here's our `main`, then:

```c
undefined8 main(void)
{
  char cVar1;
  int iVar2;
  size_t sVar3;
  long in_FS_OFFSET;
  int local_90;
  int local_8c;
  undefined8 local_78;
  undefined8 local_70;
  undefined2 local_68;
  undefined local_66;
  char local_58 [32];
  undefined4 local_38;
  undefined2 local_34;
  undefined local_32;
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  local_78 = 0x2073692074616877;
  local_70 = 0x657263657320796d;
  local_68 = 0x3f74;
  local_66 = 0;
  puts((char *)&local_78);
  __isoc99_scanf(&DAT_00102004,local_58);
  local_38 = 0x676e6942;
  local_34 = 0x216f;
  local_32 = 0;
  sVar3 = strlen(local_58);
  iVar2 = (int)sVar3;
  sVar3 = strlen(local_58);
  if (9 < sVar3) {
    sVar3 = strlen(local_58);
    if (sVar3 < 0x21) {
      local_90 = 0;
      while (local_90 < iVar2 + -1) {
        local_8c = 0;
        while (local_8c < (iVar2 - local_90) + -1) {
          if (local_58[local_8c + 1] < local_58[local_8c]) {
            cVar1 = local_58[local_8c];
            local_58[local_8c] = local_58[local_8c + 1];
            local_58[local_8c + 1] = cVar1;
          }
          local_8c = local_8c + 1;
        }
        local_90 = local_90 + 1;
      }
      if ((int)local_58[0] + (int)local_58[iVar2 + -1] == (int)local_58[iVar2 + -2] * 2 + -1) {
        puts_thunk(&local_38);
      }
      else {
        challengeMe();
      }
      goto LAB_001013af;
    }
  }
  challengeMe();
LAB_001013af:
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return 0;
}
```
</details>

That would've been pretty good to know before, tbh. 
