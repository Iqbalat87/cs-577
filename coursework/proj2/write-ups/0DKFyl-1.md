---
title: 0DKFyl-1
author: Marcus Simpkins
---

So I loaded this into GDB to find the start address, which was
`0x0000555555554238`, and then went into ghidra and found the source code for
main:

```c
int main(int argc,char **argv)
{
  int retval;
  int local_1c;
  int local_18;
  int local_14;
  int local_10;
  int local_c;
  
  if (argc == 2) {
    local_1c = 0;
    local_18 = 0;
    local_14 = 0;
    local_10 = 0;
    while (argv[1][local_1c] != '\0') {
      local_1c = local_1c + 1;
    }
    if (local_1c == 0x20) {
      local_c = 0x10;
      while (local_18 < 0x10) {
        local_14 = local_14 + argv[1][local_18];
        local_18 = local_18 + 1;
      }
      while (local_c < 0x20) {
        local_10 = local_10 + argv[1][local_c];
        local_c = local_c + 1;
      }
      if (local_14 == local_10) {
        puts("correct nice job.");
        retval = 0;
      }
      else {
        puts("incorrect string try again.");
        retval = 1;
      }
    }
    else {
      puts("incorrect string try again.");
      retval = 1;
    }
  }
  else {
    puts("needs exactly 1 argument.");
    retval = -1;
  }
  return retval;
}
```

The address for line 28 was `0x0000555555555b26` according to ghidra. My plan
was to then load this up in GDB and examine the values of `local_10` and
`local_14`, however both appeared to be based on the input, so it's not like
this program is building a password and then analyzing it. 

Looking at this program more closely, it's clear that these locals are arranged
in such a way to be a 4 byte array. The lower 2 bytes, `local_14` and
`local_10` are used in the two while loops, and then checked against each other
when getting to the `correct, nice job` printout. 

So, to make things clearer, I retyped these variables into a 5 byte array:

```c
int main(int argc,char **argv)
{
  int retval;
  int nums [5];
  
  if (argc == 2) {
    nums[0] = 0;
    nums[1] = 0;
    nums[2] = 0;
    nums[3] = 0;
    while (argv[1][nums[0]] != '\0') {
      nums[0] = nums[0] + 1;
    }
    if (nums[0] == 0x20) {
      nums[4] = 0x10;
      while (nums[1] < 0x10) {
        nums[2] = nums[2] + argv[1][nums[1]];
        nums[1] = nums[1] + 1;
      }
      while (nums[4] < 0x20) {
        nums[3] = nums[3] + argv[1][nums[4]];
        nums[4] = nums[4] + 1;
      }
      if (nums[2] == nums[3]) {
        puts("correct nice job.");
        retval = 0;
      }
      else {
        puts("incorrect string try again.");
        retval = 1;
      }
    }
    else {
      puts("incorrect string try again.");
      retval = 1;
    }
  }
  else {
    puts("needs exactly 1 argument.");
    retval = -1;
  }
  return retval;
}
```

So here we can see that `nums[0]` is being used as an index in the first loop, 
which counts the number of characters in `argv[1]`. Then, an `if` statement 
checks that the number of characters is `0x20`, or decimal 32, which means we
know the password is 32 characters long.

Next we have the following logic:

```c
nums[4] = 0x10;
while (nums[1] < 0x10) {
  nums[2] = nums[2] + argv[1][nums[1]];
  nums[1] = nums[1] + 1;
}
while (nums[4] < 0x20) {
  nums[3] = nums[3] + argv[1][nums[4]];
  nums[4] = nums[4] + 1;
}
if (nums[2] == nums[3]) {
  puts("correct nice job.");
  retval = 0;
}
else {
  puts("incorrect string try again.");
  retval = 1;
}
```

So when this code runs, everything in `nums[1:]` is zeroed out. In the first
loop, `nums[1]` is used as the index, and in the second loop, `nums[3]` is. 
In the first and second loop, `nums[2]` and `nums[3]` are written to, and later
compared to decide whether to print correct or incorrect. 

The byproduct of this behavior is that `nums[2]` scans through the first `0x10`
characters of the input, and `nums[3]` scans through the last `0x10` characters
of the input. Both these variables are produced by summing up the ascii ranges,
which means the password is any `0x20` character string where the ascii values
of the first and second half of the string add up. This means `'A'*32` should
work as a password.

And, indeed it does:

```shell
$ ./0DKFyl_1 AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
correct nice job.
```

