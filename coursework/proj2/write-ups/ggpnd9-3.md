---
title: ggpnd9-3
author: Marcus Simpkins
---

This binary has a very similar structure to `ggpnd9-2`:

```c
int main(int argc,char **argv)
{
  uint fib;
  int retval;
  char str [360];
  
  if (argc == 2) {
    fib = fib(0x2d);
    snprintf(str,0xb,"%d",(ulong)fib);
    retval = strcmp(argv[1],str);
    if (retval == 0) {
      succeed(argv[1]);
    }
    else {
      fail(argv[1]);
    }
    retval = 0;
  }
  else {
    printf("Exactly one argument required.");
    retval = -1;
  }
  return retval;
}
```

We'll use the same style of analysis as in `ggpnd9-2`, as well. Ghidra says that
the `strcmp` is at `0x555555555368`, so let's just jump into ghidra and examine
the variable that's being given to `strcmp`:

```gdb
(gdb) x/s $rsi
0x7fffffffe020:	"1134903170"
```

And sure enough, `1134903170` is the correct password:

```shell
$ .ggpnd9_3 1134903170
Yes, 1134903170 is correct!
```
