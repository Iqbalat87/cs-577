---
title: 0DKFyl-3
author: Marcus Simpkins
---

Alright, I'm gonna skip *right* to the ghidra decompilation here:

```c
int main(int argc,char **argv)
{
  char *input;
  int retval;
  size_t inputlen2_temp;
  long lidx;
  int inputlen;
  int asciisum;
  int idx;
  int inputlen2;
  
  if (argc == 2) {
    inputlen = 0;
    asciisum = 0;
    input = argv[1];
    while (input[inputlen] != '\0') {
      asciisum = asciisum + input[inputlen];
      inputlen = inputlen + 1;
    }
    if (inputlen == 0x20) {
      if ((int)*input == asciisum / 0x20) {
        puts("incorrect string try again.");
        retval = 1;
      }
      else {
        idx = 0;
        inputlen2_temp = strlen(input);
        inputlen2 = (int)inputlen2_temp;
        do {
          inputlen2 = inputlen2 + -1;
          if (inputlen2 <= idx) {
            puts("correct nice job.");
            return 0;
          }
          lidx = (long)idx;
          idx = idx + 1;
        } while (input[lidx] == input[inputlen2]);
        puts("incorrect string try again.");
        retval = 1;
      }
    }
    else {
      puts("incorrect string try again.");
      retval = 1;
    }
  }
  else {
    puts("needs exactly 1 argument.");
    retval = -1;
  }
  return retval;
}
```

There's a lot of weird type conversion going on here, but I think that's pretty
much just ghidra being weird about things. 

So already we know the input length has to be `0x20` or decimal 32, cause of
the first if-statement. In the while-loop previous we also summed up the ascii
characters of the input:

```c
while (input[inputlen] != '\0') {
  asciisum = asciisum + input[inputlen];
  inputlen = inputlen + 1;
}
```

One other interesting check is to make sure that the average ascii value of the
string:

```c
if ((int)*input == asciisum / 0x20) {
  puts("incorrect string try again.");
  retval = 1;
}
```

Ultimately, it's the stuff in the do-while loop that we care about:

```c
do {
  inputlen2 = inputlen2 + -1;
  if (inputlen2 <= idx) {
    puts("correct nice job.");
    return 0;
  }
  lidx = (long)idx;
  idx = idx + 1;
} while (input[lidx] == input[inputlen2]);
puts("incorrect string try again.");
retval = 1;
```

So the first thing we do is subtract `inputlen2` by 1, which makes sense since
we use it to loop backwards through the input. Then we have this other variable,
`idx` and `lidx`, that we're using to loop forwards through the input. The while
loop checks to see that the ascii value at the backwards-looping index is the 
same as the one at the forwards-looping index. 

The `if` statement is tripped when these two indices meet in the middle. This 
serves as our final requirement. What this means is that our input must also 
be a palindrome. 

---

So, we need to construct a 32-character palindrome whose first character isn't
the average ascii value of the string. Let's just be silly and go with
`LAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAL`, since `L` should be far enough away from `A`
to pull away from the average. 

Sure enough, this works:

```shell
$ ./challenge-pool/0DKFyl_3 LAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAL
correct nice job.
```
