---
title: hy58ZR-1
author: Marcus Simpkins
---

This one was ridiculously easy. Here's what Ghidra thinks:

```c
int main(int argc,char **argv)

{
  int iVar1;
  
  iVar1 = strcmp(argv[1],"yes");
  if (iVar1 == 0) {
    printf("Correct");
  }
  else {
    printf("Wrong");
  }
  return 0;
}
```

`yes` is literally the password:

```shell
$ ./hy58ZR_1 yes
Correct
```

