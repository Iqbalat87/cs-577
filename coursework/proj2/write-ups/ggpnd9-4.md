---
title: ggpnd9-4
author: Marcus Simpkins
---

This is the first Category II binary that I'm attempting. Here's the decompiled
C source curtesy of Ghidra:

```c
int main(void)
{
  int iVar1;
  FILE *__stream;
  FILE *__stream_00;
  char filename [100];
  FILE *fptr2;
  FILE *fptr1;
  char c;
  
  __stream = fopen("/proc/net/tcp","r");
  if (__stream == (FILE *)0x0) {
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  __stream_00 = tmpfile();
  iVar1 = fgetc(__stream);
  c = (char)iVar1;
  while (c != -1) {
    fputc((int)c,__stream_00);
    iVar1 = fgetc(__stream);
    c = (char)iVar1;
  }
  fclose(__stream);
  fclose(__stream_00);
  return 0;
}
```

So here we can see that the "secret behavior" on display is actually pretty
simple.  The program is reading from `/proc/net/tcp`, and copying the contents
of the file to a tempfile via
[`tmpfile()`](https://linux.die.net/man/3/tmpfile).

We can actually see the reads and writes by calling `strace` on this binary:

```shell
$ strace ./challenge-pool/ggpnd9_4
execve("./ggpnd9_4", ["./ggpnd9_4"], 0x7fffc7204e50 /* 26 vars */) = 0
/* ... Omitted ... */
openat(AT_FDCWD, "/proc/net/tcp", O_RDONLY) = 3
openat(AT_FDCWD, "/tmp", O_RDWR|O_EXCL|O_TMPFILE, 0600) = 4
fcntl(4, F_GETFL)                       = 0x418002 (flags O_RDWR|O_LARGEFILE|O_TMPFILE)
fstat(3, {st_mode=S_IFREG|0444, st_size=0, ...}) = 0
read(3, "  sl  local_address rem_address "..., 1024) = 1024
fstat(4, {st_mode=S_IFREG|0600, st_size=0, ...}) = 0
read(3, "0 -1                     \n", 1024) = 26
read(3, "", 1024)                       = 0
close(3)                                = 0
write(4, "  sl  local_address rem_address "..., 1050) = 1050
close(4)                                = 0
exit_group(0)                           = ?
+++ exited with 0 +++
```

