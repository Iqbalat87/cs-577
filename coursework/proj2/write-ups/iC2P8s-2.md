---
title: iC2P8s-2
author: Marcus Simpkins
---

The main here is actually pretty big, so what I'm gonna do is I'm gonna go
through it in chunks. Here's the first chunk:

```c
bool bVar1;
int looplen;
int strlen;
long in_FS_OFFSET;
int idx;
char input [104];
long local_10;

local_10 = *(long *)(in_FS_OFFSET + 0x28);
printf("\nEnter input : ");
__isoc99_scanf(&DAT__S,input);
idx = 0;
while (looplen = idx, input[idx] != '\0') {
  idx = idx + 1;
}
```

This just does some initialization of variables, and reads the users password
from STDIN. I've never actually seen that while loop syntax before, but
apparently the `looplen = idx` is being run at the beginning of each loop, so by
the end the `looplen` would be roughly the length of the user's input.

```c
idx = 0;
bVar1 = true;
strlen = (*(code *)PTR_strlen_00301018)(PTR_s_HdwVkpAhlD_G_ZZe_00301010);
```

This is just some variable initialization before we get to the first loop! So I
don't actually know what this `PTR` business is about since I've not actually
seen this before, but I assume it's calling `strlen`.

```c
do {
  if (looplen <= idx) {
LAB_001008bb:
    if (bVar1) {
      if (looplen == strlen) {
        success_or_fail(0);
      }
      else {
        success_or_fail(1);
      }
    }
    else {
      success_or_fail(1);
    }
    if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                  /* WARNING: Subroutine does not return */
      __stack_chk_fail();
    }
    return 0;
  }
  if (strlen <= idx) {
    bVar1 = false;
    goto LAB_001008bb;
  }
  if ((char)((char)idx + PTR_s_HdwVkpAhlD_G_ZZe_00301010[idx]) == input[idx]) {
    idx = idx + 1;
  }
  else {
    bVar1 = false;
    idx = idx + 1;
  }
} while( true );
```

Alright, this is the main loop. The success state is actually the
`success_or_fail(0)` call:

```c
void success_or_fail(int param_1)
{
  if (param_1 == 0) {
    puts("\nBingo!");
  }
  else {
    puts("\nChallenge me if you can!");
  }
  return;
}
```

So, my first guess is to check out this `PTR_s_HdwVkpAhlD_G_ZZe_00301010`
variable, since that seems to come up a lot. Not gonna show any screenshots, but
in Ghidra that's actually a global pointer to a string `"HdwVkpAhlD[G]ZZe"`. 

So, it *looks* like these two strings are meant to be equal. Let's try that!

```shell
$ ./iC2P8s_2

Enter input : HdwVkpAhlD[G]ZZe

Challenge me if you can!
```

No such luck. Well, this *was* wishful thinking. The stuff at the top of the
loop only runs once the `idx` has gone through to transform the input. Here's
what that transformation looks like:

```c
if (strlen <= idx) {
  bVar1 = false;
  goto LAB_001008bb;
}
if ((char)((char)idx + PTR_s_HdwVkpAhlD_G_ZZe_00301010[idx]) == input[idx]) {
  idx = idx + 1;
}
else {
  bVar1 = false;
  idx = idx + 1;
}
```

If we want out input to be accepted, we have to fail `strlen <= idx` at each
each loop iteration, and always succeed `(char)((char)idx +
PTR_s_HdwVkpAhlD_G_ZZe_00301010[idx]) == input[idx]` at each loop iteration.

So the *true* password is gonna be `HdwVkpAhlD[G]ZZe`, where the ascii value of
each character is shifted up by its position. Using python, we can see the result
of that will be `HeyYouGotMeRight`:

```shell
$ python -c 'print("".join([chr(ord(c)+i) for i,c in enumerate("HdwVkpAhlD[G]ZZe")]))'
HeyYouGotMeRight
```

And this is indeed the password:

```shell
$ ./iC2P8s_2

Enter input : HeyYouGotMeRight

Bingo!
```
