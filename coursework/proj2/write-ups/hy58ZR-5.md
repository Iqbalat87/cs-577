---
title: hy58ZR-5
author: Marcus Simpkins
---

The secret behavior for `hy58ZR-5` is dead simple:

```c
int main(void)
{
  FILE *__stream;
  FILE *fp;
  
  __stream = fopen("crake04_file","w");
  fclose(__stream);
  return 0;
}
```

Literally just opens the file `crake04_file` for writing, then immediately
closes it.

You can see this for yourself by testing in the shell:

```shell
$ ls
challenge-pool  completed-binaries.svg  fauxware  readme.md  scratch.c  scripts  write-ups
$ ./challenge-pool/hy58ZR_5 
$ ls
challenge-pool  completed-binaries.svg  crake04_file  fauxware  readme.md  scratch.c  scripts  write-ups
$ cat crake04_file 
```

