---
title: zwcQY9-1
author: Marcus Simpkins
---

Here's the decompiled source for this binary:

```c
int main(int argc,char **argv)
{
  int cmp_result;
  char sprint_buf [2];
  char password [13];
  int idx;
  char *bingo;
  
  if (argc == 2) {
    bingo = "Bingo!";
    idx = 0;
    while (*bingo != '\0') {
      sprintf(sprint_buf,"%x",(ulong)(uint)(int)*bingo);
      password[idx] = sprint_buf[0];
      password[idx + 1] = sprint_buf[1];
      idx = idx + 2;
      bingo = bingo + 1;
    }
    password[12] = '\0';
    cmp_result = strcmp(password,argv[1]);
    if (cmp_result == 0) {
      puts("Bingo!");
    }
    else {
      puts("Challenge me if you can!");
    }
  }
  else {
    puts("Requires 1 argument.");
  }
  return 0;
}
```

Because `password` isn't based on the user input, instead of reversing this
algorithm, I'm just gonna take a peek in GDB and see if I can grab the password
myself. Ghidra tells me that the `strcmp` happens at `0x555555555200`:

```gdb
(gdb) x/s $rdi
0x7fffffffe167:	"42696e676f21"
```

`42696e676f21` is indeed the password:

```shell
$ ./zwcQY9_1 42696e676f21
Bingo!
```
