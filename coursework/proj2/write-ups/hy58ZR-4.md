---
title: hy58ZR-4
author: Marcus Simpkins
---

This binary is the first Category II binary from the author `hy58ZR`! It turns
out it's pretty simple:

```c
int main(int argc,char **argv)
{
  int i;
  int sum;
  
  i = 100;
  sum = 0;
  do {
    sum = sum + i;
    i = i + -1;
  } while (i != 0);
  printf("the result is %d",(ulong)(uint)sum);
  return 1;
}
```

As we can see, this just sums up the numbers from 0 to 100. The result is 5050 -
we'll get this result every time.
