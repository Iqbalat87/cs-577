---
title: 0DKFyl-4
author: Marcus Simpkins
---

Here's the decompiled C source from Ghidra:

```c
undefined8 main(void)
{
  char *timestr;
  size_t timestrlen;
  long in_FS_OFFSET;
  int idx;
  time_t rawtime;
  tm *timebuf;
  FILE *timefile;
  char ascii_bumped_timestr [32];
  char dup_ascii_bumped_timestr [40];
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  time(&rawtime);
  timebuf = localtime(&rawtime);
  timestr = asctime(timebuf);
  strcpy(ascii_bumped_timestr,timestr);
  timestr = asctime(timebuf);
  timestrlen = strlen(timestr);
  idx = 0;
  while (idx < (int)timestrlen) {
    ascii_bumped_timestr[idx] = ascii_bumped_timestr[idx] + '\x01';
    idx = idx + 1;
  }
  strcpy(dup_ascii_bumped_timestr,ascii_bumped_timestr);
  timefile = fopen("2-1.txt","w");
  fprintf(timefile,"%s\n",dup_ascii_bumped_timestr);
  fclose(timefile);
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return 0;
}
```

So there's a lot of weirdness going on, but basically this just gets the current
time, calls `asctime` to convert it to a timestring in the same format as
`ctime` (which is what the `date` program returns). The ascii values for each
character are bumped by 1, and then written to a file `2-1.txt`.

I wrote a simple awk program to simulate what the value of this text file might
be:

```shell
$ date | awk -i ord.awk '{
>   split($0, chars, "")
>   for (i=1; i <= length($0); i++) {
>     printf("%s", chr(ord(chars[i])+1))
>   }
>   printf("\n")
>   print $0
> }'
Npo!Efd!!8!28;58;36!FTU!3131
Mon Dec  7 17:47:25 EST 2020
$ date && ./0DKFyl_4 && cat 2-1.txt
Mon Dec  7 17:49:25 EST 2020
Npo!Efd!!8!28;5:;36!3131
```

As we can see, the results are pretty much as what we'd expect. 
