#!/usr/bin/env python3

# Also hosted on Gist:
# https://gist.github.com/MyriaCore/3ccd713f1b48ec2c8db487504af1895b
# 
# This is a tool I'm developing to solve "Category 1 binaries" in my Reverse
# Engineering Course.
# 
# A "category 1 binary" is a binary that takes a password as its first argument,
# and will print a "success" string or a "fail" string depeneding on if your
# argument was a valid password.
# 
# The goal of this script is to bruteforce the password to one of these binaries
# using angr's symbolic execution.

import sys
import time

# globals
binary = None         # path to binary
success_output = None # success output
password_length = 20  # length of password

# find paths where the user provided success output is present in stdout
check = lambda s: success_output.encode() in s.posix.dumps(1)

def print_result(result, st = None, print_none=True):
    """Prints if the program did or didn't find any results.""" 
    if print_none and (result == None or st == None):
        print(f"---\n\nCouldn't find a path for {binary} with " +
              f"'{success_output}' in output\n\n---")
    else: 
        print(f'---\n\nFound path for {binary} with output: \n' +
              f'{st.posix.dumps(1).decode()}\n' +
              f'Run: \n{binary} "$(echo -ne \'{result}\')"\n\n---')

def validate(result):
    """Returns the trimmed password if result is valid, otherwise returns None"""
    try:
        result = result[:result.index(b'\0')]
        result = ''.join('\\x{:x}'.format(b) for b in result)
        return result
    except ValueError:
        return None

def compute_value(st, password):
    """Computes the concrete value of password in the given state, producing a
    result."""
    return validate(st.solver.eval(password, cast_to=bytes))
    
        
def explore(sm, password):
    """Finds password using sm.explore()."""
    
    # explore to find execution paths
    sm.explore(find=check)
    
    # if anything was found, print the results to the screen
    if len(sm.found) > 0:
        result = compute_value(sm.found[0], password)
        print_result(result, sm.found[0])
    else:
        print_result(None)

def run(sm, password):
    """Finds password using sm.explore()."""
    
    sm.run()

    found = [s for s in sm.deadended if check(s)]

    if len(found) > 0:
        result, state = None, None
        # Try to find a result that isn't None
        for s in found:
            result = compute_value(s, password)
            state = s
            if result != None:
                break
        print_result(result, state)
    else:
        print_result(None)
    
    

def main():
    # importing here is kinda jank but its for performance reasons
    import angr
    import claripy

    # setup project, inital state & simulation manager
    p = angr.Project(binary)

    # Setup symbolic argument so we can solve for the argument password
    password = claripy.BVS('pass', 8*password_length)

    # Setup state and simulation manager
    state = p.factory.entry_state(
               args = [p.filename, password],
               add_options=angr.options.unicorn
            )
    sm = p.factory.simulation_manager(state)

    # Uncomment the following line to spawn an IPython shell when the program
    # gets to this point so you can poke around at the four objects we just
    # constructed. Use tab-autocomplete and IPython's nifty feature where if
    # you stick a question mark after the name of a function or method and hit
    # enter, you are shown the documentation string for it.

    # import IPython; IPython.embed()

    # find paths where user-provided success output is present in stdout
    # explore(sm, password)

    run(sm, password)

if __name__ == "__main__":
    # Print Usage
    if len(sys.argv) == 1:
        print(f'{sys.argv[0]} <path to password binary> <success output> [<password len>]')
        sys.exit(0)
    else: 
        binary = sys.argv[1]
        success_output = sys.argv[2]
        if len(sys.argv) == 4:
            password_len = int(sys.argv[3])

        print(f'Trying to bruteforce {binary} with angr.\n---')
        try: # Exception handling & run main
            main()
        except KeyboardInterrupt as e:
            print('')
            exit(0)
        except Exception as e:
            print("\n\n---- Error ----\n" + str(e))
            exit(1)
