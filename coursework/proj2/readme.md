---
title: Project Phase 2 - Reverse the blue team binaries
---

<details>
<summary>Project Summary</summary>

> In this phase, you can freely pick binaries from the pool and reverse engineer
> them. For type 1 binaries, you can test if you pass them by running them; For
> type 2 binaries, you will need to send the answer to JX and JX will let you
> know if you pass.
> 
> You need to submit a report with your answers to the ELF binaries (you need to
> specific which answer to which ELF binary)
> 
> Your final score will be based on your ranks among all the students
> 
> If you resolved at least 3 ELF binaries from the first type + 3 ELF binaries
> from the second type, you will enter the ranking process. Otherwise, you will
> only get 2 points for each of the ELF binaries you resolve.
> 
> E.g., you only resolve 2 type-1 binaries and 2 type-2 binaries, you will get
> 2 * (2+2) = 8 points.
> 
> If you enter the ranking process, your score will be based on the following
> rules:
> 
> - If you ranked top 10%, you will get 25 points
> - If you ranked top 10%-20%, you will get 24 points
> - If you ranked top 20%-30%, you will get 23 points
> - If you ranked top 30%-40%, you will get 22 points
> - If you ranked top 40%-50%, you will get 21 points
> - If you ranked top 50%-60%, you will get 20 points
> - If you ranked top 60%-70%, you will get 19 points
> - If you ranked top 70%-80%, you will get 18 points
> - If you ranked top 80%-90%, you will get 17 points
> - If you ranked top 90%-100%, you will get 16 points
> 
> How to calculate your score:
> 
> For each of the ELF binary you resolve, you will get a score based on the
> difficulty level of the ELF binary
> 
> - Level 1: 1 points
> - Level 2: 2 points
> - Level 3: 3 points
> - Level 4: 4 points
> - Level 5: 5 points
> - Level 6: 6 points
> 
> For students that have the same total scores, they will be ranked based on who
> gets the total scores earlier The determination of the difficulty level of an
> ELF binary will be based on the portion of students solve that ELF binary:
> 
> - Less than 16% students solve the ELF binary: Level 6
> - 16% - 32% students solve the ELF binary: Level 5
> - 32% - 48% students solve the ELF binary: Level 4
> - 48% - 64% students solve the ELF binary: Level 3
> - 64% - 80% students solve the ELF binary: Level 2
> - 80% - 100% students solve the ELF binary: Level 1
> 
> **NOTE:** YOU MAY WANT TO TRY ELF BINARIES THAT FEWER STUDENTS SOLVE TO
> OPTIMIZE YOUR SCORE
> 
> **Note:** a score board will be provided and how many students solve each ELF
> binary will also be publicized
</details>


- [Challenge Pool](https://github.com/yzhang71/CS577_Project_Challenge_Pool)
- [Leaderboard](https://docs.google.com/spreadsheets/d/13rbLC0fMR25yKQTHMKdJwFN6OS82CsUmkYQGdyLurII)
- Solutions will be in the [writeups directory](write-ups/pdfs).
- My user id in the challenge pool is `5hykxB`
- The binaries that *I* wrote are [over here](../proj1)
- My final presentation slides are [here](presentation/slides.pdf)

Here are all of the other student IDs in the challenge pool. Each student
created between 3 to 6 binaries. Below is a table indicating my progress:

| Legend                    |      |
| ------------------------- | ---- |
| Won't Crack               |      |
| Will crack                | 💣   |
| Cracking...               | 💣🔥 |
| Cracked                   | 💥   |
| Attempted - Might Revisit | 🕳   |


| Player   | Binary 1 | Binary 2 | Binary 3 | Binary 4 | Binary 5 | Binary 6 |
| -------- | -------- | -------- | -------- | -------- | -------- | -------- |
| `0DKFyl` | 💥       | 💥       | 💥       | 💥       |          |          |
| `aLXAMz` |          |          |          |          |          |          |
| `as1wRi` | 🕳       | 🕳       |          |          |          |          |
| `c4y8c7` | 🕳       | 🕳       |          |          |          |          |
| `FktI2H` | 🕳       |          |          |          |          |          |
| `ggpnd9` | 💥       | 💥       | 💥       | 💥       | 💥       | 💥       |
| `hy58ZR` | 💥       | 💥       | 💥       | 💥       | 💥       | 💥       |
| `iC2P8s` | 💥       | 💥       | 💥       | 💥       | 🕳       |          |
| `kv7Xc9` |          |          |          |          |          |          |
| `MQNQe6` | 🕳       |          |          |          |          |          |
| `Q2esoH` |          |          |          |          |          |          |
| `q5F8mG` | 💣       | 💣       | 💣       |          |          |          |
| `q8m4Qv` | 💣       | 💣       | 💣       |          |          |          |
| `RzO3B8` |          |          |          |          |          |          |
| `xZhfgq` |          |          |          |          |          |          |
| `zbGQBB` | 🕳       |          |          |          |          |          |
| `zwcQY9` | 💥       | 💣🔥[^1] | 💣       | 💣       | 💣       | 💣       |

[^1]: I did plan on completing this binary, but I reached the deadline before I
      ended up doing anything. 
