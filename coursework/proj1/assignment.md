---
title: Assignment
author: Jun Xu
---

In this phase, every student will need to create up to 6 ELF binaries, with up
to 3 from each of the following two categories

### Category 1 ELF binaries

The binary will hide a secret string in its code. Once getting executed, the
binary will wait for a user input; Once getting the input, the binary will check
whether the input matches the secret string or not. If the input matches the
secret, the binary will print “Bingo!”; otherwise it will print “Challenge me if
you can!”

The secret string can have up to 32 bytes (DO NOT USE LONGER ONES). You have
very short ones, but you will take the risk of being brute forced.

You can hide your secret to impede reverse engineering, BUT you are only allowed
to use the following two strategies (you can apply both strategies to the same
binary)

1. Complicate the logic of comparing the input with the secret string.  If you
   opt to transform the input first and then compare the transformed input with
   the transformed secret string, you need to MAKE SURE YOUR TRANSFORMATION IS
   INVERTIBLE!!!
   - compression is OK because it is invertible through decompression
   - Encryption is also OK but only symmetrical encryption is allowed
     (symmetrical encryption means the encryption and decryption use the same
     key) and only standard encryption algorithms such as AES can be used (that
     means you cannot use an encryption algorithm created by yourself)
   - HASHING is not OK because it is non-invertible.
   - If you opt to do multiple layers of transformation, you are only allowed to
     do UP TO THREE layers of transformation.
 2. Complicate the code in the ELF after the ELF is generated. For instance, you
    may rewrite the code to do obfuscation, compression, or encryption, without
    breaking the execution of the code
    - If you opt to adopt this option, you are not allowed to use
      virtual-machine-based techniques (i.e., transform your original code to a
      new programming language and encode an interpreter in the ELF to interpret
      the transformed code)
    - If you opt to adopt this option, you can either use existing tools to help
      you or you can do it manually

If you are unsure whether a strategy you design is allowed or not, email your
code and your strategy to JX and JX will let you know

### Category 2 ELF binaries

This binary, when getting executed, will automatically launch some behaviors. If
all the behaviors are successfully completed, the binary will print “Guess what
I did :)”; otherwise it will print “Please run me again :)”.

The behaviors are defined based on system calls (system calls are interfaces to
access system-level resources like files, which are usually wrapped by library
functions).  In each binary, at most 6 system calls/library functions can be
used to complete the behaviors (this is to avoid over complicated behaviors)

- you can use “fopen”, “fwrite”, “fread” to complete the behavior of “writing a specific string into a specific file”  
- you can use “socket”, “connect”, “recv”, “close” etc to get the homepage of
  “www.google.com”
- you can use functions from the “exec” families to run another program in the
  background.

System calls are only allowed to implement your behaviors or complicate the
logics (as described below). That means system calls for other goals are not
permitted.  You can hide your behaviors to impede reverse engineering, BUT you
are only allowed to use the following two strategies (you can apply both
strategies in the same binary)

1. Complicate the logic of the behaviors
   - you can use dummy system calls to cover the real behaviors. Dummy system
     calls are system calls with no effects to the system, such as writing an
     empty string to a file.
   - you can split the behavior that can be done with one system call to
     multiple systems, such as splitting one writing into multiple writings.
   - you can write inline-assembly to use the system calls instead of using
     library wrappers.
2. Complicate the code in the ELF after the ELF is generated. For instance, you
   may rewrite the code to do obfuscation, compression, or encryption, without
   breaking the execution of the code
   - If you opt to adopt this option, you are not allowed to use
     virtual-machine-based techniques (i.e., transform your original code to a
     new programming language and encode an interpreter in the ELF to interpret
     the transformed code)
   - If you opt to adopt this option, you can either use existing tools to help
     you or you can do it manually

If you are not sure whether a strategy you design is allowed, email your code
and your strategy to JX and JX will let you know

You can create the binary by developing a source code program and then compile
the source code program to an ELF binary.  You are only allowed to use C, C++,
assembly code, or direct machine code. You cannot use Python, RUST, GO etc and
then compile the code to ELF binaries.

If you need to compile your source code to produce the ELF binary, you are only
allowed to use the GCC/G++ compiler, but you are free to use whatever
compilation options.

Up to 150 lines of source code can be used to produce an ELF binary. If you
directly develop the ELF with assembly code. At most, 1000 lines of assembly
code can be used. If you mix source code with assembly code, ask JX about the
limit.

It is not mandated to use a compiler to produce the ELF. You can hand-craft an
ELF with your code encapsulated inside.

### Submission

ELF Binaries + Source Code/Assembly Code (a canvas assignment will be
created). Submit as a .zip package

For type 2 binaries, please include your descriptions about the behaviors in the
corresponding source code.

### Grading policy

Total points: 15

For each submitted ELF binary, the binary will get a score based on its
difficulty level. The difficulty level ranges from 1-6 (metrics of the
difficulty level are explained later)

- Level 1: 1.5 points
- Level 2: 1.8 points
- Level 3: 2.1 points
- Level 4: 2.4 points
- Level 5: 2.7 points
- Level 6: 3 points 

If you get more than 15 points, it will be capped to 15 points.  Based on the
above policy, you will get at least 1.5 points when you submit an ELF binary.

THIS IS NOT THE CASE. At the final presentation, you will need to present how
you create each ELF binary. The other students will vote whether your ELF binary
qualifies any points.  Late submission will not be graded.
