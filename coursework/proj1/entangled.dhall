{- Entangled configuration file
   ============================

   This configuration uses the Dhall language, see https://dhall-lang.org/.

   This file represents a record, as can be seen from the `let ... in { ... }`
   structure. Configuration for the Entangled daemon and command-line utility
   is stored in the `entangled` member of the record. Other fields are open
   for use by `pandoc` filters or other tools.

   If you want to check the configuration for correctness, run

       dhall <<< ./entangled.dhall
  -}

{- Schema
   ------

   The schema is loaded here. It is recommended to use a schema from a
   released version of Entangled.

   The hash is not strictly needed, but it ensures that you get the version
you're
   expecting to, or raise an error. You can generate the correct hash by running

       dhall hash <<< <location to schema>
  -}
let entangled =
      https://raw.githubusercontent.com/entangled/entangled/v1.2.2/data/config-schema.dhall sha256:9bb4c5649869175ad0b662d292fd81a3d5d9ccb503b1c7e316d531b7856fb096

let intercalComment = entangled.Comment.Line "PLEASE NOTE: "

let languages =
        entangled.languages
      # [ { name = "Unlambda"
          , identifiers = [ "unlambda" ]
          , comment = entangled.comments.hash
          }
        , { name = "Intercal"
          , identifiers = [ "intercal" ]
          , comment = intercalComment
          }
        ]

let syntax
    : entangled.Syntax
    = { matchCodeStart = "```[ ]*{[^{}]*}"
      , matchCodeEnd = "```"
      , extractLanguage = "```[ ]*{\\.([^{} \t]+)[^{}]*}"
      , extractReferenceName = "```[ ]*{[^{}]*#([^{} \t]*)[^{}]*}"
      , extractFileName = "```[ ]*{[^{}]*file=([^{} \t]*)[^{}]*}"
      }

let database = Some ".entangled/db.sqlite"

let watchList = [ "readme.md" ]

in  { entangled = entangled.Config::{ database, watchList, languages, syntax }
    , jupyter = { name = "Python", kernel = "python3" }
    }
