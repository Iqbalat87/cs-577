/* ~\~ language=C filename=src/crackme02.c */
/* ~\~ begin <<readme.md|src/crackme02.c>>[0] */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ptrace.h>

int main(int argc, char **argv) {
  if (argc < 2) {
    printf("Please provide an argument.\n");
    return 1;
  }

  /* ~\~ begin <<readme.md|crackme02-ptrace>>[0] */
  if (ptrace(PTRACE_TRACEME, 0, 1, 0) < 0) {
    if (argc >= 2) // >:^)
      printf("\nHmmm, that output doesn't look right. Have you\n"
             "tried turning your computer off and on again?\n\n");
    return 1;
  }
  /* ~\~ end */

  /* ~\~ begin <<readme.md|crackme02-main-loop>>[0] */
  /* "0x424242" 
   * => 0x30 '0',  [0]
   *    0x78 'x',  [1]
   *    0x34 '4',  [2]
   *    0x32 '2',  [3]
   *    0x34 '4',  [4]
   *    0x32 '2',  [5]
   *    0x34 '4',  [6]
   *    0x32 '2',  [7]
   *    0x00 '\0', [8]
   */
  char password[9] = "pAsSwOrD"; // value will be over-written c;
  password[8] = '\0';
  int base; // 0x74 when i is 1, and 0x30 otherwise.
  for (int i = 0; i < 8; i++ ) {
    int can = 0x18; int ack = 0x06; 
    if (i == 1) base = (can * 5) - ack+4;
    else base = (can * 2);
    int offset; // 0 when i is 0, 2 when i is even, 4 when i is odd.
    offset = (1 + ((i+1) % 2)) * 2;
    if (i == 0) offset = 0;
    password[i] = base + offset;
  }

  if (strcmp(password, argv[1]) == 0) goto success;
  else goto fail;
  /* ~\~ end */

 success:
  printf("Yes, '%s' is the correct password!\n", argv[1]);
  return 0;
 fail:
  printf("No, '%s' is not the correct password.\n", argv[1]);
  return 0;
}
/* ~\~ end */
