/* ~\~ language=C filename=src/crackme03.c */
/* ~\~ begin <<readme.md|src/crackme03.c>>[0] */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ptrace.h>
#include <stdbool.h>
#include <dlfcn.h>

int main(int argc, char **argv) {
  if (argc < 2) {
    printf("Please provide an argument.\n");
    return 1;
  }

  /* ~\~ begin <<readme.md|crackme02-ptrace>>[0] */
  if (ptrace(PTRACE_TRACEME, 0, 1, 0) < 0) {
    if (argc >= 2) // >:^)
      printf("\nHmmm, that output doesn't look right. Have you\n"
             "tried turning your computer off and on again?\n\n");
    return 1;
  }
  /* ~\~ end */

  /* ~\~ begin <<readme.md|crackme03-obfuscated-functions>>[0] */
  void *handle = dlopen("/lib/x86_64-linux-gnu/libc.so.6", RTLD_LAZY);
  char strcmp_sym[7]; /* strcmp */
  char strcmp_obf[] = { 0xf3, 0xf4, 0xf2, 0xe3, 0xed, 0xf0, 0x80 };

  memcpy(strcmp_sym, &strcmp_obf, 7 * sizeof(char));
  for (int i = 0; i < 7; i++)
    strcmp_sym[i] -= 0x80;

  // create functions
  int (*strcmp_)(const char *, const char *) = dlsym(handle, strcmp_sym);
  /* ~\~ end */

  /* ~\~ begin <<readme.md|crackme03-main-loop>>[0] */
  char password[9];   // deadbeef: 64 65 61 64 62 65 65 66
  password[8] = '\0'; // deadbeef:  4  5  1  4  2  5  5  6
  int base = 0x60;
  for (int i = 0; i < 8; i++) {
    switch (i) {
      case 0: { password[i] = base + 4; break; }
      case 1: { password[i] = base + 5; break; }
      case 2: { password[i] = base + 1; break; }
      case 3: { password[i] = base + 4; break; }
      case 4: { password[i] = base + 2; break; }
      case 5: { password[i] = base + 5; break; }
      case 6: { password[i] = base + 5; break; }
      case 7: { password[i] = base + 6; break; }
    }
  }

  if (strcmp_(password, argv[1]) == 0) goto success;
  else goto fail;
  /* ~\~ end */

 success:
  printf("Yes, '%s' is the correct password!\n", argv[1]);
  return 0;
 fail:
  printf("No, '%s' is not the correct password.\n", argv[1]);
  return 0;
}
/* ~\~ end */
