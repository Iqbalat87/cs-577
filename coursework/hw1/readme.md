---
title: 'Homework 1'
---

## Task Description

> - You are required to download a package of crackmes challenges from [google
>   drive](https://drive.google.com/file/d/1skfiliwGfOypGJGD_rjgFh4biPYE503q/view?usp=sharing)
> - You will then need to move the package to your VM and decompress the package
>   * How to do it with terminal: unzip homework1.zip
>   * After you are done with the decompression, please check you see five
>     binaries, crackme01 - crackme05
> - Your job is to figure out the secret string that each crackme binary will need
>   **as an argument**
>   * How to test: run the crackme binary with the secret you want to try as an
>     argument.
>     - For instance, if you figure out the secret for crackme01 is “crackme”,
>       then you can test your answer by running: ./crackme01 crackme
>     - The binary will produce an output telling you whether you found the
>       correct secret string
>   * You can use whatever reverse engineering approaches you would like, even if
>     what you used is brute force.
> - For each binary you tried to reverse engineer (regardless you figured out the
>   correct secret or not), you need to write a brief report about how you crack
>   the binary and figure out the secret string:
>   * The report does not have to be extremely detailed, but you will have to
>     cover the following items
>     - At each key step, what tools you used to do what
>     - At each key step, screenshot and explain the key pieces of code.
>     - After each key step, what clues/results you obtained
>   * Two examples of reports about crame:
>     - https://tekwizz123.blogspot.com/2016/06/solving-crackmes-beginners-guide-using.html
>       (a simpler example)
>     - https://blog.malwarebytes.com/threat-analysis/2016/04/petya-ransomware (a
>       more complicated example)
>     - You DO NOT need to be as detailed/professional as the above two examples.

Used the following to transfer the zip file:

```shell
$ sftp -oPort=2222 remnux@localhost
sftp> put homework/hw1/homework1.zip Documents/homework1
```

## `crackme01`

`file` output:

- ELF 64-bit LSB executable, x86-64, version 1 (SYSV)
- dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2,
- for GNU/Linux 3.2.0
- BuildID[sha1]=a1b3af060aa1ebb0884f90d55c671e78956c48f6, stripped

`readelf` output:

- Entrypoint is `0x4004e0`!
- <details>
  <summary>Full <code>readelf</code> output</summary>
  
  ```shell
  remnux@remnux:~/Documents/homework1$ readelf -l ./crackme01
  
  Elf file type is EXEC (Executable file)
  Entry point 0x4004e0
  There are 9 program headers, starting at offset 64
  
  Program Headers:
    Type           Offset             VirtAddr           PhysAddr
                   FileSiz            MemSiz              Flags  Align
    PHDR           0x0000000000000040 0x0000000000400040 0x0000000000400040
                   0x00000000000001f8 0x00000000000001f8  R      0x8
    INTERP         0x0000000000000238 0x0000000000400238 0x0000000000400238
                   0x000000000000001c 0x000000000000001c  R      0x1
        [Requesting program interpreter: /lib64/ld-linux-x86-64.so.2]
    LOAD           0x0000000000000000 0x0000000000400000 0x0000000000400000
                   0x00000000000007e8 0x00000000000007e8  R E    0x200000
    LOAD           0x0000000000000e10 0x0000000000600e10 0x0000000000600e10
                   0x0000000000000228 0x0000000000000230  RW     0x200000
    DYNAMIC        0x0000000000000e20 0x0000000000600e20 0x0000000000600e20
                   0x00000000000001d0 0x00000000000001d0  RW     0x8
    NOTE           0x0000000000000254 0x0000000000400254 0x0000000000400254
                   0x0000000000000044 0x0000000000000044  R      0x4
    GNU_EH_FRAME   0x00000000000006a8 0x00000000004006a8 0x00000000004006a8
                   0x000000000000003c 0x000000000000003c  R      0x4
    GNU_STACK      0x0000000000000000 0x0000000000000000 0x0000000000000000
                   0x0000000000000000 0x0000000000000000  RW     0x10
    GNU_RELRO      0x0000000000000e10 0x0000000000600e10 0x0000000000600e10
                   0x00000000000001f0 0x00000000000001f0  R      0x1
  
   Section to Segment mapping:
    Segment Sections...
     00     
     01     .interp 
     02     .interp .note.ABI-tag .note.gnu.build-id .gnu.hash .dynsym .dynstr .gnu.version .gnu.version_r .rela.dyn .rela.plt .init .plt .text .fini .rodata .eh_frame_hdr .eh_frame 
     03     .init_array .fini_array .dynamic .got .got.plt .data .bss 
     04     .dynamic 
     05     .note.ABI-tag .note.gnu.build-id 
     06     .eh_frame_hdr 
     07     
     08     .init_array .fini_array .dynamic .got 
  ```
  </details>

Popped it into ghidra, and found the main function at `0x400470`:

![](https://i.imgur.com/d4NI8GW.png)

Here's the decompiled main function after retyping and renaming a few variables
(like `retval`, `input`, etc). 

```c
int main(int argc,char **argv)
{
  long lVar1;
  char cVar2;
  int retval;
  char *input;
  char *local_RDI_25;
  bool bVar3;
  bool bVar4;
  
  bVar3 = (uint)argc < 2;
  bVar4 = argc == 2;
  if (bVar4) {
    lVar1 = 9;
    input = argv[1];
    local_RDI_25 = "password1";
    do {
      if (lVar1 == 0) break;
      lVar1 = lVar1 + -1;
      bVar3 = (byte)*input < (byte)*local_RDI_25;
      bVar4 = *input == *local_RDI_25;
      input = input + 1;
      local_RDI_25 = local_RDI_25 + 1;
    } while (bVar4);
    cVar2 = (!bVar3 && !bVar4) - bVar3;
    retval = (int)cVar2;
    if (cVar2 == '\0') {
      __printf_chk(1,"Yes, %s is correct!\n");
    }
    else {
      retval = 1;
      __printf_chk(1,"No, %s is not correct.\n");
    }
  }
  else {
    retval = -1;
    puts("Need exactly one argument.");
  }
  return retval;
}
```

**Answer is literally `password1`. `local_RDI_25` was the true password.**

## `crackme02`

### Initial Static Analysis

`file` output:

- ELF 64-bit LSB executable, x86-64, version 1 (SYSV)
- dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2
- for GNU/Linux 3.2.0
- BuildID[sha1]=6573f77a04487bcaca732ad266b854fbd64b421a, stripped

`readelf` output:
- Entry point at `0x400500`!
- <details>
  <summary>Full <code>readelf</code> output</summary>

  ```shell
  remnux@remnux:~/Documents/homework1$ readelf -l crackme02
  
  Elf file type is EXEC (Executable file)
  Entry point 0x400500
  There are 9 program headers, starting at offset 64
  
  Program Headers:
    Type           Offset             VirtAddr           PhysAddr
                   FileSiz            MemSiz              Flags  Align
    PHDR           0x0000000000000040 0x0000000000400040 0x0000000000400040
                   0x00000000000001f8 0x00000000000001f8  R      0x8
    INTERP         0x0000000000000238 0x0000000000400238 0x0000000000400238
                   0x000000000000001c 0x000000000000001c  R      0x1
        [Requesting program interpreter: /lib64/ld-linux-x86-64.so.2]
    LOAD           0x0000000000000000 0x0000000000400000 0x0000000000400000
                   0x0000000000000808 0x0000000000000808  R E    0x200000
    LOAD           0x0000000000000e10 0x0000000000600e10 0x0000000000600e10
                   0x0000000000000228 0x0000000000000230  RW     0x200000
    DYNAMIC        0x0000000000000e20 0x0000000000600e20 0x0000000000600e20
                   0x00000000000001d0 0x00000000000001d0  RW     0x8
    NOTE           0x0000000000000254 0x0000000000400254 0x0000000000400254
                   0x0000000000000044 0x0000000000000044  R      0x4
    GNU_EH_FRAME   0x00000000000006c8 0x00000000004006c8 0x00000000004006c8
                   0x000000000000003c 0x000000000000003c  R      0x4
    GNU_STACK      0x0000000000000000 0x0000000000000000 0x0000000000000000
                   0x0000000000000000 0x0000000000000000  RW     0x10
    GNU_RELRO      0x0000000000000e10 0x0000000000600e10 0x0000000000600e10
                   0x00000000000001f0 0x00000000000001f0  R      0x1
  
   Section to Segment mapping:
    Segment Sections...
     00     
     01     .interp 
     02     .interp .note.ABI-tag .note.gnu.build-id .gnu.hash .dynsym .dynstr .gnu.version .gnu.version_r .rela.dyn .rela.plt .init .plt .text .fini .rodata .eh_frame_hdr .eh_frame 
     03     .init_array .fini_array .dynamic .got .got.plt .data .bss 
     04     .dynamic 
     05     .note.ABI-tag .note.gnu.build-id 
     06     .eh_frame_hdr 
     07     
     08     .init_array .fini_array .dynamic .got 
  ```
  </details>
  
Popped into ghidra, found the main after finding the entrypoint:

![](https://i.imgur.com/RZ8n5sH.png)

Here's the decompiled main function after I've retyped and renamed some of the
variables:

```c
int main(int argc,char **argv)
{
  char current_char;
  long idx_lead;
  int retval;
  long idx;
  
  if (argc == 2) {
    current_char = *argv[1];
    if (current_char != '\0') {
      if (current_char != 'o') {
LAB_bad:
        __printf_chk(1,"No, %s is not correct.\n");
        return 1;
      }
      retval = 0x61;
      idx = 1;
      do {
        current_char = argv[1][idx];
        if (current_char == '\0') break;
        if (retval + -1 != (int)current_char) goto LAB_bad;
        idx_lead = idx + 1;
        retval = (int)"password1"[idx_lead];
        idx = idx + 1;
      } while ("password1"[idx_lead] != '\0');
    }
    __printf_chk(1,"Yes, %s is correct!\n");
    retval = 0;
  }
  else {
    puts("Need exactly one argument.");
    retval = -1;
  }
  return retval;
}
```

I don't think my renaming of these variables is that problematic. It appears 
obvious what the *are* and how they're used, but it doesn't mean I know what
the result of the computation is. 

Some notes:

- First letter of password must be `o`
- `0x61` = `0110 0001` (not sure why `retval` is set to this since its reset
  later)
- `idx_lead` is kept 1 ahead of `idx`.
- The while loop starts on the 2nd character of the input, since the first is
  compared before the loop.
- The while loop is hardcoded to 8 iterations:
  * The condition is *while `idx_lead` hasn't reached the end of the string
    "password1"*
  * "password1" has 9 characters (10 including the null terminator)
  * on the first loop iteration, `idx` is 1 and `idx_lead` is 2
  * Therefore $`10 - 2 = 8`$ loop iterations
- On each loop iteration, the ascii value is of `idx_lead` position in
  "password1" is stored as the `retval`
- On each loop iteration, the value of `retval - 1` is checked against the
  ascii value of the current char. If they aren't equal, then it's a bad match.

So, to me this said that the true password was something like `ossword1`, but I
guess that wasn't correct.

### Attempt at pass with GDB

At this point, I kinda wanna just try stepping through this in gdb. Lemme just
mark the interesting lines with their memory position:

<details>
<summary><code>crack02</code>'s main with annotated memory pos</summary>

```c
int main(int argc,char **argv)
{
  char current_char;
  long idx_lead;
  int retval;
  long idx;
  
  if (argc == 2) {
    current_char = *argv[1];
    if (current_char != '\0') {
      if (current_char != 'o') {
LAB_bad:                                                    // 0x4004d5
        __printf_chk(1,"No, %s is not correct.\n");
        return 1;
      }
      retval = 0x61;
      idx = 1;
      do {
        current_char = argv[1][idx];
        if (current_char == '\0') break;                    // 0x4004a0
        if (retval + -1 != (int)current_char) goto LAB_bad; // 0x4004d5
        idx_lead = idx + 1;
        retval = (int)"password1"[idx_lead];                // 0x4004b3
        idx = idx + 1;
      } while ("password1"[idx_lead] != '\0');
    }
    __printf_chk(1,"Yes, %s is correct!\n");
    retval = 0;
  }
  else {
    puts("Need exactly one argument.");
    retval = -1;
  }
  return retval;
}
```
</details>

<details>
<summary>Ghidra dissassembly around <code>LAB_bad</code> (ghidra called this
label <code>LAB_004004d5</code>)</summary>

```
                     LAB_004004d5                                    XREF[2]:     00400486(j), 004004ad(j)  
004004d5 48 8d 35        LEA        idx, [s_No,_%s_is_not_correct._0040068f]         ; = "No, %s is not correct.\n"
004004dc bf 01 00        MOV        argc, 0x1
004004e1 31 c0           XOR        retval, retval
004004e3 e8 78 ff        CALL       __printf_chk                                     ; undefined __printf_chk()
004004e8 b8 01 00        MOV        retval, 0x1
004004ed eb e1           JMP        LAB_004004d0
                     LAB_004004ef                                    XREF[1]:     00400477(j)  
004004ef 48 8d 3d        LEA        argc, [s_Need_exactly_one_argument._00400674]    ; = "Need exactly one argument."
004004f6 e8 55 ff        CALL       puts                                             ; int puts(char * __s)
004004fb 83 c8 ff        OR         retval, 0xffffffff
004004fe eb d0           JMP        LAB_004004d0
```
</details>

Alright, let's focus. Here are where the different relevant variables are
stored, at least according to ghidra:

| Type         | Register / Pos | Symbol         |
| ------------ | -------------- | -------------- |
| `int`        | `EAX:4`        | `<RETURN>`     |
| `int`        | `EDI:4`        | `argc`         |
| `char * *`   | `RSI:8`        | `argv`         |
| `undefined8` | `RSI:8`        | `idx`          |
| `undefined4` | `EAX:4`        | `retval`       |
| `undefined1` | `HASH:5fd3db7` | `current_char` |
| `undefined8` | `HASH:27f8784` | `idx_lead`     |

Some notes:
- `idx` is kinda shadowing `argv` a bit here, I guess this is just a compiler
  decision?
- I *think* `current_char` is stored in `EAX` or `RDX` cause the asm instruction
  used to set it is:
  <table>
  <td>
  
  ```asm
  MOV   RDX, qword ptr [argv + 0x8] ;; 0x400479
  MOVZX EAX, byte ptr [RDX]         ;; 0x40047d
  ```
  </td>
  <td>

  ```c
  current_char = *argv[1];
  ```
  </td>
  </table>

```gdb
(gdb) set disassembly-flavor intel
(gdb) break *0x4004d5
Breakpoint 1 at 0x4004d5
(gdb) # print 20 instructions after bp to make sure it's in the right place
(gdb) x/20si 0x4004d5
   0x4004d5:	lea    rsi,[rip+0x1b3]        # 0x40068f
   0x4004dc:	mov    edi,0x1
   0x4004e1:	xor    eax,eax
   0x4004e3:	call   0x400460 <__printf_chk@plt>
   0x4004e8:	mov    eax,0x1
   0x4004ed:	jmp    0x4004d0
   0x4004ef:	lea    rdi,[rip+0x17e]        # 0x400674
   0x4004f6:	call   0x400450 <puts@plt>
   0x4004fb:	or     eax,0xffffffff
   0x4004fe:	jmp    0x4004d0
   0x400500:	xor    ebp,ebp
   0x400502:	mov    r9,rdx
   0x400505:	pop    rsi
   0x400506:	mov    rdx,rsp
   0x400509:	and    rsp,0xfffffffffffffff0
   0x40050d:	push   rax
   0x40050e:	push   rsp
   0x40050f:	mov    r8,0x400660
   0x400516:	mov    rcx,0x4005f0
   0x40051d:	mov    rdi,0x400470
(gdb) # looks right to me!
(gdb) run ossword1
Starting program: /home/remnux/Documents/homework1/crackme02 ossword1

Breakpoint 1, 0x00000000004004d5 in ?? ()
(gdb) # let's see where all our variables are
(gdb) # the "password1" literal is stored in rdi:
(gdb) x/9bc $rdi
0x4006a7:	112 'p'	97 'a'	115 's'	115 's'	119 'w'	111 'o'	114 'r'	100 'd'
0x4006af:	49 '1'
(gdb) # the user's input is stored in $rdx:
(gdb) x/8bc $rdx
0x7fffffffe726:	111 'o'	115 's'	115 's'	119 'w'	111 'o'	114 'r'	100 'd'	49 '1'
(gdb) # our retval is stored in eax (even tho its 0x60 rn)
(gdb) (gdb) p/x $eax
$1 = 0x60
(gdb) # It's probably not meant to be a character, cause it's the ` character:
(gdb) p/c $eax
$2 = 96 '`'
```

Ultimately, I'm not really sure where I'm going w/ this atm. 

### CFG

I decided to ignore the decompiled source and take a look at the function graph:

![](https://i.imgur.com/gkjKLAm.png)

The highlighted basic block is the 'password success' block. Let's start from 
there and work backwards:

![](https://i.imgur.com/ASxd640.png)

There are 2 ways to get to this block, highlighted here:

![](https://i.imgur.com/PL430Dh.png)

Now, highlighted are the intermediate ways to get to this block:

![](https://i.imgur.com/Od9xLC1.png)

Understanding the password means understanding the relationship between these
blocks. 

The more common way that the password will be validated is if we go through the
loop, and exit through that way:

![](https://i.imgur.com/lIIUMPL.png)

The jump condition occurs with the following check:

```asm
MOVSX ECX, byte ptr [RDX + idx*0x01]
TEST  CL,  CL
JZ    LAB_004004bb
```

Some notes:

- `TEST` is just a compare instruction. It's comparing two registers - `CL` and
  `CL`. See [Wikipedia
  Article](https://en.wikipedia.org/wiki/TEST_(x86_instruction)) for details.
- I was confused about what `CL` meant, but the following [Wikipedia
  Article](https://en.wikipedia.org/wiki/CL_register) cleared things up for me:
  
  > The original Intel 8086 and 8088 have fourteen 16-bit registers. Four of
  > them (AX, BX, CX, DX) are general-purpose registers (GPRs), although each
  > may have an additional purpose; for example, only CX can be used as a
  > counter with the loop instruction. Each can be accessed as two separate
  > bytes (thus BX's high byte can be accessed as BH and low byte as BL).
  
  It appears that `CL` is the low byte of `CX`. In other words, it's the lowest
  word in `ECX`, which is where we're putting `current_char` into. 
  
  * I found the following infographic helpful when understanding this:
  * ![](https://upload.wikimedia.org/wikipedia/commons/1/15/Table_of_x86_Registers_svg.svg)
- I *think* this jump condition is the null byte check, actually. 

### Idea

Maybe I've been getting things wrong cause I've been misinterpreting the C code.
Let's go back to that:

> ```c
> int main(int argc,char **argv)
> {
>   char current_char;
>   long idx_lead;
>   int retval;
>   long idx;
>   
>   if (argc == 2) {
>     current_char = *argv[1];
>     if (current_char != '\0') {
>       if (current_char != 'o') {
> LAB_bad:
>         __printf_chk(1,"No, %s is not correct.\n");
>         return 1;
>       }
>       retval = 0x61;
>       idx = 1;
>       do {
>         current_char = argv[1][idx];
>         if (current_char == '\0') break;
>         if (retval + -1 != (int)current_char) goto LAB_bad;
>         idx_lead = idx + 1;
>         retval = (int)"password1"[idx_lead];
>         idx = idx + 1;
>       } while ("password1"[idx_lead] != '\0');
>     }
>     __printf_chk(1,"Yes, %s is correct!\n");
>     retval = 0;
>   }
>   else {
>     puts("Need exactly one argument.");
>     retval = -1;
>   }
>   return retval;
> }
> ```
> 
> Some notes:
> 
> - First letter of password must be `o`
> - `0x61` = `0110 0001` (not sure why `retval` is set to this since its reset
>   later)
> - `idx_lead` is kept 1 ahead of `idx`.
> - The while loop starts on the 2nd character of the input, since the first is
>   compared before the loop.
> - The while loop is hardcoded to 8 iterations:
>   * The condition is *while `idx_lead` hasn't reached the end of the string
>     "password1"*
>   * "password1" has 9 characters (10 including the null terminator)
>   * on the first loop iteration, `idx` is 1 and `idx_lead` is 2
>   * Therefore $`10 - 2 = 8`$ loop iterations
> - On each loop iteration, the ascii value is of `idx_lead` position in
>   "password1" is stored as the `retval`
> - On each loop iteration, the value of `retval - 1` is checked against the
>   ascii value of the current char. If they aren't equal, then it's a bad match.

So in my notes here, I said that the value of `retval - 1` is checked against
the current char, and that after each loop iteration, `retval` is set to the
ascii value of `idx_lead` position in "password1". This means my `"ossword1"`
was probably *partially right*, but I should've ascii-shifted everything after
the first char back by one.

I wrote a quick python program to find the password given this:

```python
input = "ossword1"
ascii_shift = lambda c: chr(ord(c) - 1)
print(input[0] + "".join([ascii_shift(c) for c in input[:1]]))
```

I got `orrvnqc0` with it, which unfortunately isn't correct. Going back to the
drawing board, I tried doing this, but for *just* the whole string `password1`:

```python
input = "password1"
ascii_shift = lambda c: chr(ord(c) - 1)
print("".join([ascii_shift(c) for c in input))
```

**That gave me `` o`rrvnqc0 ``, which is correct!**

Looking back on it, this explains a lot of useful things. Like, how when testing
things in GDB I expected a value for `a` but I got the ascii value for `` ` ``:

> ```gdb
> (gdb) # our retval is stored in eax (even tho its 0x60 rn)
> (gdb) (gdb) p/x $eax
> $1 = 0x60
> (gdb) # It's probably not meant to be a character, cause it's the ` character:
> (gdb) p/c $eax
> $2 = 96 '`'
> ```

## `crackme03`

Alright, here's the decompiled C source:

```c
int main(int argc,char **argv)
{
  char *input;
  char cmpChar;
  int retval;
  size_t inputLen;
  long idx;
  char currentChar;
  long in_FS_OFFSET;
  undefined4 local_1e;
  undefined2 local_1a;
  char local_18;
  undefined4 local_17;
  undefined2 local_13;
  undefined local_11;
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  if (argc == 2) {
    input = argv[1];
    local_1e = 0x426d416c;
    local_1a = 0x4164;
    local_18 = '\0';
    local_11 = 0;
    local_17 = 0x3020302;
    local_13 = 5;
    inputLen = strlen(input);
    if (inputLen == 6) {
      cVar1 = *input;
      cmpChar = 'l';
      it = 1;
      do {
        // LAB_004005b7
        if ((char)(cmpChar + (&local_18)[it]) != cVar1) goto LAB_BAD;
        cmpChar = *(char *)((long)&local_1e + it);
        if (cmpChar == '\0') break;
        cVar1 = input[it];
        it = it + 1;
      } while (cVar1 != '\0');
      __printf_chk(1,"Yes, %s is correct!\n",input);
      retval = 0;
    }
    else {
LAB_BAD: // LAB_004005b7
      __printf_chk(1,"No, %s is not correct.\n",input);
      retval = 1;
    }
  }
  else {
    puts("Need exactly one argument.");
    retval = -1;
  }
  if (local_10 == *(long *)(in_FS_OFFSET + 0x28)) {
    return retval;
  }
                    /* WARNING: Subroutine does not return */
  __stack_chk_fail();
}
```

Alright, so from looking at this code, I have a theory that these weird
`local_whatever` style variables are being used to store the secret
string. They're kinda obfuscated though, since they *appear* to be storing hex
literals (most of them as longs, quite a few others as something else), but if
you look at how `idx`, `cmpChar`, and `currentChar` are used in conjunction with
these locals (particularly `local_18` for comparison in the if statement that's
guarding `goto LAB_BAD`), there's definitely something fishy going on here. 

The `in_FS_OFFSET` stuff looks like it's being used to guard the if-statement,
along with some weird check about `local_10`, but I have no clue what any of
that stuff does, so I kinda wanna ignore it. It, along with `local_10` could
easily be used for stack management or something. I mean, there are enough local
variables here for the compiler to want to throw some onto the stack.

The `acStack29` looks like a scary name, but I think it's basically just an
uninitialized buffer of 5 chars. Not too awful. 

If we want to go with my other theory, then we basically have to *hope* that
the declaration order in the decompiled C code is accurate, because then we can
come up with a memory model here. We'll do well to take a close look at the 
order of declarations:

```c
long in_FS_OFFSET;  // ignore for now
undefined4 local_1e;
undefined2 local_1a;
char local_18;
undefined4 local_17;
undefined2 local_13;
undefined local_11;
long local_10;      // ignore for now
```

None of these `local_whatever` variables appear to change, so it's safe to say
that they're all constants. Here are the declarations with their values, as
they're defined later in the function in the order they're declared earlier:

```c
undefined4 local_1e = 0x426d416c;
undefined2 local_1a = 0x4164;
char local_18 = '\0';
undefined4 local_17 = 0x3020302;
undefined2 local_13 = 5;
undefined local_11 = 0;
```

So, a big struggle is gonna be around these `undefined4`, `undefined2`, and
`undefined` types. For now, I'm gonna assume they're chars with weird data in
them. If we treat them as chars, then the main logic will look like this:

```c
if (inputLen == 6) {
  currentChar = *input;
  cmpChar = 'l';
  idx = 1;
  do {
    // LAB_004005b7
    if ((char)(cmpChar + (&local_18)[idx]) != currentChar) goto LAB_BAD;
    cmpChar = (&local_1e)[idx];
    if (cmpChar == '\0') break;
    currentChar = input[idx];
    idx = idx + 1;
  } while (currentChar != '\0');
  __printf_chk(1,"Yes, %s is correct!\n",input);
  retval = 0;
}
else {
LAB_BAD: // LAB_004005b7
  __printf_chk(1,"No, %s is not correct.\n",input);
  retval = 1;
}
```

So, from here, there are 2 ways to approach this:

1. Throw this stuff in GDB and take a look at what the memory looks like
2. Decode the bytes manually to see what the memory looks like.
3. Both of the above

#### Decode the bytes manually

I'm probably gonna go for 3, so let's start with looking at what this looks like,
sequentially. We're looking at storing the following values:

- `0x426d416c`
- `0x4164`
- `'\0'`, null terminator
- `0x3020302`
- 5
- 0

I tried using this [hex to ascii
converter](https://www.binaryhexconverter.com/hex-to-ascii-text-converter) on
some of these hex values, and I'm getting:

- BmAlAd from `0x426d416c 0x4164 '\0'`
- Some garbage from `0x3020302`

None of this would probably be the password itself, however, it could still be
what the password is based on. Let's take a moment to review what we know:

- The password is 6 characters long
- `currentChar` tracks `idx` on the input string
- `cmpChar` tracks `idx` on `0x426d416c 0x4164 '\0'` (or, the string `"BmAlAd"`
  when interpreted as an array of chars)
- `cmpChar` is initialized to `'l'` on the first loop iteration.
- For every loop iteration, the current character must be equal to 
  `cmpChar + (&local_18)[idx]`. Since `idx` starts at 1 and not 0, 
  `(&local_18)[idx]` should actually track over `0x3020302 5 0`.
- Using this [hex to binary
  converter](https://www.binaryhexconverter.com/hex-to-binary-converter), and
  this [decimal to binary
  converter](https://www.binaryhexconverter.com/decimal-to-binary-converter),
  and assuming that the `0` and `5` are stored in 4 bits, we can safely say that
  the value of `0x3020302 5 0` is: 
  
  ``` 
  0011 0000 0010 0000 0011 0000 0010
  0000 0101
  0000 0000
  ```
  
  ... which is still garbage in ASCII, but remember they're being added with
  `cmpChar` to get the actual thing that the `currentChar` needs to be equal to!

So it appears that the true comparison for the `currentChar` is being made by
adding the ascii values for `0x426d416c4164` and `0x30203020500` character by
character. I added these together with:

```python
print("0x{:x}".format(0x426d416c4164 + 0x30203020500))
# => 0x456f446e4664
```

Using that handy dandy hex to ascii converter we talked about earlier, we can
see that `0x456f446e4664` is `EoDnFd`, which also isn't correct.

I'm kinda stumped at this point. Maybe throwing this in GDB would be a good
idea.

### Decode the bytes using GDB

So, the main function starts at `0x00400520`, and the `if` statement that starts
the main logic is located at `0x0040057e`. By the time we're in the `if`
statement, the entire string should be constructed. Here's a nice little table
of where ghidra thinks things are:

| Type         | Location         | Symbol        |
| ------------ | ---------------- | ------------- |
| `int`        | `EAX:4`          | `<RETURN>`    |
| `int`        | `EDI:4`          | `argc`        |
| `char * *`   | `RSI:8`          | `argv`        |
| `undefined8` | `RAX:8`          | `inputLen`    |
| `undefined1` | `AL:1`           | `cmpChar`     |
| `undefined8` | `RCX:8`          | `idx`         |
| `undefined1` | `SIL:1`          | `currentChar` |
| `undefined4` | `EAX:4`          | `retval`      |
| `undefined8` | `Stack[-0x10]:8` | `local_10`    |
| `undefined1` | `Stack[-0x11]:1` | `local_11`    |
| `undefined2` | `Stack[-0x13]:2` | `local_13`    |
| `undefined4` | `Stack[-0x17]:4` | `local_17`    |
| `undefined1` | `Stack[-0x18]:1` | `local_18`    |
| `undefined2` | `Stack[-0x1a]:2` | `local_1a`    |
| `undefined4` | `Stack[-0x1e]:4` | `local_1e`    |
| `undefined8` | `HASH:5f2c60e`   | `input`       |

If this doesn't work, IIRC there's a way to find the data in the function's
stack frame. Maybe that would be good too. 

Alright, so I just put a breakpoint at the `if` statement, and I'm trying to 
take a peek at the earlier code, since that'll tell me where my `local_whatever`
variables are:

```gdb
(gdb) set disassembly-flavor intel
(gdb) break *0x0040057e
Breakpoint 1 at 0x40057e
(gdb) run EoDnFd
Starting program: /home/remnux/Documents/homework1/crackme03 EoDnFd

Breakpoint 1, 0x000000000040057e in ?? ()
(gdb) x/20si $rip - 20*4
   0x40052e:	mov    QWORD PTR [rsp+0x18],rax
   0x400533:	xor    eax,eax
   0x400535:	cmp    edi,0x2
   0x400538:	jne    0x400602
   0x40053e:	mov    rbx,QWORD PTR [rsi+0x8]
   0x400542:	xor    edx,edx
   0x400544:	mov    eax,0x4164
   0x400549:	mov    WORD PTR [rsp+0x15],dx
   0x40054e:	mov    DWORD PTR [rsp+0xa],0x426d416c
   0x400556:	mov    WORD PTR [rsp+0xe],ax
   0x40055b:	mov    BYTE PTR [rsp+0x10],0x0
   0x400560:	mov    rdi,rbx
   0x400563:	mov    BYTE PTR [rsp+0x17],0x0
   0x400568:	mov    DWORD PTR [rsp+0x11],0x3020302
   0x400570:	mov    BYTE PTR [rsp+0x15],0x5
   0x400575:	call   0x4004f0 <strlen@plt>
   0x40057a:	cmp    rax,0x6
=> 0x40057e:	jne    0x4005b7
   0x400580:	movzx  esi,BYTE PTR [rbx]
   0x400583:	mov    ecx,0x1
(gdb) 
```

So these instructions are *really* interesting, since they're where our
assignments are:

<table>
<td>

```asm
mov    eax,0x4164
mov    WORD PTR [rsp+0x15],dx
mov    DWORD PTR [rsp+0xa],0x426d416c
mov    WORD PTR [rsp+0xe],ax
mov    BYTE PTR [rsp+0x10],0x0
mov    rdi,rbx
mov    BYTE PTR [rsp+0x17],0x0
mov    DWORD PTR [rsp+0x11],0x3020302
mov    BYTE PTR [rsp+0x15],0x5
```
</td>
<td>

```c
local_1e = 0x426d416c;
local_1a = 0x4164;
local_18 = '\0';
local_11 = 0;
local_17 = 0x3020302;
local_13 = 5;
```
</td>
</table>

The data itself supposedly starts at `local_1e` and `local_17`, respectively. 
Next, I start interrogating the different memory positions here:

```gdb
(gdb) x/s $rsp+0xe
0x7fffffffe3ae:	"dA"
(gdb) x/s $rsp+0xa
0x7fffffffe3aa:	"lAmBdA"
(gdb) x/s $rsp+0x11
0x7fffffffe3b1:	"\002\003\002\003\005"
(gdb) x/s $rsp+0x17
0x7fffffffe3b7:	""
```

So the `rsp+0xa` looks like it holds the string `"lAmBdA"`, which looks as
coherent as anything else I've seen here. It looks like the C source code
must've garbled some of this stuff up, so I'm kinda hesitant to trust it now. 

### IDEA: Run through letter-by-letter in the loop to check what is being compared

So I've kinda been bashing my head against the wall for a while now. I think
I'll learn a bit more by tracking the variables as the loop iterates.  The if
statement *inside* of the loop is at memory address `0x004005b5`. This is where
the conditional jump happens after the check, so whatever 2 variables are being
compared **will** be the current character, and whatever the weird algorithm
came up with. So, it'll take a while 'cause the program will reject my input, 
and I'll have to keep re-running it, but it'll be worth it.

After setting the breakpoint, a look around yields:

```gdb
(gdb)  x/20si $rip-10*3
   0x400597:	add    BYTE PTR [rdi],cl
   0x400599:	mov    dh,0x44
   0x40059b:	or     al,0xa
   0x40059d:	test   al,al
   0x40059f:	je     0x4005e8
   0x4005a1:	add    rcx,0x1
   0x4005a5:	movzx  esi,BYTE PTR [rbx+rcx*1-0x1]
   0x4005aa:	test   sil,sil
   0x4005ad:	je     0x4005e8
   0x4005af:	add    al,BYTE PTR [rdx+rcx*1]
   0x4005b2:	cmp    al,sil
=> 0x4005b5:	je     0x400598
   0x4005b7:	lea    rsi,[rip+0x231]        # 0x4007ef
   0x4005be:	mov    rdx,rbx
   0x4005c1:	mov    edi,0x1
   0x4005c6:	xor    eax,eax
   0x4005c8:	call   0x400510 <__printf_chk@plt>
   0x4005cd:	mov    eax,0x1
   0x4005d2:	mov    rdi,QWORD PTR [rsp+0x18]
   0x4005d7:	xor    rdi,QWORD PTR fs:0x28
```

The `cmp` compares `al, sil`, so my hunch is that `al` has my comparison
character. I can print the char value of `al` with `print (char) $al`. 

I can avoid the hastle of running each loop iteration by having a conditional
breakpoint on the value of `idx`. This is possible with :

```
(gdb) # To create breakpoint for 3 loop iterations
(gdb) break *0x004005b5 if $rcx == 3
Breakpoint 1 at 0x4005b5
(gdb) # To modify breakpoint for 4 loop iterations
(gdb) cond 1 $rcx == 4
```

 . Here's a list of `al` values, indexed by the
value of `idx`, which is stored in `$rcx`:

1. `al` is `n`. Next input should be `n-----`
2. `al` is `D`. Next input should be `nD----`
3. `al` is `o`. Next input should be `nDo---`
4. `al` is `E`. Next input should be `nDoE--`
5. `al` is `i`. Next input should be `nDoEi-`
6. `al` is `i`. Next input should be `nDoEiA`

**`nDoEiA` is the password.**

## `crackme04`

Alright, as usual, here's the lightly-edited decompiled C:

```c
int main(int argc,char **argv)
{
  char current_char;
  int trailing_idx;
  long idx;
  int retval;
  
  if (argc == 2) {
    retval = 0;
    idx = 1;
    current_char = *argv[1];
    if (current_char != '\0') {
      do {
        trailing_idx = (int)idx;
        retval = retval + current_char;
        current_char = argv[1][idx];
        idx = idx + 1;
      } while (current_char != '\0');
      if ((trailing_idx == 0x10) && (retval == 0x6e2)) {
        __printf_chk(1,"Yes, %s is correct!\n");
        return 0;
      }
    }
    __printf_chk(1,"No, %s is not correct.\n");
    retval = 1;
  }
  else {
    puts("Need exactly one argument.");
    retval = -1;
  }
  return retval;
}
```

Some immediate reactions:

- So, by the time we get to the big important `if` statement, `trailing_idx` is 
  `idx-1`. I *think* the resulting behavior should be fixing the length of the
  input? Like an obfuscated `strlen` check?
- `retval` is added to w/ the ascii value of the current char. So, overall, this
   password is a string whose characters' ascii values add to equal `0x6e2` by
   the `0x10`'th character.
   
So, getting a valid password for this binary means finding a sequence of 15
integers (15 cause `0x10 - 1` is 15) between 32 and 126 that sum up to 1762
(1762 cause `0x6e2` is hex for 1762). 

- $`120 \times 14 = 1680`$, and 120 represents ASCII `x`
- $`120 \times 15 = 1800`$, which is above the number we want.
- So, the difference is $`1762 - 1680 = 82`$, which is ASCII `R`
- So, our string could be `xxxxxxxxxxxxxxR`, which unfortunately fails.

I figured it was too easy. Maybe I messed up with my interpretation of `0x10`,
and it'd need to be `16` characters long instead? We could split our `82` into
2 `41`'s, and have the string `xxxxxxxxxxxxxx))` instead.

**`xxxxxxxxxxxxxx))` is the password!**

## `crackme05`

<details>
<summary>View Decompiled C Code</summary>

```c
int main(int argc,char **argv)

{
  int *piVar1;
  long lVar2;
  int iVar3;
  char *__s1;
  int iVar4;
  ulong unaff_R12;
  ulong unaff_R13;
  ulong unaff_R14;
  
  if (argc != 2) {
    puts("Need exactly one argument.");
    return -1;
  }
  __s1 = (char *)malloc(0xf);
  iVar4 = 0;
  if (true) {
    piVar1 = (int *)cpuid_basic_info(0);
  }
  else {
    if (false) {
      piVar1 = (int *)cpuid_Version_info(0);
    }
    else {
      if (false) {
        piVar1 = (int *)cpuid_cache_tlb_info(0);
      }
      else {
        if (false) {
          piVar1 = (int *)cpuid_serial_info(0);
        }
        else {
          if (false) {
            piVar1 = (int *)cpuid_Deterministic_Cache_Parameters_info(0);
          }
          else {
            if (false) {
              piVar1 = (int *)cpuid_MONITOR_MWAIT_Features_info(0);
            }
            else {
              if (false) {
                piVar1 = (int *)cpuid_Thermal_Power_Management_info(0);
              }
              else {
                if (false) {
                  piVar1 = (int *)cpuid_Extended_Feature_Enumeration_info(0);
                }
                else {
                  if (false) {
                    piVar1 = (int *)cpuid_Direct_Cache_Access_info(0);
                  }
                  else {
                    if (false) {
                      piVar1 = (int *)cpuid_Architectural_Performance_Monitoring_info(0);
                    }
                    else {
                      if (false) {
                        piVar1 = (int *)cpuid_Extended_Topology_info(0);
                      }
                      else {
                        if (false) {
                          piVar1 = (int *)cpuid_Processor_Extended_States_info(0);
                        }
                        else {
                          if (false) {
                            piVar1 = (int *)cpuid_Quality_of_Service_info(0);
                          }
                          else {
                            if (false) {
                              piVar1 = (int *)cpuid_brand_part1_info(0);
                            }
                            else {
                              if (false) {
                                piVar1 = (int *)cpuid_brand_part2_info(0);
                              }
                              else {
                                if (false) {
                                  piVar1 = (int *)cpuid_brand_part3_info(0);
                                }
                                else {
                                  piVar1 = (int *)cpuid(0);
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  if (*piVar1 != 0) goto LAB_0040065c;
  do {
    __s1[8] = (char)unaff_R12;
    *__s1 = (char)unaff_R14;
    __s1[1] = (char)((unaff_R14 & 0xffffffff) >> 8);
    __s1[2] = (char)((unaff_R14 & 0xffffffff) >> 0x10);
    __s1[4] = (char)unaff_R13;
    __s1[5] = (char)((unaff_R13 & 0xffffffff) >> 8);
    __s1[6] = (char)((unaff_R13 & 0xffffffff) >> 0x10);
    __s1[0xb] = (char)(unaff_R12 >> 0x18);
    argv = (char **)argv[1];
    iVar4 = (int)argv;
    __s1[9] = (char)((unaff_R12 & 0xffffffff) >> 8);
    __s1[7] = (char)(unaff_R13 >> 0x18);
    __s1[3] = (char)(unaff_R14 >> 0x18);
    __s1[10] = (char)((unaff_R12 & 0xffffffff) >> 0x10);
    *(undefined2 *)(__s1 + 0xc) = 0x5133;
    __s1[0xe] = '\0';
    iVar3 = strcmp(__s1,(char *)argv);
    free(__s1);
    if (iVar3 == 0) {
      FUN_00400760();
    }
    FUN_00400790();
LAB_0040065c:
    if (iVar4 == 0) {
      lVar2 = cpuid_basic_info(0);
    }
    else {
      if (iVar4 == 1) {
        lVar2 = cpuid_Version_info(1);
      }
      else {
        if (iVar4 == 2) {
          lVar2 = cpuid_cache_tlb_info(2);
        }
        else {
          if (iVar4 == 3) {
            lVar2 = cpuid_serial_info(3);
          }
          else {
            if (iVar4 == 4) {
              lVar2 = cpuid_Deterministic_Cache_Parameters_info(4);
            }
            else {
              if (iVar4 == 5) {
                lVar2 = cpuid_MONITOR_MWAIT_Features_info(5);
              }
              else {
                if (iVar4 == 6) {
                  lVar2 = cpuid_Thermal_Power_Management_info(6);
                }
                else {
                  if (iVar4 == 7) {
                    lVar2 = cpuid_Extended_Feature_Enumeration_info(7);
                  }
                  else {
                    if (iVar4 == 9) {
                      lVar2 = cpuid_Direct_Cache_Access_info(9);
                    }
                    else {
                      if (iVar4 == 10) {
                        lVar2 = cpuid_Architectural_Performance_Monitoring_info(10);
                      }
                      else {
                        if (iVar4 == 0xb) {
                          lVar2 = cpuid_Extended_Topology_info(0xb);
                        }
                        else {
                          if (iVar4 == 0xd) {
                            lVar2 = cpuid_Processor_Extended_States_info(0xd);
                          }
                          else {
                            if (iVar4 == 0xf) {
                              lVar2 = cpuid_Quality_of_Service_info(0xf);
                            }
                            else {
                              if (iVar4 == -0x7ffffffe) {
                                lVar2 = cpuid_brand_part1_info(0x80000002);
                              }
                              else {
                                if (iVar4 == -0x7ffffffd) {
                                  lVar2 = cpuid_brand_part2_info(0x80000003);
                                }
                                else {
                                  if (iVar4 == -0x7ffffffc) {
                                    lVar2 = cpuid_brand_part3_info(0x80000004);
                                  }
                                  else {
                                    lVar2 = cpuid(iVar4);
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    unaff_R14 = (ulong)*(uint *)(lVar2 + 4);
    unaff_R12 = (ulong)*(uint *)(lVar2 + 0xc);
    unaff_R13 = (ulong)*(uint *)(lVar2 + 8);
  } while( true );
}
```
</details>

Alright, this one is just kinda obnoxious. The program *does* behave like the
other crackmes:

```shell
remnux@remnux:~/Documents/homework1$ ./crackme05 test
No, test is not correct.
```

The lack of any strings is pretty suspicious as well. IDK what these `cpuid`
functions do, but if I had to guess, they're used to create the string literals
necessary for printing.

It's funny, cause looking through the assembly, it's clear that the decompiler
must've gotten **really** confused with what was happening. The assembly is
MUCH clearer:

<details>
<summary>Assembly code</summary>

```asm
    PUSH       R14
    PUSH       R13                                                                              ``
    PUSH       R12
    PUSH       RBP
    PUSH       RBX
    SUB        RSP, 0x10
    CMP        argc, 0x2
    JZ         LAB_004005bd
    LEA        argc, [s_Need_exactly_one_argument._00400891]    ; = "Need exactly one argument."
    CALL       puts                                             ; int puts(char * __s)
    ADD        RSP, 0x10
    OR         EAX, 0xffffffff
    POP        RBX
    POP        RBP
    POP        R12
    POP        R13
    POP        R14
    RET
LAB_004005bd                                    XREF[1]:     0040059f(j)
    MOV        argc, 0xf
    MOV        qword ptr [RSP + local_30], argv
    CALL       malloc                                           ; void * malloc(size_t __size)
    XOR        argc, argc
    MOV        RBP, RAX
    MOV        argv, qword ptr [RSP + local_30]
    MOV        EAX, argc
    CPUID
    TEST       EAX, EAX
    JNZ        LAB_0040065c
LAB_004005de                                    XREF[1]:     00400669(j)
    MOV        EAX, R14D
    MOV        byte ptr [RBP + 0x8], R12B
    MOV        byte ptr [RBP], R14B
    MOV        byte ptr [RBP + 0x1], AH
    SAR        EAX, 0x10
    SHR        R14D, 0x18
    MOV        byte ptr [RBP + 0x2], AL
    MOV        EAX, R13D
    MOV        byte ptr [RBP + 0x4], R13B
    MOV        byte ptr [RBP + 0x5], AH
    SAR        EAX, 0x10
    SHR        R13D, 0x18
    MOV        byte ptr [RBP + 0x6], AL
    MOV        EAX, R12D
    SHR        R12D, 0x18
    MOV        byte ptr [RBP + 0xb], R12B
    MOV        R12, qword ptr [argv + 0x8]
    MOV        argc, RBP
    MOV        byte ptr [RBP + 0x9], AH
    SAR        EAX, 0x10
    MOV        byte ptr [RBP + 0x7], R13B
    MOV        byte ptr [RBP + 0x3], R14B
    MOV        byte ptr [RBP + 0xa], AL
    MOV        argv, R12
    MOV        word ptr [RBP + 0xc], 0x5133
    MOV        byte ptr [RBP + 0xe], 0x0
    CALL       strcmp                                           ; int strcmp(char * __s1, char *
    MOV        argc, RBP
    MOV        R13D, EAX
    CALL       free                                             ; void free(void * __ptr)
    TEST       R13D, R13D
    MOV        argc, R12
    JNZ        LAB_00400657
    CALL       FUN_00400760                                     ; undefined FUN_00400760()
LAB_00400657                                    XREF[1]:     00400650(j)
    CALL       FUN_00400790                                     ; undefined FUN_00400790()
LAB_0040065c                                    XREF[1]:     004005dc(j)
    MOV        EAX, argc
    CPUID
    MOV        R14D, EBX
    MOV        R12D, ECX
    MOV        R13D, EDX
    JMP        LAB_004005de
```
</details>

Here, the entire convoluted logic tree is just the `cpuid` instruction. I didn't
know much about it, but here are some relevant resources:

- [`cpuid` man pages](https://man7.org/linux/man-pages/man4/cpuid.4.html)
- [CPUID Wikipedia Page](https://en.wikipedia.org/wiki/CPUID)

So, now I have some more reasonable reactions to this:

- According to the wikipedia page, `CPUID` uses `EAX` to determine the category
  of information that's returned. 

It's funny, now that I'm looking at the assembly, I'm realizing that there are
these 2 function calls to `FUN_00400760` and `FUN_00400790`. Jumping through 
the code, it's clear that these functions are actually obfuscated versions of
the print!

Here's Ghidra's decompiled version of these functions:

```c
void FUN_00400760(undefined8 param_1)
{
  __printf_chk(1,"Yes, %s is correct!\n",param_1);
                    /* WARNING: Subroutine does not return */
  exit(0);
}

void FUN_00400790(undefined8 param_1)
{
  __printf_chk(1,"No, %s is not correct.\n",param_1);
                    /* WARNING: Subroutine does not return */
  exit(1);
}
```

At this point, we may as well rename these functions `print_correct` and
`print_incorrect` in ghidra.

Here's the full, relabeled assembly, *with* the other two functions included:

<details>
<summary>Full Assembly including `print_correct` and `print_incorrect`</summary>

```asm
main
    PUSH       R14
    PUSH       R13
    PUSH       R12
    PUSH       RBP
    PUSH       RBX
    SUB        RSP, 0x10
    CMP        argc, 0x2
    JZ         LAB_004005bd
    LEA        argc, [s_Need_exactly_one_argument._00400891]    ; = "Need exactly one argument."
    CALL       puts                                             ; int puts(char * __s)
    ADD        RSP, 0x10
    OR         EAX, 0xffffffff
    POP        RBX
    POP        RBP
    POP        R12
    POP        R13
    POP        R14
    RET
LAB_004005bd                                    XREF[1]:     0040059f(j)
    MOV        argc, 0xf
    MOV        qword ptr [RSP + local_30], argv
    CALL       malloc                                           ; void * malloc(size_t __size)
    XOR        argc, argc
    MOV        RBP, RAX
    MOV        argv, qword ptr [RSP + local_30]
    MOV        EAX, argc
    CPUID
    TEST       EAX, EAX
    JNZ        LAB_0040065c
LAB_004005de                                    XREF[1]:     00400669(j)
    MOV        EAX, R14D
    MOV        byte ptr [RBP + 0x8], R12B
    MOV        byte ptr [RBP], R14B
    MOV        byte ptr [RBP + 0x1], AH
    SAR        EAX, 0x10
    SHR        R14D, 0x18
    MOV        byte ptr [RBP + 0x2], AL
    MOV        EAX, R13D
    MOV        byte ptr [RBP + 0x4], R13B
    MOV        byte ptr [RBP + 0x5], AH
    SAR        EAX, 0x10
    SHR        R13D, 0x18
    MOV        byte ptr [RBP + 0x6], AL
    MOV        EAX, R12D
    SHR        R12D, 0x18
    MOV        byte ptr [RBP + 0xb], R12B
    MOV        R12, qword ptr [argv + 0x8]
    MOV        argc, RBP
    MOV        byte ptr [RBP + 0x9], AH
    SAR        EAX, 0x10
    MOV        byte ptr [RBP + 0x7], R13B
    MOV        byte ptr [RBP + 0x3], R14B
    MOV        byte ptr [RBP + 0xa], AL
    MOV        argv, R12
    MOV        word ptr [RBP + 0xc], 0x5133
    MOV        byte ptr [RBP + 0xe], 0x0
    CALL       strcmp                                           ; int strcmp(char * __s1, char *
    MOV        argc, RBP
    MOV        R13D, EAX
    CALL       free                                             ; void free(void * __ptr)
    TEST       R13D, R13D
    MOV        argc, R12
    JNZ        LAB_00400657
    CALL       print_success                                    ; undefined print_success()
LAB_00400657                                    XREF[1]:     00400650(j)
    CALL       print_failure                                    ; undefined print_failure()
LAB_0040065c                                    XREF[1]:     004005dc(j)
    MOV        EAX, argc
    CPUID
    MOV        R14D, EBX
    MOV        R12D, ECX
    MOV        R13D, EDX
    JMP        LAB_004005de

print_success                                   XREF[3]:     main:00400652(c), 004008d8,
    LEA        RSI, [s_Yes,_%s_is_correct!_00400864]            ; = "Yes, %s is correct!\n"
    MOV        RDX, RDI
    SUB        RSP, 0x8
    MOV        EDI, 0x1
    XOR        EAX, EAX
    CALL       __printf_chk                                     ; undefined __printf_chk()
    XOR        EDI, EDI
    CALL       exit                                             ; void exit(int __status)
;; -- Flow Override: CALL_RETURN (CALL_TERMINATOR)
    NOP        dword ptr [RAX + RAX*0x1]
    NOP        word ptr CS:[RAX + RAX*0x1]

print_failure                                   XREF[3]:     main:00400657(c), 004008e0,
    LEA        RSI, [s_No,_%s_is_not_correct._00400879]         ; = "No, %s is not correct.\n"
    MOV        RDX, RDI
    SUB        RSP, 0x8
    MOV        EDI, 0x1
    XOR        EAX, EAX
    CALL       __printf_chk                                     ; undefined __printf_chk()
    MOV        EDI, 0x1
    CALL       exit                                             ; void exit(int __status)
;;  -- Flow Override: CALL_RETURN (CALL_TERMINATOR)
    NOP
    NOP        word ptr CS:[RAX + RAX*0x1]
```
</details>

This assembly itself won't do, however. I need a better high-level reference
point for this code. At this point, we can make quite a few more modifications
now that we know that `CPUID` is just a single instruction as well. 

Let's revisit the C code, and replace the scary looking `if-else` logic tree
with `asm("CPUID")`. We can also reason a bit better about the code, and rename
a few variables to make it a bit more readable:

```c
void print_correct(undefined8 param_1)
{
  __printf_chk(1,"Yes, %s is correct!\n",param_1);
                    /* WARNING: Subroutine does not return */
  exit(0);
}

void print_incorrect(undefined8 param_1)
{
  __printf_chk(1,"No, %s is not correct.\n",param_1);
                    /* WARNING: Subroutine does not return */
  exit(1);
}

int main(int argc,char **argv)
{
  int *piVar1;
  long lVar2;
  int cmp_result;
  char *password;
  int iVar3;
  ulong R12;
  ulong R13;
  ulong R14;
  
  if (argc != 2) {
    puts("Need exactly one argument.");
    return -1;
  }
  password = (char *)malloc(0xf);
  iVar3 = 0;
  /**
   * Don't trust this C code!
   * There's a bunch of low level ASM things happening here involving registers
   * R12 through R14. You can *kinda* get an idea of what's happening, and I'm 
   * *sure* it's all kosher, but it's pretty sus. See the assembly instead.
   **/
  asm("CPUID"); // piVar1 is written to
  if (*piVar1 != 0) goto LAB_0040065c;
  do {
    password[8] = (char)R12;
    *password = (char)R14;
    password[1] = (char)((R14 & 0xffffffff) >> 8);
    password[2] = (char)((R14 & 0xffffffff) >> 0x10);
    password[4] = (char)R13;
    password[5] = (char)((R13 & 0xffffffff) >> 8);
    password[6] = (char)((R13 & 0xffffffff) >> 0x10);
    password[0xb] = (char)(R12 >> 0x18);
    argv = (char **)argv[1];
    iVar3 = (int)argv;
    password[9] = (char)((R12 & 0xffffffff) >> 8);
    password[7] = (char)(R13 >> 0x18);
    password[3] = (char)(R14 >> 0x18);
    password[10] = (char)((R12 & 0xffffffff) >> 0x10);
    *(undefined2 *)(password + 0xc) = 0x5133;
    password[0xe] = '\0';
    cmp_result = strcmp(password,(char *)argv);
    free(password);
    if (cmp_result == 0) {
      print_success();
    }
    print_failure();
LAB_0040065c:
    /**
     * Don't trust this C code!
     * Once again, there's some low level ASM things happening with our good
     * friends R12 through R14. This seems a bit less complicated than the other
     * block, though. 
     **/
    asm("CPUID"); // lVar2 is written to.
    R14 = (ulong)*(uint *)(lVar2 + 4);
    R12 = (ulong)*(uint *)(lVar2 + 0xc);
    R13 = (ulong)*(uint *)(lVar2 + 8);
  } while( true );
}
```

A few reactions:

- The password itself is probably 14 characters long (since they do
  `password[0xe] = '\0'`).
- It appears some kinda weird algorithm is used to create the `password` based
  on the registers `R12`, `R13`, and `R14`. These registers *appear* to be
  populated at the end of the `do-while` loop with data from `CPUID`.
- If the code ever gets to the `password` defining block in the do-while loop,
  there won't be *any* more iterations, since the code would have either
  printed success or failure.
- There are 2 ways to approach `password[..] = ..` block:
  1. The `*piVar1 != 0` check fails, so the code falls through into it
  2. The `*piVar1 != 0` check succeeds, so the code jumps to `LAB_0040065c`, 
     updates `R12`, `R13`, and `R14` based on the `CPUID` call, then the loop
     *iterates* and puts us in the `password[..] = ..` block.
- Since there are only 2 ways to approach the `password[..] = ..` block, and both
  hit the `while` at most 1 time, we can say that the loop only really iterates
  at most 1 time. 

So because the loop only iterates at most 1 time, it means I can cheese this by
doing what I did with [crackme03](#crackme03), since there will always be a
"true" iteration of the loop, and it's not like a loop that repeatedly updates
the password.

Here's another one of those tables where Ghidra tells me where things are:

| Type         | Location         | Symbol       |
| ------------ | ---------------- | ------------ |
| `int`        | `EAX:4`          | `<RETURN>`   |
| `int`        | `EDI:4`          | `argc`       |
| `char  *  *` | `RSI:8`          | `argv`       |
| `undefined8` | `R12:8`          | `R12`        |
| `undefined8` | `R13:8`          | `R13`        |
| `undefined8` | `R14:8`          | `R14`        |
| `undefined8` | `RAX:8`          | `password`   |
| `undefined4` | `EAX:4`          | `cmp_result` |
| `undefined8` | `Stack[-0x30]:8` | `local_30`   |

The instruction where `strcmp` is called is at memory address `0x0040063a`, so I
can probably just set a breakpoint there, when the breakpoint is tripped,
examine `RAX` to see what the password is.

```gdb
(gdb) x/20si $rip-40
   0x400612:	mov    BYTE PTR [rbp+0xb],ah
   0x400615:	mov    r12,QWORD PTR [rsi+0x8]
   0x400619:	mov    rdi,rbp
   0x40061c:	mov    BYTE PTR [rbp+0x9],ah
   0x40061f:	sar    eax,0x10
   0x400622:	mov    BYTE PTR [rbp+0x7],r13b
   0x400626:	mov    BYTE PTR [rbp+0x3],r14b
   0x40062a:	mov    BYTE PTR [rbp+0xa],al
   0x40062d:	mov    rsi,r12
   0x400630:	mov    WORD PTR [rbp+0xc],0x5133
   0x400636:	mov    BYTE PTR [rbp+0xe],0x0
=> 0x40063a:	call   0x400550 <strcmp@plt>
   0x40063f:	mov    rdi,rbp
   0x400642:	mov    r13d,eax
   0x400645:	call   0x400530 <free@plt>
   0x40064a:	test   r13d,r13d
   0x40064d:	mov    rdi,r12
   0x400650:	jne    0x400657
   0x400652:	call   0x400760
   0x400657:	call   0x400790
(gdb) x/s $rax
0x6c65:	Cannot access memory at address 0x6c65
(gdb) x/s $rbp
0x602260:	"GenuineIntel3Q"
```

So it appears that GDB disagrees with Ghidra on where `password` is. GDB thinks
it's over at `$rbp`. Fair enough, since it looks like `$rax` doesn't point to 
anything valid anyway.

**It turns out, the password was `"GenuineIntel3Q"`!**

