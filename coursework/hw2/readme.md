---
title: Homework 2
author: Marcus Simpkins
---

## Summary

> In this task, you are given two binaries posted at
> https://drive.google.com/file/d/1Z5lWEd6oGcMkwHrkA_oekeRnEEcSMhu_/view?usp=sharing
>  
> Each of the binaries contains one or more memory errors, which can be triggered
> by special inputs. Your job is to (i) find *at least one memory error* from each
> binary; (ii) create inputs to trigger the memory errors you find; (iii) analyze
> the root cause of the memory errors you trigger (e.g., stack overflow, heap
> overflow); (ivi) document how you find the errors, how you create the inputs,
> and how you analyze the errors.
> 
> Note: 
> 
> - You do not have to strictly separate the steps of error-finding,
>   input-creating, and error-analysis. For example, if you find the error and
>   create the input in the process of analysis, your report can mix the analysis
>   part with descriptions about finding the error and creating the input.
> - *You can use whatever methods* (reverse engineering, fuzzing, symbolic
>   execution, etc) to complete the assignments.
> 
> ### Details
> 
> - You should download the .zip file to your VM, unpack it, and then analyze the
>   two tasks
> - Task1 contains a single binary file named "task1". To run it:
>   * ```
>     chmod +x task1 
>     ./task1
>     ```
> - Task2 is a folder named "task2", which contains several files --- please do
>   not remove any file in that folder. To run it:
>   * ```
>     cd task2
>     chmod +x task2
>     ./task2
>     ```
> - Any type of memory errors will be counted, including overflow, overread,
>   use-after-free, uninitialized memories, integer overflows, mixed signedness etc.
> - No other details will be given. You are all on your own to figure out how the
>   binaries take inputs, which kind of inputs they want, where the errors are, and
>   how to trigger them.

## Task 1

I tried fuzzing, but i think there's some fuzz-detection going on in the source
code, because afl gets angry when I try to fuzz against this. I was going to try
angr, but we really didn't go over that tool enough, so I'm not super comfortable
with it. I ended up just putting it in ghidra:

![CFG for `main`](tasks/task1/cfg/main.svg)

![CFG for `ReadBytes`](tasks/task1/cfg/ReadBytes.svg)

```c
void main(void)
{
  size_t buf [4];
  
  setbuf(stdout,(char *)0x0);
  setbuf(stderr,(char *)0x0);
  puts("Welcome to the echo service!");
  do {
    buf[0] = 0;
    printf("Enter number of bytes: ");
    ReadBytes((char *)buf,4);
    printf("Enter data: ");
    sleep(1);
    ReadBytes(buffer,buf[0]);
    write(1,buffer,buf[0]);
  } while( true );
}


/* ReadBytes(char*, unsigned int) */

void ReadBytes(char *dest,uint n)
{
  ssize_t bytes_read;
  uint idx;
  
  idx = 0;
  while( true ) {
    if (idx == n) {
      return;
    }
    bytes_read = read(0,dest + idx,n - idx);
    if (bytes_read == 0) break;
    idx = idx + bytes_read;
  }
                    /* WARNING: Subroutine does not return */
  exit(1);
}
```

There's also a global in the decompiled source here called `buffer`, which is a
4kB chunk of memory at the end of the `.text` section. 

So after staring at this code for like 45 minutes I've deduced the following:

- `ReadBytes` is a function that reads `n` bytes into `dest` using the `read`
  function. If `read` reads less than `n` bytes, the function exits. 
- The `main` function gets the number of bytes by calling `ReadBytes` to read
  in 4 bytes, then truncates those 4 bytes and only uses the value in the 
  highest byte.
- That number gets used as the `n` argument for another call to `ReadBytes`,
  which reads user input into the global buffer `buffer`

The way I see it, the only place a stack overflow could happen is with the
second `ReadBytes` call in `main`, which calls on `buffer`

This means we have to find a way of specifying an $`\texttt{n} > 4095`$, and
producing 4095 bytes of garbage input in the second call to `ReadBytes`.

I'm fairly confident we could have an $`\texttt{n} > 4095`$, since the max value
for an integer is within that range. For now, I'll just try to create an `n` of
4110 and an input of all A's. 

```shell
$ python -c "print(b'\xff\xff\xff\xff' + ('A') * 4110)" | ./task1
```

I don't appear to get any output with this, but *theoretically* this should
produce a buffer overflow, as we're writing past `buffer`.

## Task 2

Opening this binary in Ghidra was really intimidating, so I tried to run AFL on
it, in the hopes that it would be a bit more scrutible. Unfortunately, this
didn't work, since AFL would find new execution paths, but couldn't ultimately
trigger any crashes.

I couldn't sync Ghidra and GDB's base addresses for this binary, since Ghidra
kept converting instructions into garbage instructions when I did that. So I just
have to remember that the base address in GDB is `0x555555554000`, and that the
base address in Ghidra is `0x100000`. 

This means, to get the address of something in GDB given its address in Ghidra,
I have to do:

```math
\begin{aligned}
  \text{address of X in Ghidra} &- \tt{0x100000} \\
                                &+ \tt{0x555555554000}
\end{aligned}
```

Unfortunately, I couldn't locate the memory vulnerability in time for
submission, but I do have a *ton* of notes that aid in basic understanding
of the binary. I mostly took a static-analysis approach, since it was difficult
and time-consuming to replicate input paths in GDB.

### Basics of task2 binary

So, I've opted to look through this by hand, and the codebase is really *filled*
with tedious garbage. Like, functions that basically just alias other functions,
functions that call functions on arguments and call a function on the
result. I'm almost scared to look at the call graph cause I'm sure it's just
this *vast*, *dense* forest of confusion.

I *do* know *some* stuff, though, which I'll talk about here.

#### Figuring out user interaction

Here's an *abridged* main function:

```c
void main(void)
{
  char read_result;
  int iVar1;
  char *__path;
  uint input_buf;
  
  setbuf(stdout,(char *)0x0);
  setbuf(stderr,(char *)0x0);
  __path = getenv("DISTFILES");
  if (__path != (char *)0x0) {
    __path = getenv("DISTFILES");
    iVar1 = chdir(__path);
    if (iVar1 != 0) {
                    /* WARNING: Subroutine does not return */
      exit(1);
    }
  }
  readfiles();
  puts("Welcome to the Night Sky Creator, version alpha-0.0.1.");
  do {
    puts("Select an operation.");
    read_result = read_bytes(0,&input_buf,4);
    if (read_result != '\x01') {
                    /* WARNING: Subroutine does not return */
      exit(1);
    }
    switch(input_buf) {
    // ... [cases omitted]
  } while( true );
}
```

So, the main function, generally speaking, is a loop with this "Night Sky
Creator" application. One of the first functions called in the loop is
`read_bytes`, whose definition is as follows:

```c
undefined8 read_bytes(int fd,void *input_buf,ulong n)
{
  ssize_t bytes_read;
  ulong idx;
  void *buf;
  
  idx = 0;
  buf = input_buf;
  while( true ) {
    if (n <= idx) {
      return 1;
    }
    bytes_read = read(fd,buf,n - idx);
    if (bytes_read < 1) break;
    idx = idx + bytes_read;
    buf = (void *)((long)buf + bytes_read);
  }
  return 0;
}
```

So `read_bytes` is a function that reads `n` bytes into `input_buf` by
repeatedly calling `read` and trying to read in data. If `read` ends up reading
too much data, then `read_bytes` returns a 0, which lets the caller know that
something bad has happened. You'll notice a recurring pattern in this program is
to call `read_bytes`, then `exit(1)` if the return value wasn't `\x01`. 

Anyways, back to `main`. Here are the omitted switch/case statements from the
loop:

```c
switch(input_buf) {
case 0:
  age_name();
  break;
case 1:
  which_star();
  break;
case 2:
  which_star_age_name();
  break;
case 3:
  print_user();
  break;
case 4:
  name_stars();
  break;
case 5:
  dat_math();
  break;
case 6:
  name_stars_dat_math();
  break;
case 7:
  print_name_stars_dat();
  break;
case 8:
  serial_number();
  break;
case 9:
  if (DAT_003057c1 != '\x01') {
    puts("You have to be registered to use this feature!");
                /* WARNING: Subroutine does not return */
    exit(1);
  }
  file_stuff();
}
while( true );
```

So this switch statement appears to be the main "user-interaction" loop. It's
actually kinda difficult to interact with, because it's running directly from
the ascii value of `input_buf`, *and* if the user gives the program any less 
than 4 characters of input, the program will exit. 

However, since the program reads from STDIN, you can slowly explore the
interaction by using `echo -ne` to print hex bytes into the task2 binary's
input:

```shell
$ echo -ne '\x00\x00\x00\x00\n' | ./task2
Welcome to the Night Sky Creator, version alpha-0.0.1.
Select an operation.
Age?
$ echo -ne '\x01\x00\x00\x00\n' | ./task2
Welcome to the Night Sky Creator, version alpha-0.0.1.
Select an operation.
Which star?
$ echo -ne '\x04\x00\x00\x00\n' | ./task2
Welcome to the Night Sky Creator, version alpha-0.0.1.
Select an operation.
Name?
$ echo -ne '\x08\x00\x00\x00\n' | ./task2
Welcome to the Night Sky Creator, version alpha-0.0.1.
Select an operation.
What serial number?
$ echo -ne '\x09\x00\x00\x00\n' | ./task2
Welcome to the Night Sky Creator, version alpha-0.0.1.
Select an operation.
You have to be registered to use this feature!
```

So here we can see the switch statement taking different paths. You can probably
guess which functions some of these paths are in from the tentative names I gave
them. 

We'll explore the switch statement functions a bit more, but the point here is 
that we now know how to properly interact with this program! We also know a bit
about what to expect:

- At some point we should find a way to register a user, or at least a way to 
  log in as a user who is registered ("You should be registered to use this
  feature!")
- Users have names, ages, stars, and serial numbers. 

### Characterizing-Info: `readfiles`

So we kinda glossed over this other function that was being called in `main`,
`readfiles`. Not much in it makes sense to me yet, but I wanted to go over it
at a high level, just so we can have some characterizing information. 

So, this program comes with 2 other files in the directory - `sky.txt` and
`serial.txt`. The `readfiles` function appears to read some data from these
files, and into chunks of memory. Lemme just show you these files before we 
jump into the decompiled code:

```shell
$ cat sky.txt 
0
0
$ cat serial.txt 
7fcc3-3e62a-ef5bc-e89c9-c44ad-d303b
```

This data will very likely become important later, once we understand what it
means.

```c
void readfiles(void)
{
  int items_matched;
  void *pvVar1;
  long idx2;
  char *serial_string;
  undefined8 *sky_ptr;
  long in_FS_OFFSET;
  byte zero;
  ulong sky_1;
  ulong sky_3;
  ulong idx;
  ulong idx3;
  ulong idx4;
  FILE *file;
  undefined *dat_ptr;
  char *str_ptr;
  undefined8 sky_2 [5];
  undefined8 local_20;
  long local_10;
  
  zero = 0;
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  file = fopen("sky.txt","r");
  if (file == (FILE *)0x0) {
                    /* WARNING: Subroutine does not return */
    exit(1);
  }
  items_matched = fscanf(file,"%lu",&sky_1);
  if (items_matched != 1) {
                    /* WARNING: Subroutine does not return */
    exit(1);
  }
  // ... [rest of function omitted ]
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return;
}
```

So you can see the file being read with `fopen` and `fscanf`, which should just
be getting the first 0. I think `fscanf` *does* modify the `FILE` pointer, so
when we're reading this input into `sky_1`, I think we're also advancing the
file stream forward into the file's contents a bit. 

Next in this function, we have some looping logic:

```c
idx = 0;
while (idx < sky_1) {
  idx2 = 6;
  useless_ptr = sky_2;
  while (idx2 != 0
                  /* INFO: this loop does nothing */) {
    idx2 = idx2 + -1;
    *useless_ptr = 0;
    useless_ptr = useless_ptr + (ulong)zero * 0x1ffffffffffffffe + 1;
  }
  items_matched = fscanf(file,"%u",sky_2);
  if (items_matched != 1) {
                  /* WARNING: Subroutine does not return */
    exit(1);
  }
  items_matched = fscanf(file,"%s",(long)sky_2 + 4);
  if (items_matched != 1) {
                  /* WARNING: Subroutine does not return */
    exit(1);
  }
  local_20 = 0;
  weird_memory_set_fn(&DAT_00305040,sky_2);
  idx = idx + 1;
}

// ... [logic continues]
```

So the nested loop is actually useless. We set the `zero` variable to `0` and we
never modify it, which means this loop just updates and does nothing. I don't
think `useless_ptr` is even mentioned later.

The more interesting stuff is what's going on with these two `fscanf`'s. It
appears that `sky_1` (the first number appearing in the `sky.txt` file) dictates
how many fields should be read from the file later, in the form of `sky_2` and
`sky_2 + 4`. These fields are read DAT_00305040 by the `weird_memory_set_fn`.

We can take a peek into `weird_memory_set_fn` later, because it *is* doing a bit
more than *just* setting memory, but for the most part that's what it appears
to be doing. 

Once again, the behavior of this loop is important, because all these `fscanf`'s
have me thinking that maybe there's an integer overflow somewhere here. However,
we just don't really know what this stuff is yet. Plus, since our `sky.txt` is
currently just 0 0, this loop never runs for us anyways. 

The next chunk of code is what *actually* reads the next (and final) 0 in our
`sky.txt` file:

```c
items_matched = fscanf(file,"%lu",&sky_3);
if (items_matched != 1) {
                  /* WARNING: Subroutine does not return */
  exit(1);
}
idx3 = 0;
while (idx3 < sky_3) {
  dat_ptr = &DAT_00305060 + DAT_00305760 * 0x38;
  items_matched = fscanf(file,"%s",dat_ptr);
  if (items_matched != 1) {
                  /* WARNING: Subroutine does not return */
    exit(1);
  }
  items_matched = fscanf(file,"%lu",dat_ptr + 0x28);
  if (items_matched != 1) {
                  /* WARNING: Subroutine does not return */
    exit(1);
  }
  pvVar1 = malloc(*(long *)(dat_ptr + 0x28) << 3);
  *(void **)(dat_ptr + 0x30) = pvVar1;
  if (*(long *)(dat_ptr + 0x30) == 0) {
                  /* WARNING: Subroutine does not return */
    exit(1);
  }
  // ... [subloop omitted]
  idx3 = idx3 + 1;
}

// ... [logic continues]
```

So here we're reading the next sky value in. We have another loop we enter based
on that value. Once again, if this value were bigger, we'd be reading a bit more
data from our file. We exect to read a string, and then another number, and we
store that at `dat_ptr` and `dat_ptr + 0x28`, which will of course have the
effect of sequentially-storing our strings and numbers after the beginning of
`DAT_00305060`.

The interesting thing is that we also malloc some memory, and store the pointer
to it in alongside our strings and ints. This malloc has to do with *yet more
data* that we're gonna read in in the subloop that was omitted:

```c
idx4 = 0;
while (idx4 < *(ulong *)(dat_ptr + 0x28)) {
  items_matched = fscanf(file,"%lu",idx4 * 8 + *(long *)(dat_ptr + 0x30));
  if (items_matched != 1) {
                /* WARNING: Subroutine does not return */
    exit(1);
  }
  idx2 = math_30y_plus_x0((long *)&DAT_00305040,*(long *)(*(long *)(dat_ptr + 0x30) + idx4 * 8))
  ;
  if (*(long *)(idx2 + 0x28) != 0) {
                /* WARNING: Subroutine does not return */
    exit(1);
  }
  idx2 = math_30y_plus_x0((long *)&DAT_00305040,*(long *)(*(long *)(dat_ptr + 0x30) + idx4 * 8))
  ;
  *(undefined **)(idx2 + 0x28) = dat_ptr;
  idx4 = idx4 + 1;
}
```

So here, we can see another `fscanf` expecting a number to come from the
file. We use the malloced memory to store it. We're also seeing `idx2` come back
into the picture, this time with this weird function called `math_30y_plus_x0`.
This is another one of those functions that I *really* don't understand, it's
this weird mathematical compuation and 99% of the time it's called I don't
really get why. We'll go over it a bit later, though, just remember that we've
been reading in numbers that are subject to that function's logic, and storing
them at these `DAT_305xxx` offsets.

Alright, let's look at what comes after all this looping business:

```c
DAT_00305760 = sky_3;
items_matched = fclose(file);
if (items_matched != 0) {
                  /* WARNING: Subroutine does not return */
  exit(1);
}
file = fopen("serial.txt","r");
if (file == (FILE *)0x0) {
                  /* WARNING: Subroutine does not return */
  exit(1);
}
serial_string = fgets(&DAT_00305780,0x40,file);
if (serial_string != &DAT_00305780) {
                  /* WARNING: Subroutine does not return */
  exit(1);
}
items_matched = fclose(file);
if (items_matched != 0) {
                  /* WARNING: Subroutine does not return */
  exit(1);
}
str_ptr = strchr(&DAT_00305780,10);
if (str_ptr != (char *)0x0) {
  *str_ptr = '\0';
}
```

This is probably the most boring one. It reads the serial number in as a string, 
and just stores it in `DAT_00305780`. Honestly, this is so plain jane, that I 
feel comfortable renaming this label to `DAT_SERIAL`.

### Exploring switch statement functions

So remember in `main`, how there were a bunch of functions in each switch
statement, and they led to different printouts? Well we really need to answer
some questions about the data format of the `DAT` labels, so maybe we should
look around and see what we can find.

#### `age_name`

The `age_name` function is where we were getting our `Age?` printout from when
we made the switch statement input 0. It basically just reads in the user's
`Name` and `Age` from standard input, and then writes some strange hash to
`DAT_00305040` (the same one that we write `sky_2` to before). If these two
calculations are strange in the same way, maybe we could look at that to try and
find a link between them.

```c
void age_name(void)
{
  char read_result;
  ulong math_result;
  long useless_idx2;
  undefined8 *useless_idx;
  long in_FS_OFFSET;
  ulong input_name;
  undefined8 input_age [5];
  undefined8 local_20;
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  useless_idx2 = 6;
  useless_idx = input_age;
  while (useless_idx2 != 0
                    /* INFO: this loop does nothing */) {
    useless_idx2 = useless_idx2 + -1;
    *useless_idx = 0;
    useless_idx = useless_idx + 1;
  }
  math_result = math_x1_minus_x0_shiftleft4_times_negbig((long *)&DAT_00305040);
  if (0x7f < math_result) {
                    /* WARNING: Subroutine does not return */
    exit(1);
  }
  puts("Age?");
  read_result = read_bytes(0,input_age,4);
  if (read_result != '\x01') {
                    /* WARNING: Subroutine does not return */
    exit(1);
  }
  puts("Name?");
  read_result = read_bytes(0,&input_name,8);
  if (read_result != '\x01') {
                    /* WARNING: Subroutine does not return */
    exit(1);
  }
  if (0x23 < input_name) {
                    /* WARNING: Subroutine does not return */
    exit(1);
  }
  read_result = read_bytes(0,(long)input_age + 4,input_name,(long)input_age + 4);
  if (read_result != '\x01') {
                    /* WARNING: Subroutine does not return */
    exit(1);
  }
  *(undefined *)((long)input_age + input_name + 4) = 0;
  local_20 = 0;
  weird_memory_set_fn(&DAT_00305040,input_age);
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return;
}
```

The other weird thing that you probably noticed is the last read, which appears
to data into this user-dictated part of memory. I actually have no clue what's
even going on here, as `read_bytes` is supposed to only take 3 inputs. Maybe a
Ghidra error?

Anyways, after thinking about the strange
`math_x1_minus_x0_shiftleft4_times_negbig` function I've been seeing everywhere,
I'm starting to think that it's a checksum of some sort to try and verify that
the data isn't corrupted or otherwise tampered with. 

### `which_star`

The `which_star` function (which we can trigger by making the input to the
switch statement 2) is probably the most *deceptively small* rabbit-holey
function I've ever had the misfortune of debugging. On a high level, I know it's
job is to do some math and set some memory, but I absolutely drowned in the
chain of function calls trying to understand this. 

Here's the code for the function itself:

```c
void which_star(void)
{
  char read_result;
  ulong math_result1;
  undefined8 dat_ptr_next_byte;
  long math_result2;
  ulong star [2];
  
  puts("Which star?");
  read_result = read_bytes(0,star,8);
  if (read_result != '\x01') {
                    /* WARNING: Subroutine does not return */
    exit(1);
  }
  math_result1 = math_x1_minus_x0_shiftleft4_times_negbig((long *)&DAT_00305040);
  if (math_result1 <= star[0]) {
                    /* WARNING: Subroutine does not return */
    exit(1);
  }
  dat_ptr_next_byte = next_byte_math(&DAT_00305040);
  math_result2 = math_30y_plus_x0((long *)&DAT_00305040,star[0]);
  another_strange_memory_set_fn(math_result2,dat_ptr_next_byte);
  math_x_1_minus_0x30(&DAT_00305040);
  return;
}
```

So this function asks the user which star they want for this operation, then
performs some math on `DAT_00305040` (once again, the very same one that was
written to with `star_1` in `readfiles` and in `age_name`). It calls some
functions that generally were a pain to trace down, and are still kinda
ambiguous what they do, but we'll go over them here.

Well, what's with this `next_byte_math` function, what's that doing? Well this
function actually lead me down probably the most painful rabbithole in a while.

Here's the definition:

```c
void next_byte_math(long param_1)

{
  undefined8 local_28 [2];
  undefined8 local_18;
  undefined8 local_10;
  
  local_10 = 1;
  local_28[0] = next_byte(param_1);
  local_18 = math_x0_times_neg30_plus_y0(local_28,&local_10,&local_10);
  id(&local_18);
  return;
}
```
::: Note :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
You haven't seen `next_byte` yet, but it's this function that's based on
`setptr`. So to understand one, you're gonna have to understand both.

`setptr` literally does what it says on the tin: 

```c
void setptr(undefined8 *ptr1,undefined8 *ptr2)
{
  *ptr1 = *ptr2;
  return;
}
```

I get the feeling that this function was introduced mainly for obfuscation
purposes, since it's really wasting my time to chase down functions that just
exist to be aliases of standard C one-liners.

`next_byte` just takes a `ptr` and advances it by 8. Since 8 is usually the word
size for this architecture, I'm assuming that it's basically reading advancing
the pointer to the next byte:

```c
undefined8 next_byte(long param_1)
{
  undefined8 local_18 [2];
  
  setptr(local_18,param_1 + 8,param_1 + 8);
  return local_18[0];
}
```

I'm using this interpretation despite the three argument stuff because I think
that's a mistake by ghidra - `setptr` doesn't call with 3 arguments anyways, it
calls with 2. 
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

So, we haven't seen `math_x0_times_neg30_plus_y0` yet, what does that function
do? Well, it performs some strange math, but kinda hides it by putting that
math in a redundant `setptr`:

```c
undefined8 math_x0_times_neg30_plus_y0(long *offs,long *old_base)
{
  undefined8 retval;
  long math_result;
  
  math_result = *old_base * -0x30 + *offs;
  setptr(&retval,&math_result,&math_result);
  return retval;
}
```

As you can see, it's using `math_result` to store the result of the calculation.
Then, `set_ptr` is called for literally no good reason - it's an obfuscation,
and just serves to copy the value we just got to a different variable.

Wow, all that to just explain the `next_byte_math` function! I'm not gonna go
through the rest of the functions, because I honestly don't think I have enough
time.

#### `which_star_age_name`

This function was positively *soothing* to analyze after dealing with
`which_star`.

All this function does is ask the user questions, and write stuff to `DAT_`
memory. Take a look:

```c
void which_star_age_name(void)
{
  char read_result;
  ulong math_result;
  ulong input_star;
  ulong input_name;
  long input_age;
  
  puts("Which star?");
  read_result = read_bytes(0,&input_star,8);
  if (read_result != '\x01') {
                    /* WARNING: Subroutine does not return */
    exit(1);
  }
  math_result = math_x1_minus_x0_shiftleft4_times_negbig((long *)&DAT_00305040);
  if (math_result <= input_star) {
                    /* WARNING: Subroutine does not return */
    exit(1);
  }
  input_age = math_30y_plus_x0((long *)&DAT_00305040,input_star);
  puts("Age?");
  read_result = read_bytes(0,input_age,4);
  if (read_result != '\x01') {
                    /* WARNING: Subroutine does not return */
    exit(1);
  }
  puts("Name?");
  read_result = read_bytes(0,&input_name,8);
  if (read_result != '\x01') {
                    /* WARNING: Subroutine does not return */
    exit(1);
  }
  if (0x24 < input_name) {
                    /* WARNING: Subroutine does not return */
    exit(1);
  }
  read_result = read_bytes(0,input_age + 4,input_name,input_age + 4);
  if (read_result != '\x01') {
                    /* WARNING: Subroutine does not return */
    exit(1);
  }
  return;
}
```

The only remark I really have is that the final `read_bytes` call looks a lot
like the one I saw in `age_and_name`.
